import pickle
import gzip
import argparse
import os
import numpy
import pybedtools

def decompress_index(compressed_indexes):
    decompressed_indexes = {key: {} for key in compressed_indexes}

    for chr in compressed_indexes.keys():
        compressed_chromosome = compressed_indexes[chr]
        decompressed_chromosome = []

        for entry in compressed_chromosome:
            if ',' in entry:  # Sequenza di -1
                count = int(entry.split(',')[1])
                decompressed_chromosome.extend([-1] * count)
            elif '-' in entry:  # Sequenza di numeri contigui
                start, end = map(int, entry.split('-'))
                decompressed_chromosome.extend(numpy.arange(start, end + 1))
            else:  # Numero singolo
                decompressed_chromosome.append(int(entry))

        decompressed_indexes[chr] = numpy.array(decompressed_chromosome)

    return decompressed_indexes

def main(args):

    bed_path = args.bed
    rdm_path = args.rdm
    pbem_path = args.pbem
    index_path = args.index
    output_path = args.output
    rlen = args.rlen

    bed_name = os.path.splitext(os.path.basename(bed_path))[0] # eg. prova.extended
    fake_bed_name = bed_name.split(".extended")[0] # eg. prova
    haplotype = os.path.splitext(os.path.basename(index_path))[0][-1] # eg. prova.idx_0 prendo l'ultimo char della stringa

    with open(index_path, 'rb') as f:
        indexes = pickle.load(f)

    bed = []
    with open(bed_path, 'r') as f:
        for line in f:
            bed.append(line.strip().split())

    rdm = []
    with gzip.open(rdm_path, 'r') as f:
        header_lines = []
        for line in f:
            if line.startswith(b'#'):
                header_lines.append(line.decode().strip())
            else:
                rdm.append([int(x) for x in line.decode().strip().split(";")])

    pbe = []
    with gzip.open(pbem_path, 'r') as f:
        for line in f:
            pbe.append(line.decode().strip().split(";"))

    new_rdm = []
    new_pbe = []
    new_bed = []
    fake_bed = []

    decompressed_indexes = decompress_index(indexes)

    # Devo assicurarmi che il BED esteso non vada oltre i limiti del genoma
    genome_bed = pybedtools.BedTool('\n'.join(['\t'.join([chr, str(0), str(len(decompressed_indexes[chr]))]) for chr in decompressed_indexes]), from_string=True)
    py_bed = pybedtools.BedTool('\n'.join(['\t'.join([str(c) if i != 1 or int(c) >= 0 else '0' for i, c in enumerate(reg)]) for reg in bed]), from_string=True)
    bed = py_bed.intersect(genome_bed)

    for i, region in enumerate(bed):

        chr = region[0]
        start = int(region[1])
        end = int(region[2])

        original_tile = numpy.arange(start, end)
        mutant_tile = decompressed_indexes[chr][start:end]
        #original_tile = list(range(start,end))
        #mutant_tile = list(decompressed_indexes[chr])[start:end]
        #print(original_tile)
        #print(mutant_tile)

        delete_region = False

        mapped_start = mutant_tile[0]
        if mapped_start == -1:
            new_start = None
            for k in range(len(mutant_tile)):
                if mutant_tile[k] != -1:
                    new_start = k
                    break
            if new_start is not None:
                mapped_start = mutant_tile[new_start]

            else:
                print("Region number {} is deleted, as it does not exist anymore in the mutated genome".format(i))
                delete_region = True

        if not delete_region:

            mapped_end = mutant_tile[-1]
            if mapped_end == -1:
                new_end = None
                for k in mutant_tile[::-1]:
                    if k != -1:
                        new_end = k
                        break
                if new_end is not None:
                    mapped_end = mutant_tile[new_end]

            # Modify BED

            new_bed.append([chr, mapped_start, mapped_end + 1])
            if len(header_lines)>0: # Vuol dire paired-end
                fake_bed.append([chr, mapped_start + 2*int(rlen), (mapped_end + 1) - 2*int(rlen)])
            else:
                fake_bed.append([chr, mapped_start + int(rlen), (mapped_end + 1) - int(rlen)])
            length = mapped_end - mapped_start + 1

            # Modify RDM

            rdm_values = numpy.array(rdm[i][1:])
            new_rdm_values = numpy.array([-1 for _ in range(length)])

            indici_uguali = numpy.where(mutant_tile == original_tile)[0]
            new_rdm_values[mutant_tile[indici_uguali] - mapped_start] = rdm_values[indici_uguali]

            indici_diversi = numpy.where((mutant_tile != original_tile) & (mutant_tile != -1))
            new_rdm_values[mutant_tile[indici_diversi] - mapped_start] = rdm_values[indici_diversi]

            # for j, value in enumerate(rdm_values):
            #     if mutant_tile[j] != original_tile[j]:
            #         if mutant_tile[j] != -1:
            #             new_rdm_values[mutant_tile[j] - mapped_start] = value
            #     else:
            #         new_rdm_values[mutant_tile[j] - mapped_start] = value

            # Convert -1 to zero
            new_rdm_values = [0 if n == -1 else n for n in new_rdm_values]
            # Calculate the sum
            s = sum(new_rdm_values)
            # Concatenate
            new_rdm_values = [s] + new_rdm_values
            # Append
            new_rdm.append(new_rdm_values)

            # Modify PBE

            pbe_values = pbe[i]
            new_pbe_values = []

            if pbe_values != ['NA']:

                for read in pbe_values:
                    pos = int(read.split("|")[0])
                    mapped_pos = mutant_tile[pos]
                    if mapped_pos != -1:
                        new_pos = mapped_pos - mapped_start
                        tmp = read.split("|")
                        tmp[0] = str(new_pos)
                        new_pbe_values.append('|'.join(tmp))
            else:
                new_pbe_values.append('NA')

            new_pbe.append(new_pbe_values)

    with open(output_path + fake_bed_name + "_" + haplotype + ".bed", 'wt') as f: # prova_0.bed
        for l in fake_bed:
            r = '\t'.join(map(str, l))
            f.write(r + '\n')

    # with open(output_path + bed_name + ".mod.bed", 'wt') as f:
    #     for l in new_bed:
    #         r = '\t'.join(map(str, l))
    #         f.write(r + '\n')

    with gzip.open(output_path + fake_bed_name + "_" + haplotype + ".rdm.gz", 'wt') as f: # eg. prova_0.rdm.gz
        if len(header_lines)>0: # Se non è vuoto
            for line in header_lines:
                f.write(line + '\n')
        for l in new_rdm:
            r = ';'.join(map(str, l))
            f.write(r + '\n')

    with gzip.open(output_path + fake_bed_name + "_" + haplotype +".pbe.gz", 'wt') as f: # eg. prova_0.pbe.gz
        for l in new_pbe:
            r = ';'.join(map(str, l))
            f.write(r + '\n')

def run():
    parser = argparse.ArgumentParser(description="Convert Synggen RDM & PBEM")

    parser.add_argument('-b', '--bedfile', dest='bed',
                        help="Input bed file extended")
    parser.add_argument('-r', '--rdm', dest='rdm',
                        help="Input RDM")
    parser.add_argument('-p', '--pbe', dest='pbem',
                        help="Input PBEM")
    parser.add_argument('-i', '--index', dest='index',
                        help="Input indexes")
    parser.add_argument('-o', '--output', dest='output',
                        help="Output directory"),
    parser.add_argument('-rlen', '--read_length', dest='rlen',
                        help="Length of the reads")

    args = parser.parse_args()
    main(args)

if __name__ == '__main__':
    run()
