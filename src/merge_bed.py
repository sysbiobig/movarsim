import pybedtools
import argparse
import os
import pysam


def sort_bed_matrix(bed_matrix,custom_chromosome_order):

    # Order based on the order found in the input file
    chromosome_order_dict = {chrom: index for index, chrom in enumerate(custom_chromosome_order)}

    # Order first by chr and then by starting pos
    sorted_matrix = sorted(bed_matrix, key=lambda x: (chromosome_order_dict.get(x[0], float('inf')), int(x[1])))

    return pybedtools.BedTool(sorted_matrix)

def chromosome_names(fasfile):
    fasseq = pysam.Fastafile(fasfile)
    seqnames = [fasseq.references[i] for i in range(0, fasseq.nreferences)]
    return seqnames

def merge_bed_files(bed_files, output_file,seqnames):

    bedtool_objs = [pybedtools.BedTool(bed_file) for bed_file in bed_files]
    merged_bed = pybedtools.BedTool.cat(*bedtool_objs, postmerge=False)
    merged_bed = sort_bed_matrix(merged_bed, seqnames).merge()
    merged_bed.saveas(output_file)


def main():
    parser = argparse.ArgumentParser(description="Merge multiple BED files")
    parser.add_argument("bed_files", nargs='+', help="List of BED files")
    parser.add_argument("-o", "--output", required=True, help="Name of the output file")
    parser.add_argument("-a", "--hapfile", required=True, help="Reference genome")


    # Parsing
    args = parser.parse_args()

    for bed_file in args.bed_files:
        if not os.path.isfile(bed_file):
            print(f"Error: {bed_file} does not exist.")
            return

    hapfile = args.hapfile
    seqnames = chromosome_names(hapfile)
    merge_bed_files(args.bed_files, args.output,seqnames)


if __name__ == "__main__":
    main()
