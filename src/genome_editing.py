import argparse
import collections
import copy
import csv
import os
import random
import shutil
import string
import sys
import tempfile
import time
import pickle
import re
import numpy
import pybedtools
import pybedtools.featurefuncs
import pysam
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from multiprocessing import Pool

starttime = time.time()

# Assign variables for each index of the columns in the var file
VARFILE_COL_VID = 0
VARFILE_COL_MID = 1
VARFILE_COL_HAP = 2
VARFILE_COL_CHR = 3
VARFILE_COL_POS = 4
VARFILE_COL_DEL = 5
VARFILE_COL_DEL_SPAN = 6
VARFILE_COL_INS = 7
VARFILE_COL_INS_SEQ = 8


# class to read a tab delimited file also handling commented lines, i.e. lines staring with a special character (default= "#")
class CommentedFile:
    def __init__(self, f, commentstring="#"):
        self.f = csv.reader(f, delimiter='\t')
        self.commentstring = commentstring

    def __next__(self):
        line = next(self.f)
        while line[0].startswith(self.commentstring):
            line = next(self.f)
        return line

    def __iter__(self):
        return self

def main(args):

    nprocs = args.nprocs
    chooseSeed = args.chooseSeed
    hapfiles = args.hapfiles.split(',')
    nploidy = args.nploidy
    varfiles = args.varfiles.split(',')
    tempfile.tempdir = args.tmpdir
    left_padding = args.left_padding
    right_padding = args.right_padding

    if varfiles == None:
        print("One (or more) .var file is required!", file=sys.stderr)
        quit()

    if hapfiles == None and varfiles == None:
        raise Exception("You must provide .var file and .fasta file!")

    random.seed(chooseSeed)

    if os.path.exists(tempfile.tempdir):
        os.rmdir(tempfile.tempdir)
    try:
        os.mkdir(tempfile.tempdir)
    except:
        print(tempfile.tempdir, "Error creating the tmp directory! Check if the directory already exists", file=sys.stderr)
        quit()

    pybedtools.set_tempdir(tempfile.tempdir)

    if len(hapfiles) == 1: # If the user specify only one haplotype
        hapseq = [pysam.Fastafile(hapfiles[0])] * nploidy  # that haplotype is multiplied by the ploidy
    elif len(hapfiles) == nploidy: # If the user specified as many haplotypes as the ploidy
        hapseq = [pysam.Fastafile(hapfiles[i]) for i in range(0, nploidy)] # they should all have same length
        for ref in hapseq:
            assert ref.lengths == hapseq[0].lengths, "Haploids must contain the same chromosomes and all chromosomes must have the same length!"
    else:
        raise Exception("Incorrect number of hapfiles provided.")

    # take chromosome names from the genome
    seqnames = chromosome_names(hapseq[0].filename)

    # read the var files for the different clones and the name of each clone
    varlists, names = read_variants(varfiles)

    # create a dictionary where the key is the haplotype (0 or 1) and the value is the string of the genome (divided in chromosomes)
    original_hap = collections.OrderedDict()
    # create a dictionary where the key is the haplotype (0 or 1) and the value is a vector of the indexes of the genome (divided in chromosomes)
    indexes = collections.OrderedDict()

    for hap in range(len(hapseq)):
        hapfas = hapseq[hap].filename
        original_hap[hap] = SeqIO.to_dict(SeqIO.parse(hapfas, "fasta")) # import the string of the genome (divided in chromosome)
        indexes[hap] = {key: numpy.array([range(len(original_hap[hap][key]))]) for key in original_hap[hap]} # import the indexes of the genome (divided in chromosomes)

    clones = []
    for i,varlist in enumerate(varlists):
        # this list serve for parallelisation purpose
        # save the name of the clone, its var file, the template genome, the indexes of the template genome, names of the chromosomes included in the template genome
        # left and right padding for the regions of interest which will be included in the output BED file
        clones.append([names[i],varlist,original_hap,indexes,seqnames,left_padding,right_padding])

    # If the number of available cpus is 1 the clones will be created sequentially
    if nprocs < 2:
        for clone in clones:
            create_sample(clone)
    else: # If the number of available cpus is >1 the clones will be created in parallel
        pool = Pool(processes=int(nprocs))
        pool.map(create_sample, clones, 1)

    shutil.rmtree(tempfile.tempdir)

def chromosome_names(fasfile):
    fasseq = pysam.Fastafile(fasfile)
    seqnames = [fasseq.references[i] for i in range(0, fasseq.nreferences)]
    # size = [fasseq.references[i] + '\t' + str(fasseq.lengths[i] + 1) for i in range(len(seqnames))]
    return seqnames

def sort_bed_matrix(bed_matrix,custom_chromosome_order):

    # Order based on the order found in the input file
    chromosome_order_dict = {chrom: index for index, chrom in enumerate(custom_chromosome_order)}

    # Order first by chr and then by starting pos
    sorted_matrix = sorted(bed_matrix, key=lambda x: (chromosome_order_dict.get(x[0], float('inf')), int(x[1])))

    return pybedtools.BedTool(sorted_matrix)

def generate_bed(variants,left,rigth):
    bed = []
    for variant in variants:
        chr = variant[0]
        if variant[1] - left < 0:
            start = 0
        else:
            start = variant[1] - left
        end = variant[2] + rigth
        bed.append([chr,start,end])
    return bed

def compress_index(indexes,tmp,original_hap,variants):
    compressed_indexes = {key: {} for key in indexes} # create a dictionary where the key is the haplotype (0 or 1) to store indexes
    vcf = {key: {} for key in indexes} # create a dictionary where the key is the haplotype (0 or 1) to store ground truth variants

    for hap in indexes.keys(): # for each haplotype (0 or 1)...

        compressed_hap = {key: [] for key in indexes[hap]}  # create a dictionary where the keys are chr names
        hap_vcf = [] # empty list of ground truth variants per haplotype

        for chr in indexes[hap].keys(): # for each chromosome...

            data = numpy.array(indexes[hap][chr][0]) # take the indexes
            possible_variants = [sublist for sublist in variants if sublist[0] == chr] # take all the variants (each represented through a list) in that chromosome
            intervals = []
            i = 0

            while i < len(data): # check the indexes element per element

                current_number = data[i]


                if i > 0: # if we are not at the beginning...
                    prec_number = data[i-1] # check the index right before the current index
                else: # if we are at the beginning...
                    prec_number = None # the preceding index is set none

                if current_number == -1: # [ 0 1 2 3 20 21 -1 ]

                    count = 1
                    while i + count < len(data) and data[i + count] == -1:
                            count += 1
                       # else:
                       #     break

                    #if data[i] == -1 and i + count >= len(data): # maybe -1 is the last element
                    #  count +=1


                    intervals.append(f"-1,{count}")

                    vars = [sublist for sublist in possible_variants if  i <= sublist[1] <= i + count - 1] # le varianti che sono state inserite nella zona cancellata
                    var_names = ",".join([var[3] for var in vars]) # prendo i vid di quelle varianti separate da comma

                    # print(prec_number)
                    # print(prec_number + 1)
                    # print(i)
                    # print(i + count)
                    # print(data[i + count])
                    # print(data[i:])
                    # print(tmp[hap][chr][i])
                    # print(tmp[hap][chr][:i])
                    # print(tmp[hap][chr][i:])
                    # print(original_hap)
                    # print(original_hap.keys())
                    # print(original_hap[hap].keys())
                    # print(original_hap[hap][chr])
                    # print(len(original_hap[hap][chr]))
                    # print(type(hap), type(chr), type(i), type(prec_number))
                    # print(original_hap[hap][chr][prec_number + 1])

                    if prec_number is not None: # I am not at the beginning
                        if i + count < len(data): # I am not at the end
                            if count == 1: # I found only one -1
                                if prec_number + 2 == data[i + count]: # The previous base +2 = the following base (it is a SNP)
                                    if tmp[hap][chr][i] == original_hap[hap][chr][prec_number+1]: # equal alleles
                                        hap_vcf.append([chr,str(i+1),'.',str(tmp[hap][chr][i]),str(original_hap[hap][chr][prec_number+1]),'.','invisible_variant',var_names]) # VCF 1-based while i 0-based
                                    else: # different alleles
                                        hap_vcf.append([chr,str(i+1),'.',str(tmp[hap][chr][i]),str(original_hap[hap][chr][prec_number+1]),'.','visible_variant',var_names]) # VCF 1-based while i 0-based
                                    #hap_vcf.append([chr, str(i + 1), '.', '.','.', '.','.', var_names])
                                else: # The previous base +2 differs from the following base (it is not a SNP)
                                    hap_vcf.append([chr, str(i), '.', '.', '.', '.', '.', var_names])
                            else: # I found more than one -1
                                hap_vcf.append([chr, str(i), '.', '.', '.', '.', '.',var_names])
                        else: # I am at the end
                            if count == 1: # I found only one -1
                                if tmp[hap][chr][i] == original_hap[hap][chr][prec_number+1]:
                                    hap_vcf.append([chr,str(i+1),'.',str(tmp[hap][chr][i]),str(original_hap[hap][chr][prec_number+1]),'.','invisible_variant',var_names]) # VCF 1-based while i 0-based
                                else:
                                    hap_vcf.append([chr,str(i+1),'.',str(tmp[hap][chr][i]),str(original_hap[hap][chr][prec_number+1]),'.','visible_variant',var_names]) # VCF 1-based while i 0-based
                                #hap_vcf.append([chr, str(i + 1), '.', '.','.', '.','.', var_names])
                            else: # I found more than one -1
                                hap_vcf.append([chr, str(i), '.', '.', '.', '.', '.',var_names])
                    else: # I am at the beginning
                        hap_vcf.append([chr, str(i + 1), '.', '.', '.', '.', '.',var_names])

                    i += count

                else:
                    init = current_number
                    next_numbers = data[i + 1:]
                    contiguous_mask = next_numbers == init + numpy.arange(1, len(next_numbers) + 1)
                    n_contiguous = 0
                    for k in range(len(contiguous_mask)):
                        if contiguous_mask[k]:
                            n_contiguous += 1
                        else:
                            break
                    i += n_contiguous + 1
                    if n_contiguous == 0:
                        intervals.append(str(init))
                    else:
                        intervals.append(f"{init}-{init + n_contiguous}")
                    if i < len(data):
                         if data[i] != -1:
                             vars = [sublist for sublist in possible_variants if sublist[1] == i ]
                             var_names = ",".join([var[3] for var in vars])  # prendo i vid di quelle varianti separate da comma
                             hap_vcf.append([chr, str(i), '.', '.', '.', '.', '.', var_names])

            compressed_hap[chr] = intervals

        vcf[hap] = hap_vcf
        compressed_indexes[hap] = compressed_hap

    return compressed_indexes,vcf

def decompress_index(compressed_indexes):
    decompressed_indexes = {key: {} for key in compressed_indexes}

    for hap in compressed_indexes.keys():
        decompressed_hap = {key: {} for key in compressed_indexes[hap]}

        for chr in compressed_indexes[hap].keys():
            compressed_chromosome = compressed_indexes[hap][chr]
            decompressed_chromosome = []

            for entry in compressed_chromosome:
                if ',' in entry:  # Sequenza di -1
                    count = int(entry.split(',')[1])
                    decompressed_chromosome.extend([-1] * count)
                elif '-' in entry:  # Sequenza di numeri contigui
                    start, end = map(int, entry.split('-'))
                    decompressed_chromosome.extend(numpy.arange(start, end + 1))
                else:  # Numero singolo
                    decompressed_chromosome.append(int(entry))

            decompressed_hap[chr] = numpy.array(decompressed_chromosome)

        decompressed_indexes[hap] = decompressed_hap

    return decompressed_indexes

def create_sample(clone):

    # save the information about each clone/sample
    name = clone[0]
    varlist = clone[1]
    original_hap = clone[2]
    indexes = clone[3]
    seqnames = clone[4]
    left_padding = clone[5]
    right_padding = clone[6]

    # create a dictionary to store variants which belong to different haplotypes
    variants_per_hap = collections.OrderedDict()

    for var in varlist.values():
        # The code checks the dictionary variants_per_hap for a key corresponding to the haplotype (0 or 1)
        # If the key does not exist the get() method returns an empty list []
        # otherwise the new information is appended to the list associated with that key
        variants_per_hap[var[VARFILE_COL_HAP]] = variants_per_hap.get(var[VARFILE_COL_HAP], []) + \
                                          [[var[VARFILE_COL_CHR],
                                            var[VARFILE_COL_POS],
                                            var[VARFILE_COL_POS] + var[VARFILE_COL_DEL_SPAN] + 1,
                                            var[VARFILE_COL_VID],
                                            var[VARFILE_COL_MID],
                                            var[VARFILE_COL_DEL_SPAN],
                                            formatins(var[VARFILE_COL_INS_SEQ])]]

    # create a copy of the genome, needed to identify invisible SNPs
    tmp = copy.deepcopy(original_hap)

    # For each haplotype
    for hap in original_hap.keys():
        # If there are variant belonging to that haplotype in the var file
        if hap in variants_per_hap.keys():
            # For each variant
            for variant in variants_per_hap[hap]:
                # mutate the genome string and derive the corresponding indexes
                original_hap[hap], indexes[hap] = make_mutation(original_hap[hap], indexes[hap], variant)

    # compress the indexes of the genome in order to write them
    # while doing that, create the vcf file containing ground truth variants
    compressed_indexes,vcf = compress_index(indexes,tmp,original_hap,variants_per_hap[0])

    if os.path.exists(name): # name = data/root
        shutil.rmtree(name)
    try:
        os.mkdir(name)
    except:
        print(name, "Error creating the ",name," directory! Check if the directory already exists", file=sys.stderr)
        quit()

    file_name = name.split("data/")[1] # ["data/", "root"] prendo "root"

    tmp_bed = generate_bed(variants_per_hap[0], left_padding, right_padding)  # only the first hap is needed
    # wheter variants are in het or homo i need to have reads on both haplotypes to detect its zigosity
    nome_bed = '.'.join([file_name, "bed"])
    bed = pybedtools.BedTool('\n'.join(['\t'.join([str(c) for c in reg]) for reg in tmp_bed]), from_string=True)
    # check that the padded bed does not go beyond template genome boundaries
    genome_bed = pybedtools.BedTool(
        '\n'.join(['\t'.join([chr, str(0), str(len(indexes[0][chr][0]))]) for chr in indexes[0]]), from_string=True)
    bed = bed.intersect(genome_bed)
    bed = sort_bed_matrix(bed, seqnames).merge()  # order and merge overlapping regions
    bed.saveas(name+"/" + nome_bed)


    nome_vcf = '.'.join([file_name, "vcf"])

    with open(name+"/"+nome_vcf, "w") as output_file:
        output_file.write("\t".join(["#CHROM","POS","ID","REF","ALT","QUAL","FILTER","INFO"]) + "\n")
        for row in vcf[0]:
            output_file.write("\t".join(row) + "\n")

    for hap in original_hap.keys():
        if hap in variants_per_hap.keys():
            nome_fasta = '.'.join([file_name, "hap_" + str(hap), "fa"])
            nome_idx = '.'.join([file_name, "idx_" + str(hap), "pkl"])

            with open(name+"/"+nome_fasta, "wt") as fasta_file:
                for seq in original_hap[hap].values():
                    SeqIO.write([seq], fasta_file, "fasta")

            with open(name+"/"+nome_idx, 'wb') as fp:
                pickle.dump(compressed_indexes[hap], fp)

    return None
def make_deletion(chromosomes,indexes,variant):
    chr = variant[0]
    start = variant[1]
    end = variant[2]

    tmp_start = calculate_previous_base(start, indexes[chr][0]) # look for the next available position right before 'start' (to the left)
    if tmp_start is not None: # if the algorithm has found it...
        tmp_mapped_start = indexes[chr][0][tmp_start] + 1 # look at where it maps
    else: # otherwise...
        tmp_mapped_start = 0 # it is assumed it maps to zero

    span = end - start - 1 # number of nt to be deleted
    nt_deleted = 0 # number of nt which have already been deleted
    tmp_indexes = copy.deepcopy(indexes[chr][0]) # copy of the indexes of the chromosome of interest
    i = start

    while span > 0: # while there are nucleotides to be deleted
        if indexes[chr][0][i] == -1: # if the starting position already maps to -1 (that position has already been deleted)
            span -= 1 # reduce the number of nucleotides to be deleted
            i += 1 # increment the position
        else: # otherwise if the starting position maps to a number which differs from -1 (that position has not been already deleted)
            previous_base = calculate_previous_base(i, indexes[chr][0]) # look at the next available position to the left
            if previous_base is not None: # if such position exists...
                mapped_previous_base = indexes[chr][0][previous_base] # see where it maps
                nt_extra = indexes[chr][0][i] - mapped_previous_base - 1 # calculate the number of extra nucleotides which might have been attached to the starting position in a previous insertion
                if nt_extra > 0: # if there are extra nucleotides the deletion should start by deleting those nucleotides
                    for j in range(nt_extra): # each time an extra nucleotide is deleted the span is reduced and the number of deleted nucleotides increase
                        if span > 0:
                            nt_deleted += 1
                            span -= 1
                    if span > 0: # once that all extra nucleotides have been deleted check if the span is still > 0
                        tmp_indexes[i] = -1 # if that s the case then delete the starting position (by setting its index to -1)
                        nt_deleted += 1
                        span -= 1
                        i += 1
                else: # if there are no extra nucleotides
                    tmp_indexes[i] = -1 # delete the starting position (by setting its index to -1)
                    nt_deleted += 1
                    span -= 1
                    i += 1
            else: # the previous position does not exist
                nt_extra = indexes[chr][0][i] - 0 # compute the number of extra nucleotides with respect to 0 (previously we have done this with respect to the mapping position of the previous base
                if nt_extra > 0: # if there are extra nucleotides...
                    for j in range(nt_extra): # delete first the extra nucleotides
                        if span > 0:
                            nt_deleted += 1
                            span -= 1
                    if span > 0: # if the span is still >0 delete the current nucleotide
                        tmp_indexes[i] = -1
                        nt_deleted += 1
                        span -= 1
                        i += 1
                else: # if there are no extra nucleotides
                    tmp_indexes[i] = -1 # delete the current nucleotide
                    nt_deleted += 1
                    span -= 1
                    i += 1

    tmp_indexes[i:][tmp_indexes[i:] != -1] -= nt_deleted # starting from the current position, decrease all indexes not equal to -1 by the number of nucleotides actually deleted
    indexes[chr][0] = tmp_indexes # reassign the mutated indexes
    chromosomes[chr].seq = (chromosomes[chr].seq[:tmp_mapped_start] + chromosomes[chr].seq[
                                                                                    tmp_mapped_start + nt_deleted:]) # alter the chromosome nucleotide sequence
    return chromosomes, indexes

def make_insertion(chromosomes,indexes,variant,seqi):

    chr = variant[0]
    start = variant[1]
    mapped_start = indexes[variant[0]][0][start] # check where the start postion maps

    if mapped_start != -1: # if the start position maps to -1
        chromosomes[chr].seq = (chromosomes[chr].seq[:mapped_start]
                                       + seqi.seq
                                       + chromosomes[chr].seq[mapped_start:]) # insert the sequence of nucleotides
        indexes[chr][0][start:][indexes[chr][0][start:] != -1] += len(seqi.seq) # modify the indexes adding the length of the inserted sequence to all the subsequent indexes !=-1

    else: # if the start position does not map to -1

        new_start = calculate_next_base(start,indexes[chr][0]) # look for the next available position to right of 'start' position

        if new_start is not None: # if such position is found...
            new_mapped_start = indexes[chr][0][new_start] # see where it maps
            chromosomes[chr].seq = (chromosomes[chr].seq[:new_mapped_start]
                                           + seqi.seq
                                           + chromosomes[chr].seq[new_mapped_start:]) # consequently add the insertion sequence of nucleotides
            indexes[chr][0][start:][indexes[chr][0][start:] != -1] += len(seqi.seq) # modify the indexes adding the length of the inserted sequence to all the subsequent indexes !=-1

        else: # if such position is not found...
            print("There are no nucleotides on the right. The insertion has no effect.")

    return chromosomes, indexes


def calculate_previous_base(start, indexes):
    # look for the next available position to the left of start
    # ie a position whose corresponding index differs from -1

    previous_base = None
    for i in range(start-1, -1, -1):
        if indexes[i] != -1:
            previous_base = i
            break

    return previous_base

def calculate_next_base(start, indexes):
    next_base = None
    if start < len(indexes) - 1:
        for i in range(start+1,len(indexes)-1):
            if indexes[i] != -1:
                next_base = i
                break
    return next_base

def make_mutation(chromosomes,indexes,variant):

    new_chromosomes = None
    new_indexes = None

    print(variant) # variant = [chr, pos, pos+span, vid, mid, del_span, ins_seq]

    if variant[5] != 0: # if the length of the DEL component is non zero

        print(chromosomes[variant[0]].seq[0:100])
        print(indexes[variant[0]][0][:100])

        # implement DEL
        new_chromosomes, new_indexes = make_deletion(chromosomes, indexes, variant)

        print(new_chromosomes[variant[0]].seq[0:100])
        print(new_indexes[variant[0]][0][:100])

    if variant[6] != "None": # if the INS component  is not null

        info = readins(variant[6])  # From string to list
        # ['template.fna',chr2,start,span,dup,rev-comp]
        # or ['random',5]
        # or 'ACTGCTGC'
        # or [chr2,start,span,dup,rev-comp]

        if info[0] == 'random':  # The user wants to add a randomly generated sequence of given length eg ['random',5]

            seqi = SeqRecord(Seq("".join(random.choice("ACGT") for x in range(info[1]))), id=info[0], name="",
                             description="") # create a radom sequence of length info[1]

            #print(chromosomes[variant[0]].seq[0:100])
            #print(indexes[variant[0]][0][:100])

            # Implement random insertion
            new_chromosomes, new_indexes = make_insertion(chromosomes, indexes, variant, seqi)

            #print(new_chromosomes[variant[0]].seq[0:100])
            #print(new_indexes[variant[0]][0][:100])

        elif len(info) == 1:  # The user specifies a precise sequence of nucleotides eg 'ACTGCTGC'

            seqi = SeqRecord(Seq(info[0]), id='user-specified', name="", description="") # create the sequence object

            print(chromosomes[variant[0]].seq[0:100])
            print(indexes[variant[0]][0][:100])

            # Implement insert of given sequence
            new_chromosomes, new_indexes = make_insertion(chromosomes, indexes, variant, seqi)

            print(new_chromosomes[variant[0]].seq[0:100])
            print(new_indexes[variant[0]][0][:100])

        elif len(info) == 6:  # The user wants to sample the insertion sequence from a FASTA file inside the data directory eg ['template.fna',chr2,start,span,dup,rev-comp]

            tmpfas = pysam.Fastafile("data/" + info[0]) # load the fasta file
            seqi = SeqRecord(Seq(tmpfas.fetch(str(info[1]), info[2], info[2] + info[3])), id=info[0], name="",
                             description="") # extract the sequence of interest using chromosome, start position and span information
            tmpfas.close()

            # Determine whether to use the reverse complement of the sequence or keep it as is
            if info[5]:
                seqi.seq = seqi.seq.reverse_complement()

            # create a copy of the sequence (wheher reverse complemented or not)
            seqt = copy.deepcopy(seqi)

            # duplicate the sequence as many times as the user specified
            for i in range(1, info[4]):
                seqi.seq += seqt.seq

            print(chromosomes[variant[0]].seq[0:100])
            print(indexes[variant[0]][0][:100])

            # Implement insertion a sequence extracted from an external fasta file
            new_chromosomes, new_indexes = make_insertion(chromosomes, indexes, variant, seqi)

            print(new_chromosomes[variant[0]].seq[0:100])
            print(new_indexes[variant[0]][0][:100])

        else:  # The user wants to sample the insertion sequence from the genome which is currently undergoing mutation eg [chr2,start,span,dup,rev-comp]

            chr = info[0]
            start = info[1]
            mapped_start = indexes[chr][0][start] # see where the starting position, from which the user wants to start extracting nucleotides for the insertion sequence, is mapped
            # it could map to a position which was previously deleted by another variant
            if mapped_start == -1:
                # in this case the algorithm will search for the next available position to the left of the starting position he/she specified
                tmp_start = calculate_previous_base(start, indexes[chr][0])
                if tmp_start is not None: # if an available position was found...
                    mapped_start = indexes[chr][0][tmp_start] + 1
                else: # otherwise...
                    print(
                        "The starting position you specified does not exist anymore. Neither do all previous positions. Starting position is set to 0.")
                    mapped_start = 0

            # calculate end position based on 'start' and 'span'
            end = info[1] + info[2]
            # look at where the end position maps
            mapped_end = indexes[chr][0][end]
            if mapped_end == -1: # if end position maps to -1 (a position which was previously deleted)
                new_end = None
                for i in range(end, len(indexes[chr][0])): # search for the next available position to the right of 'end'
                    if indexes[chr][0][i] != -1:
                        new_end = i
                        break
                if new_end is not None: # if it is found...
                    mapped_end = indexes[chr][0][new_end]
                else: # otherwise...
                    mapped_end = len(chromosomes[chr].seq)
                    print(
                        "The ending position you specified does not exist anymore. Neither do all the following positions. Ending position is set to the last position.")

            # once the extremes of the sequence are found, the sequence is extracted
            seqi = SeqRecord(Seq(str(chromosomes[chr].seq[mapped_start:mapped_end])))

            # whether to take the reverse complement of the sequence or not
            if info[4]:
                seqi.seq = seqi.seq.reverse_complement()

            # create a copy of the sequence
            seqt = copy.deepcopy(seqi)

            # duplicate the sequence as many times as the user desires
            for i in range(1, info[3]):
                seqi.seq += seqt.seq

            print(chromosomes[variant[0]].seq[0:100])
            print(indexes[variant[0]][0][:100])

            # Implement the insertion of a sequece extracted from the genome which is currently undergoing mutation
            new_chromosomes, new_indexes = make_insertion(chromosomes, indexes, variant, seqi)

            print(new_chromosomes[variant[0]].seq[0:100])
            print(new_indexes[variant[0]][0][:100])

    return new_chromosomes, new_indexes

# Read VAR file for each clone and read the names of each clone
def read_variants(varfiles):
    varlists = [] # list of var files for each clone
    names = [] # list of the names of different clones

    # for each clone
    for varfile in varfiles:

        # the name of the clone is the name you give to the file (removing .var)
        name = re.sub(r"\.var$", '', varfile)

        # create a dictionary where the key is the VID and the value is the row of the var file
        varlist = collections.OrderedDict()
        with open(varfile, 'r') as file:
            ifile = CommentedFile(file)
            for row in ifile:
                varlist[row[VARFILE_COL_VID]] = row

        # for each VID from the input VAR file
        for var in varlist.keys():  # get the info of that variant in varlist[var] and modify the different fields according to the need (eg. HAP value is converted from string to int)
            varlist[var][VARFILE_COL_HAP] = int(varlist[var][VARFILE_COL_HAP]) # convert to int
            varlist[var][VARFILE_COL_POS] = int(round(float(varlist[var][VARFILE_COL_POS]))) # convert to int
            varlist[var][VARFILE_COL_DEL] = varlist[var][VARFILE_COL_DEL] in ['True'] # convert to boolean
            varlist[var][VARFILE_COL_DEL_SPAN] = int(varlist[var][VARFILE_COL_DEL_SPAN]) # convert to int
            varlist[var][VARFILE_COL_INS] = varlist[var][VARFILE_COL_INS] in ['True'] # convert to boolean
            varlist[var][VARFILE_COL_INS_SEQ] = readins(varlist[var][VARFILE_COL_INS_SEQ]) # convert the VARINSEQ field from a comma-separated string to a list of values

        varlists.append(varlist)
        names.append(name)

    return varlists,names


# function to read INS SEQ column of the VAR file
def readins(cell):

    if cell == 'None':
        return None

    tmp = cell.split(',')
    # these are the four different formats
    # ["example.fas", "fins1:1-1200", "1", "f"]
    # or ["random", "5"]
    # or ["ACTGACTGCT"]
    # or ["chr1:1-1200", "1", "f"]

    # insertion of random content
    if tmp[0] == "random" :
        # read the number of random nucleotides to generate
        n_rnd_nucleotides= int(tmp[1])
        return [tmp[0], n_rnd_nucleotides]
    elif len(tmp) == 1: # insertion of a user-specified string of nucleotides
        return tmp
    elif len(tmp) == 4: # insertion of a sequence extracted from an external fasta

        # tmp[1].split(':') -> ["fins1", "1-1200"]
        # tmp[1].split(':')[0] -> "fins1"
        # tmp[1].split(':')[1] -> "1-1200"

        chr = tmp[1].split(':')[0]  # save the name of the chromosome
        copy = int(tmp[2]) # save the number of times the sequence should be duplicated
        start = int(tmp[1].split(':')[1].split('-')[0]) - 1 # save the starting position 0-based
        span = int(tmp[1].split(':')[1].split('-')[1]) - start # save the span
        return [tmp[0], chr, start, span, copy, True if tmp[3] == 'r' else False] # return a list
    else: # insertion of a sequence extracted from the fasta used for the simulation
        chr = tmp[0].split(':')[0]
        copy = int(tmp[1])
        start = int(tmp[0].split(':')[1].split('-')[0]) - 1
        span = int(tmp[0].split(':')[1].split('-')[1]) - start
        return [chr, start, span, copy, True if tmp[2] == 'r' else False] # return a list



# function that reverse the effect of readins
def formatins(cell):
    # cell = ['example.fasta', '22', 25202000, 1000, 2, False] (0-based cell[2])
    # or
    # cell = ['random', 5]
    # or
    # cell = ['22', 25202000, 1000, 2, False]
    # or
    # cell = ['ABCDEF']

    # convert the list of elements characterising the insertion sequence to a comma-separated string

    if not cell:
        return 'None'
    if cell[0] == 'random':
        return ','.join([cell[0], str(cell[1])])
    elif len(cell) == 1:
        return cell[0]
    elif len(cell) == 6:
        # info about revcomp: if the value is true, then set it to 'r', otherwise set it to 'f'
        revcomp = 'r' if cell[5] else 'f'
        coord = cell[1] + ':' + str(cell[2] + 1) + '-' + str(cell[2] + cell[3])
        return ','.join([cell[0], coord, str(cell[4]), revcomp])
    else:
        revcomp = 'r' if cell[4] else 'f'
        coord = cell[0] + ':' + str(cell[1] + 1) + '-' + str(cell[1] + cell[2])
        return ','.join([coord, str(cell[3]), revcomp])


def run():
    parser = argparse.ArgumentParser(description="Spike-in mutations to fasta files")

    parser.add_argument('-a', '--hapfiles', dest='hapfiles',
                        help="Input one (or more) haplotypes separated by commas, required")
    parser.add_argument('-v', '--varfiles', dest='varfiles',
                        help="Input one (or more) lists of variants separated by commas, required")
    parser.add_argument('-o', '--oprefix', dest='oprefix', default=None,  # output1,output2,output3,...
                        help="Prefix for output files, default: name of the VAR file")
    parser.add_argument('-y', '--nploidy', dest='nploidy', default=2, type=int,
                        help="Genome ploidity default= %(default)s ploid")
    parser.add_argument('-n', '--nprocs', dest='nprocs', default=1, type=int,
                        help="Split into multiple processes (default: %(default)s )")
    parser.add_argument('-d', '--tmpdir', dest='tmpdir',
                        default=os.path.join(os.environ['HOME'], 'tmp_' + ''.join(
                            [random.choice(string.ascii_letters) for _ in range(4)])),
                        help="Directory for temporary files, default (last 4 digits random): %(default)s")
    parser.add_argument('-c', '--chooseSeed', dest='chooseSeed', default=None,
                        help="Choose the seed for python random function")
    parser.add_argument('-lp', '--left_padding', dest='left_padding', default=0, type=int,
                        help="Left padding for the output BED file")
    parser.add_argument('-rp', '--right_padding', dest='right_padding', default=0, type=int,
                        help="Right padding for the output BED file")

    args = parser.parse_args()
    main(args)

if __name__ == '__main__':
    run()
    print("Total runtime: ", time.time() - starttime, " seconds", file=sys.stderr)

