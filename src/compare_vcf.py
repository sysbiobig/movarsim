import matplotlib.pyplot as plt
import pandas as pd
from matplotlib_venn import venn2
import csv
import seaborn as sns
import argparse



def read_vcf(file_path):
    variants = []
    with open(file_path, 'r') as file:
        for line in file:
            if line.startswith('#'):
                continue  # Skip header lines
            fields = line.strip().split('\t')
            chrom = fields[0]
            pos = fields[1]
            variants.append((chrom, pos))
    return variants

def read_vcf_err(file_path):
    variants = []
    with open(file_path, 'r') as file:
        for line in file:
            if line.startswith('#'):
                continue  # Skip header lines
            fields = line.strip().split('\t')
            chrom = fields[0]
            pos = str(int(fields[1]) + 1)
            variants.add((chrom, pos))
    return variants


def main(args):

    vcf1_path = args.vcf1
    vcf2_path = args.vcf2
    vcf3_path = args.vcf3
    vcf4_path = args.vcf4

    # prefix = 'low_no_pbe'
    # vcf5_path = prefix + '.haplotypecaller_adj.vcf'
    # vcf6_path = prefix + '.unifiedgenotyper.vcf'
    # vcf7_path = prefix + '.freebayes.vcf'

    # prefix = 'low_no_rdm'
    # vcf8_path = prefix + '.haplotypecaller_adj.vcf'
    # vcf9_path = prefix + '.unifiedgenotyper.vcf'
    # vcf10_path = prefix + '.freebayes.vcf'

    vcf1_variants = read_vcf(vcf1_path)
    vcf2_variants = read_vcf(vcf2_path)
    vcf3_variants = read_vcf(vcf3_path)
    vcf4_variants = read_vcf(vcf4_path)

    # vcf5_variants = read_vcf(vcf5_path)
    # vcf6_variants = read_vcf(vcf6_path)
    # vcf7_variants = read_vcf(vcf7_path)
    # vcf8_variants = read_vcf(vcf8_path)
    # vcf9_variants = read_vcf(vcf9_path)
    # vcf10_variants = read_vcf(vcf10_path)

    # fig, axes = plt.subplots(nrows=1, ncols=3,figsize=(20,20),dpi=300)
    # # fig.tight_layout()
    # #cols = ["HaplotypeCaller","UnifiedGenotyper","Freebayes"]
    # # rows = ["Normal","No PBE","No RDM"]
    # #
    # v = venn2((vcf1_variants, vcf2_variants),set_labels=["Simulated","Called"],set_colors=("g", "r"),alpha=0.5,ax=axes[0])
    # v.get_patch_by_id('11').set_color('gold')
    # for text in v.set_labels:
    #     text.set_fontsize(20)
    # for text in v.subset_labels:
    #     text.set_fontsize(20)
    # axes[0].legend(labels = ["FN","FP","TP"], fontsize=20,loc="upper left")
    # axes[0].set_title("HaplotypeCaller",fontsize=20)
    # #
    # v = venn2((vcf1_variants, vcf3_variants),set_labels=["Simulated","Called"],set_colors=("g", "r"),alpha=0.5,ax=axes[1])
    # v.get_patch_by_id('11').set_color('gold')
    # for text in v.set_labels:
    #     text.set_fontsize(20)
    # for text in v.subset_labels:
    #     text.set_fontsize(20)
    # axes[1].legend(labels = ["FN","FP","TP"], fontsize=20,loc="upper left")
    # axes[1].set_title("FreeBayes",fontsize=20)
    # #
    # v = venn2((vcf1_variants, vcf4_variants),set_labels=["Simulated","Called"],set_colors=("g", "r"),alpha=0.5,ax=axes[2])
    # v.get_patch_by_id('11').set_color('gold')
    # for text in v.set_labels:
    #     text.set_fontsize(20)
    # for text in v.subset_labels:
    #     text.set_fontsize(20)
    # axes[2].legend(labels = ["FN","FP","TP"], fontsize=20,loc="upper left")
    # axes[2].set_title("mpileup",fontsize=20)
    #
    # v = venn2((vcf1_variants, vcf5_variants),set_labels=["Simulated","Called"],set_colors=("g", "r"),alpha=0.5,ax=axes[1, 0])
    # v.get_patch_by_id('11').set_color('gold')
    # for text in v.set_labels:
    #     text.set_fontsize(20)
    # for text in v.subset_labels:
    #     text.set_fontsize(20)
    # axes[1,0].legend(labels = ["FN","FP","TP"], fontsize=20,loc="upper left")
    # #axes[1,0].set_title("HaplotypeCaller",fontsize=20)
    #
    # v = venn2((vcf1_variants, vcf6_variants),set_labels=["Simulated","Called"],set_colors=("g", "r"),alpha=0.5,ax=axes[1, 1])
    # v.get_patch_by_id('11').set_color('gold')
    # for text in v.set_labels:
    #     text.set_fontsize(20)
    # for text in v.subset_labels:
    #     text.set_fontsize(20)
    # axes[1,1].legend(labels = ["FN","FP","TP"], fontsize=20,loc="upper left")
    # #axes[1,0].set_title("UnifiedGenotyper",fontsize=20)
    #
    # v = venn2((vcf1_variants, vcf7_variants),set_labels=["Simulated","Called"],set_colors=("g", "r"),alpha=0.5,ax=axes[1, 2])
    # v.get_patch_by_id('11').set_color('gold')
    # for text in v.set_labels:
    #     text.set_fontsize(20)
    # for text in v.subset_labels:
    #     text.set_fontsize(20)
    # axes[1,2].legend(labels = ["FN","FP","TP"], fontsize=20,loc="upper left")
    # #axes[1,0].set_title("Freebayes",fontsize=20)
    #
    # v = venn2((vcf1_variants, vcf8_variants),set_labels=["Simulated","Called"],set_colors=("g", "r"),alpha=0.5,ax=axes[2, 0])
    # v.get_patch_by_id('11').set_color('gold')
    # for text in v.set_labels:
    #     text.set_fontsize(20)
    # for text in v.subset_labels:
    #     text.set_fontsize(20)
    # axes[2,0].legend(labels = ["FN","FP","TP"], fontsize=20,loc="upper left")
    # #axes[1,0].set_title("HaplotypeCaller",fontsize=20)
    #
    # v = venn2((vcf1_variants, vcf9_variants),set_labels=["Simulated","Called"],set_colors=("g", "r"),alpha=0.5,ax=axes[2, 1])
    # v.get_patch_by_id('11').set_color('gold')
    # for text in v.set_labels:
    #     text.set_fontsize(20)
    # for text in v.subset_labels:
    #     text.set_fontsize(20)
    # axes[2,1].legend(labels = ["FN","FP","TP"], fontsize=20,loc="upper left")
    # #axes[1,0].set_title("UnifiedGenotyper",fontsize=20)
    #
    # v = venn2((vcf1_variants, vcf10_variants),set_labels=["Simulated","Called"],set_colors=("g", "r"),alpha=0.5,ax=axes[2, 2])
    # v.get_patch_by_id('11').set_color('gold')
    # for text in v.set_labels:
    #     text.set_fontsize(20)
    # for text in v.subset_labels:
    #     text.set_fontsize(20)
    # axes[2,2].legend(labels = ["FN","FP","TP"], fontsize=20,loc="upper left")
    # #axes[1,0].set_title("Freebayes",fontsize=20)
    #
    # for ax in axes.flatten():
    #     ax.set_axis_on()
    #     ax.spines['top'].set_visible(False)
    #     ax.spines['right'].set_visible(False)
    #     ax.spines['left'].set_visible(False)
    #     ax.spines['bottom'].set_visible(False)
    #
    #
    # for ax, col in zip(axes[0,:], cols):
    #     if col == "HaplotypeCaller":
    #         ax.annotate(col, xy=(0.5, 1), xytext=(0, 4),
    #                     xycoords='axes fraction', textcoords='offset points',
    #                     size=30, ha='center',va='baseline')
    #     elif col == "UnifiedGenotyper":
    #         ax.annotate(col, xy=(0.5, 1), xytext=(0, 4),
    #                     xycoords='axes fraction', textcoords='offset points',
    #                     size=30, ha='center', va='baseline')
    #     else:
    #         ax.annotate(col, xy=(0.5, 1), xytext=(0, 50),
    #                     xycoords='axes fraction', textcoords='offset points',
    #                     size=30, ha='center', va='baseline')
    # fig.subplots_adjust(left=0.05, top=0.95)
    #
    #
    # for ax, row in zip(axes[:,0], rows):
    #     ax.annotate(row, xy=(0, 0.5), xytext=(-ax.yaxis.labelpad - 5, 0),
    #                 xycoords=ax.yaxis.label, textcoords='offset points',
    #                 size=30, ha='right', va='center',rotation=90)

    #plt.savefig('data/pipeline/simple/high.png')
    # exit()
    cov1_path = args.cov1
    cov2_path = args.cov2
    cov3_path = args.cov3
    cov4_path = args.cov4

    data = {}
    with open(cov1_path, 'r') as input:
        reader = csv.reader(input, delimiter='\t')
        next(reader)

        for i, row in enumerate(reader):
            key = list(vcf1_variants)[i]
            value = row[3]
            data[key] = value

    with open(cov2_path, 'r') as input:
        reader = csv.reader(input, delimiter='\t')
        next(reader)

        for i, row in enumerate(reader):
            key = list(vcf2_variants)[i]
            value = row[3]
            data[key] = value

    with open(cov3_path, 'r') as input:
        reader = csv.reader(input, delimiter='\t')
        next(reader)

        for i, row in enumerate(reader):
            key = list(vcf3_variants)[i]
            value = row[3]
            data[key] = value

    with open(cov4_path, 'r') as input:
        reader = csv.reader(input, delimiter='\t')
        next(reader)

        for i, row in enumerate(reader):
            key = list(vcf4_variants)[i]
            value = row[3]
            data[key] = value

    common_variants_hc = list(set(vcf1_variants).intersection(set(vcf2_variants)))
    common_variants_fb = list(set(vcf1_variants).intersection(set(vcf3_variants)))
    common_variants_mp = list(set(vcf1_variants).intersection(set(vcf4_variants)))

    TP_hc = len(common_variants_hc)  # Called & Simulated
    FP_hc = len(vcf2_variants) - len(common_variants_hc)  # Called - Simulated
    FN_hc = len(vcf1_variants) - len(common_variants_hc)  # Simulated - Called
    #FN = [element for element in vcf1_variants if element not in common_variants_hc]
    #FP = [element for element in vcf2_variants if element not in common_variants_hc]
    #print("FN: {}".format(FN))
    #print("FP: {}".format(FP))

    TP_fb = len(common_variants_fb)  # Called & Simulated
    FP_fb = len(vcf3_variants) - len(common_variants_fb)  # Called - Simulated
    FN_fb = len(vcf1_variants) - len(common_variants_fb)  # Simulated - Called

    TP_mp = len(common_variants_mp)  # Called & Simulated
    FP_mp = len(vcf4_variants) - len(common_variants_mp)  # Called - Simulated
    FN_mp = len(vcf1_variants) - len(common_variants_mp)  # Simulated - Called
    FP = [element for element in vcf4_variants if element not in common_variants_mp]
    print(FP)


    coverage_common_hc = []
    coverage_FN_hc = []
    coverage_FP_hc = []

    coverage_common_fb = []
    coverage_FN_fb = []
    coverage_FP_fb = []

    coverage_common_mp = []
    coverage_FN_mp = []
    coverage_FP_mp = []

    for variant in vcf1_variants:
        if variant in common_variants_hc:
            coverage_common_hc.append(float(data[variant]))
        else:
            coverage_FN_hc.append(float(data[variant]))

        if variant in common_variants_fb:
            coverage_common_fb.append(float(data[variant]))
        else:
            coverage_FN_fb.append(float(data[variant]))

        if variant in common_variants_mp:
            coverage_common_mp.append(float(data[variant]))
        else:
            coverage_FN_mp.append(float(data[variant]))

    for variant in vcf2_variants:
        if not variant in common_variants_hc:
            #print("FP in HC : {}".format(variant))
            coverage_FP_hc.append(float(data[variant]))

    for variant in vcf3_variants:
        if not variant in common_variants_fb:
            # print("FP in UG : {}".format(variant))
            coverage_FP_fb.append(float(data[variant]))
    for variant in vcf4_variants:
        if not variant in common_variants_mp:
            # print("FP in FB : {}".format(variant))
            coverage_FP_mp.append(float(data[variant]))

    coverages_hc = coverage_FN_hc + coverage_common_hc + coverage_FP_hc
    categories_hc = ["FN"] * len(coverage_FN_hc) + ["TP"] * len(coverage_common_hc) + ["FP"] * len(coverage_FP_hc)
    d = {"Category": categories_hc, "Coverages": coverages_hc}
    df_hc = pd.DataFrame(d)

    coverages_fb = coverage_FN_fb + coverage_common_fb + coverage_FP_fb
    categories_fb = ["FN"] * len(coverage_FN_fb) + ["TP"] * len(coverage_common_fb) + ["FP"] * len(coverage_FP_fb)
    d = {"Category": categories_fb, "Coverages": coverages_fb}
    df_fb = pd.DataFrame(d)

    coverages_mp = coverage_FN_mp + coverage_common_mp + coverage_FP_mp
    categories_mp = ["FN"] * len(coverage_FN_mp) + ["TP"] * len(coverage_common_mp) + ["FP"] * len(coverage_FP_mp)
    d = {"Category": categories_mp, "Coverages": coverages_mp}
    df_mp = pd.DataFrame(d)
    fig, axes = plt.subplots(nrows=2, ncols=3, figsize=(15, 10), dpi=300)

    v = venn2((set(vcf1_variants), set(vcf2_variants)), set_labels=["Simulated", "Called"], set_colors=("g", "r"), alpha=0.5,
              ax=axes[0, 0])
    v.get_patch_by_id('11').set_color('gold')
    for text in v.set_labels:
        text.set_fontsize(15)
    for text in v.subset_labels:
        text.set_fontsize(15)
    axes[0, 0].legend(labels=["FN", "FP", "TP"], fontsize="large", loc="upper left")
    axes[0, 0].set_title("HaplotypeCaller", fontsize=15)

    axes[1, 0].set_title("HaplotypeCaller", fontsize=15)
    #axes[1, 0].set_ylim(-1, 50)
    sns.stripplot(df_hc, x=df_hc["Category"], y=df_hc["Coverages"], ax=axes[1, 0], jitter=0.4, size=3,
                   hue=df_hc["Category"], palette=["g", "gold", "r"])
    # # sns.boxplot(showmeans=True,
    # # meanline=True,
    # # meanprops={'color': 'k', 'ls': '-', 'lw': 2},
    # # medianprops={'visible': False},
    # # whiskerprops={'visible': False},
    # # zorder=10,
    # # data=df_hc,
    # # x=df_hc["Category"],
    # # y=df_hc["Coverages"],
    # # showfliers=False,
    # # showbox=False,
    # # showcaps=False,
    # # ax=p)
    # # sns.violinplot(df_hc,x=df_hc["Category"],y=df_hc["Coverages"],ax=axes[1,0],cut=0,bw_adjust=0.5,density_norm="width")
    # # sns.violinplot(x=df_hc["Category"], y=df_hc["Coverages"], data=pd.melt((df_hc - df_hc.mean()) / df_hc.std()),ax=axes[1,0],cut=0,density_norm="width")
    # # sns.swarmplot(x =df_hc["Category"], y =df_hc["Coverages"], data = df_hc,color= "white",ax=axes[1,0])
    axes[1, 0].set_ylabel("Coverage", fontsize=15)
    axes[1, 0].set_xlabel("")
    axes[1, 0].tick_params(axis='both', which='major', labelsize="large")
    # # axes[1, 0].set_yscale('log')
    # # semilogy([x], y, [fmt], data=None, **kwargs)

    v = venn2((set(vcf1_variants), set(vcf3_variants)), set_labels=["Simulated", "Called"], set_colors=("g", "r"), alpha=0.5,
              ax=axes[0, 1])
    v.get_patch_by_id('11').set_color('gold')
    for text in v.set_labels:
        text.set_fontsize(15)
    for text in v.subset_labels:
        text.set_fontsize(15)
    axes[0, 1].legend(labels=["FN", "FP", "TP"], fontsize="large", loc="upper left")
    axes[0, 1].set_title("FreeBayes", fontsize=15)

    axes[1, 1].set_title("FreeBayes", fontsize=15)
    #axes[1, 1].set_ylim(-1, 50)
    sns.stripplot(df_fb, x=df_fb["Category"], y=df_fb["Coverages"], ax=axes[1, 1], jitter=0.4, size=3,
                  hue=df_fb["Category"], palette=["g", "gold", "r"])
    axes[1, 1].set_ylabel("Coverage", fontsize=15)
    axes[1, 1].set_xlabel("")
    axes[1, 1].tick_params(axis='both', which='major', labelsize="large")

    v = venn2((set(vcf1_variants), set(vcf4_variants)), set_labels=["Simulated", "Called"], set_colors=("g", "r"), alpha=0.5,
              ax=axes[0, 2])
    v.get_patch_by_id('11').set_color('gold')
    for text in v.set_labels:
        text.set_fontsize(15)
    for text in v.subset_labels:
        text.set_fontsize(15)
    axes[0, 2].legend(labels=["FN", "FP", "TP"], fontsize="large", loc="upper left")
    axes[0, 2].set_title("mpileup", fontsize=15)

    axes[1, 2].set_title("mpileup", fontsize=15)
    #axes[1, 2].set_ylim(-1, 50)
    sns.stripplot(df_mp, x=df_mp["Category"], y=df_mp["Coverages"], ax=axes[1, 2], jitter=0.4, size=3,
                  hue=df_mp["Category"], palette=["g", "gold", "r"])
    axes[1, 2].set_ylabel("Coverage", fontsize=15)
    axes[1, 2].set_xlabel("")
    axes[1, 2].tick_params(axis='both', which='major', labelsize="large")

    # plt.tight_layout()
    plt.savefig(fname = "data/pipeline/simple/comparison.png",dpi=600,format="png")
    #
    # # num_count = 0
    # #
    # # with open("root_het_1000_breast_cosmic_0.rdm", 'r') as file:  # RDM mod
    # #     for line in file:
    # #         numbers = line.strip().split(';')
    # #         num_count = num_count + len(numbers) - 1  # perché il primo numero dell'RDM non rappresenta una posizione
    #
    # # TN_hc = num_count - TP_hc - FP_hc - FN_hc
    # # TN_ug = num_count - TP_ug - FP_ug - FN_ug
    # # TN_fb = num_count - TP_fb - FP_fb - FN_fb
    #
    TPR_hc = TP_hc / (TP_hc + FN_hc)
    FDR_hc = FP_hc / (FP_hc + TP_hc)
    #
    TPR_fb = TP_fb / (TP_fb + FN_fb)
    FDR_fb = FP_fb / (FP_fb + TP_fb)
    #
    TPR_mp = TP_mp / (TP_mp + FN_mp)
    FDR_mp = FP_mp / (FP_mp + TP_mp)
    #
    # print("TP of HC: {}".format(TP_hc))
    # print("FP of HC: {}".format(FP_hc))
    # #print("TN of HC: {}".format(TN_hc))
    #print("FN of HC: {}".format(FN_hc))
    print("TPR of HC: {}".format(TPR_hc))
    print("FDR of HC: {}".format(FDR_hc))
    #
    # print("TP of UG: {}".format(TP_ug))
    # print("FP of UG: {}".format(FP_ug))
    # #print("TN of UG: {}".format(TN_ug))
    # print("FN of UG: {}".format(FN_ug))
    print("TPR of FB: {}".format(TPR_fb))
    print("FDR of FB: {}".format(FDR_fb))
    #
    # print("TP of FB: {}".format(TP_fb))
    # print("FP of FB: {}".format(FP_fb))
    # #print("TN of FB: {}".format(TN_fb))
    # print("FN of FB: {}".format(FN_fb))
    print("TPR of MP: {}".format(TPR_mp))
    print("FDR of MP: {}".format(FDR_mp))

    # data_sim = {}
    # with open(prefix + "_simulated_regions_coverages.txt", 'r') as input:
    #     reader = csv.reader(input, delimiter='\t')
    #     next(reader)
    #     count = 1
    #     for row in reader:
    #         key = "Region_" + str(count)
    #         count += 1
    #         value = row[6]
    #         data_sim[key] = value
    #
    # data_HG00138 = {}
    # with open("HG00138_regions_coverages.txt", 'r') as input:
    #     reader = csv.reader(input, delimiter='\t')
    #     next(reader)
    #     count = 1
    #     for row in reader:
    #         key = "Region_" + str(count)
    #         count += 1
    #         value = row[6]
    #         data_HG00138[key] = value
    #
    # data_HG00139 = {}
    # with open("HG00139_regions_coverages.txt", 'r') as input:
    #     reader = csv.reader(input, delimiter='\t')
    #     next(reader)
    #     count = 1
    #     for row in reader:
    #         key = "Region_" + str(count)
    #         count += 1
    #         value = row[6]
    #         data_HG00139[key] = value
    #
    # data_HG00140 = {}
    # with open("HG00140_regions_coverages.txt", 'r') as input:
    #     reader = csv.reader(input, delimiter='\t')
    #     next(reader)
    #
    #     for row in reader:
    #         key = "Region_" + str(count)
    #         count += 1
    #         value = row[6]
    #         data_HG00140[key] = value
    #
    # def cut_at(int, regions, cov_1, cov_2, cov_3, cov_4):
    #     tot_regions = regions[0:int + 1] * 4
    #     types = ["Simulated"] * len(regions[0:int + 1]) + ["Real_HG00138"] * len(regions[0:int + 1]) + [
    #         "Real_HG00139"] * len(regions[0:int + 1]) + ["Real_HG00140"] * len(regions[0:int + 1])
    #     coverages_sim = cov_1[0:int + 1]
    #     coverages_HG00138 = cov_2[0:int + 1]
    #     coverages_HG00139 = cov_3[0:int + 1]
    #     coverages_HG00140 = cov_4[0:int + 1]
    #     coverages = coverages_sim + coverages_HG00138 + coverages_HG00139 + coverages_HG00140
    #     d = {"Region": tot_regions, "Type": types, "Coverage": coverages}
    #     return pd.DataFrame(d)
    #
    # regions = list(data_sim.keys()) * 4
    # types = ["Simulated"] * len(data_sim.keys()) + ["Real_HG00138"] * len(data_sim.keys()) + ["Real_HG00139"] * len(
    #     data_sim.keys()) + ["Real_HG00140"] * len(data_sim.keys())
    # coverages_sim = [float(i) for i in data_sim.values()]
    # coverages_HG00138 = [float(i) for i in data_HG00138.values()]
    # coverages_HG00139 = [float(i) for i in data_HG00139.values()]
    # coverages_HG00140 = [float(i) for i in data_HG00140.values()]
    # coverages = coverages_sim + coverages_HG00138 + coverages_HG00139 + coverages_HG00140
    # d = {"Region": regions, "Type": types, "Coverage": coverages}
    # df = pd.DataFrame(d)
    #
    # df = cut_at(20, list(data_sim.keys()), coverages_sim, coverages_HG00138, coverages_HG00139, coverages_HG00140)
    #
    # fig, axes = plt.subplots(nrows=1, ncols=1)
    # sns.stripplot(data=df, x="Region", y="Coverage", hue="Type", palette="gist_heat", jitter=0.3)
    # axes.tick_params(axis='x', labelsize=10, rotation=90)
    # axes.tick_params(axis='y', labelsize=10)
    # axes.set_ylim(-100, 200)
    #
    # plt.show()
    #
    # d = {"HG00138_Coverage": coverages_HG00138, "Simulated_Coverage": coverages_sim}
    # df_1 = pd.DataFrame(d)
    # d = {"HG00139_Coverage": coverages_HG00139, "Simulated_Coverage": coverages_sim}
    # df_2 = pd.DataFrame(d)
    # d = {"HG00140_Coverage": coverages_HG00140, "Simulated_Coverage": coverages_sim}
    # df_3 = pd.DataFrame(d)
    #
    # fig, axes = plt.subplots(nrows=1, ncols=3, figsize=(30, 10))
    #
    # sns.scatterplot(data=df_1, x="HG00138_Coverage", y="Simulated_Coverage", ax=axes[0], s=50)
    # sns.scatterplot(data=df_2, x="HG00139_Coverage", y="Simulated_Coverage", ax=axes[1])
    # sns.scatterplot(data=df_3, x="HG00140_Coverage", y="Simulated_Coverage", ax=axes[2])
    # axes[0].tick_params(axis='x', labelsize=20)
    # axes[0].tick_params(axis='y', labelsize=20)
    # axes[1].tick_params(axis='x', labelsize=20)
    # axes[1].tick_params(axis='y', labelsize=20)
    # axes[2].tick_params(axis='x', labelsize=20)
    # axes[2].tick_params(axis='y', labelsize=20)
    #
    # axes[0].set_ylabel("Simulated Coverage", fontsize=20)
    # axes[1].set_ylabel("Simulated Coverage", fontsize=20)
    # axes[2].set_ylabel("Simulated Coverage", fontsize=20)
    # axes[0].set_xlabel("HG00138 Coverage", fontsize=20)
    # axes[1].set_xlabel("HG00139 Coverage", fontsize=20)
    # axes[2].set_xlabel("HG00140 Coverage", fontsize=20)
    #
    # plt.show()


def run():
    parser = argparse.ArgumentParser(description="Compare real VCF vs simulated VCF")

    parser.add_argument('-v1', '--vcf1', dest='vcf1',
                        help="Simulated VCF")
    parser.add_argument('-v2', '--vcf2', dest='vcf2',
                        help="VCF by HaplotypeCaller")
    parser.add_argument('-v3', '--vcf3', dest='vcf3',
                        help="VCF by FreeBayes")
    parser.add_argument('-v4', '--vcf4', dest='vcf4',
                        help="VCF by mpileup")
    parser.add_argument('-c1', '--cov1', dest='cov1',
                        help="Coverages at simulated sites")
    parser.add_argument('-c2', '--cov2', dest='cov2',
                        help="Coverages at sites called by HaplotypeCaller")
    parser.add_argument('-c3', '--cov3', dest='cov3',
                        help="Coverages at sites called by FreeBayes")
    parser.add_argument('-c4', '--cov4', dest='cov4',
                        help="Coverages at sites called by mpileup")


    args = parser.parse_args()
    main(args)

if __name__ == '__main__':
    run()
