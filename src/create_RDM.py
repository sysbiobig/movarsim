import gzip
import argparse
import os


def main(args):

    rdm_path = args.rdm
    tmp = os.path.splitext(os.path.basename(rdm_path))[0] # prova.rdm
    rdm_name = tmp.split(".rdm")[0]  # prova
    output_path = args.output

    rdm = []
    with gzip.open(rdm_path, 'r') as f:
        header_lines = []
        for line in f:
            if line.startswith(b'#'):
                header_lines.append(line.decode().strip())
            else:
                rdm.append([int(x) for x in line.decode().strip().split(";")])

    new_rdm = []

    for i, region in enumerate(rdm): # [tot; 1; 2; 0; 0; ...]

        n_positions = len(region) - 1

        new_rdm_values = [1 for _ in range(n_positions)]

        # Concatenate
        new_rdm_values = [n_positions] + new_rdm_values
        # Append
        new_rdm.append(new_rdm_values)

    with gzip.open(output_path + rdm_name + ".uniform.rdm.gz", 'wt') as f: # eg. prova.uniform.rdm.gz
        if len(header_lines)>0: # Se non è vuoto
            for line in header_lines:
                f.write(line + '\n')
        for l in new_rdm:
            r = ';'.join(map(str, l))
            f.write(r + '\n')


def run():
    parser = argparse.ArgumentParser(description="Convert RDM to uniform")

    parser.add_argument('-r', '--rdm', dest='rdm',
                        help="Input RDM")
    parser.add_argument('-o', '--output', dest='output',
                        help="Output directory")

    args = parser.parse_args()
    main(args)

if __name__ == '__main__':
    run()
