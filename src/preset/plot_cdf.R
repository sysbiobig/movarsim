plot_cdf <- function(empirical=empirical,generated_fits=generated_fits,best_fits=best_fits,xlim=xlim,ylabel=ylabel,xlabel=xlabel) {
  
  dist_names <- c("weibull", "gamma", "lnorm", "cauchy", "logis","norm","exp","geom","nbinom","pois")  
  colors <- c("red","yellow2","orange","green","blue","purple","magenta","cyan","pink","darkgray")
  
  df_empirical <- data.frame(empirical=empirical)
  
  index <- 1
  for (i in 1:length(generated_fits))
  {
    if (!is.null(generated_fits[[i]])) 
    {
      index <- i
      break
    }
  }
  
  df_theorical <- data.frame(matrix(nrow = length(generated_fits[[index]]),ncol = length(dist_names)))
  names(df_theorical) <- dist_names
  
  for (i in 1:length(generated_fits))
  {
    df_theorical[dist_names[i]] <- NA
    
    if (!is.null(generated_fits[[i]]))
    {
      df_theorical[dist_names[i]] <- generated_fits[[i]]
    }
    
  }
  
  xmin_legend <-(max(df_empirical$empirical)-min(df_empirical$empirical))/2
  xmax_legend <-max(df_empirical$empirical)
  
  g <-ggplot(df_empirical, aes(x=empirical))+
    stat_ecdf()+
    labs(title = "<b>Empirical</b>", y=ylabel,x=xlabel)+
    coord_cartesian(xlim=xlim,ylim=c(0,1))+
    theme(plot.title = element_markdown(size = 18), axis.text = element_text(size = 12),axis.title=element_text(size=18))+
    annotate("rect", xmin = xmin_legend,xmax = xmax_legend,ymax = 0.75,ymin=0.5, fill = "white", color = "black")+
    geom_segment(aes(x = 5/100*xmax_legend+xmin_legend, xend = 20/100*xmax_legend+xmin_legend, y = 0.68, yend = 0.68),color = "black", size = 1)+
    annotate("text", x = 25/100*xmax_legend+xmin_legend,y = 0.68, label = "Empirical",hjust=0, size = 5)
  
  for (i in 1:length(generated_fits))
  {
    
    if (!is.null(generated_fits[[i]]))
    {
        g<-g+ggplot(df_empirical,aes(x=empirical))+
          stat_ecdf()+
          stat_ecdf(data.frame(theorical=generated_fits[[i]]), mapping=aes(x=theorical),linewidth=1.2,linetype=2,color=colors[i])+
          labs(title = paste("Fitting <b><span style = 'color:",colors[i],";'>",dist_names[i],"</span></b>"), y=ylabel,x=xlabel)+
          coord_cartesian(xlim=xlim,ylim=c(0,1))+
          theme(plot.title = element_markdown(size = 18), axis.text = element_text(size = 12),axis.title=element_text(size=18))+
          annotate("rect", xmin = xmin_legend,xmax = xmax_legend,ymax = 0.75,ymin=0.5, fill = "white", color = "black")+
          geom_segment(aes(x = 5/100*xmax_legend+xmin_legend, xend = 20/100*xmax_legend+xmin_legend, y = 0.68, yend = 0.68),color = "black", size = 1)+
          geom_segment(aes(x = 5/100*xmax_legend+xmin_legend, xend = 20/100*xmax_legend+xmin_legend, y = 0.58, yend = 0.58),color = colors[i], size = 1,linetype="dashed")+
          annotate("text", x = 25/100*xmax_legend+xmin_legend,y = 0.68, label = "Empirical",hjust=0, size=5)+
          annotate("text", x = 25/100*xmax_legend+xmin_legend,y = 0.58, label = dist_names[i],hjust=0,size=5)
    }
    else
    {
      g<-g+ggplot()+
        labs(title = paste("Fitting <b><span style = 'color:",colors[i],";'>",dist_names[i],"</span></b>"), y=ylabel,x=xlabel)+
        coord_cartesian(xlim=xlim,ylim=c(0,1))+
        theme(plot.title = element_markdown(size = 18),axis.text = element_text(size = 12),axis.title=element_text(size=18))+
        geom_blank()
    }
  }
  text <- paste("<b>AIC</b> suggests:<b><span style = 'color:",colors[which(dist_names==best_fits$AIC)],";'>",best_fits$AIC, "</span></b><br>",
                "<b>BIC</b> suggests:<b><span style = 'color:",colors[which(dist_names==best_fits$BIC)],";'>",best_fits$BIC, "</span></b><br>",
                "<b>KS</b> suggests:<b><span style = 'color:",colors[which(dist_names==best_fits$KS)],";'>", best_fits$KS, "</span></b><br>",
                "<b>AD</b> suggests:<b><span style = 'color:",colors[which(dist_names==best_fits$AD)],";'>", best_fits$AD, "</span></b><br>",
                "<b>CvM</b> suggests:<b><span style = 'color:",colors[which(dist_names==best_fits$CvM)],";'>", best_fits$CvM, "</span></b><br>",
                "<b>CHISQ</b> suggests:<b><span style = 'color:",colors[which(dist_names==best_fits$CHISQ)],";'>",best_fits$CHISQ, "</span></b>")
  g<-g+ggplot()+
    annotate("richtext", x = 0.5, y = 0.5, label = text, size = 6, color = "black", vjust = 0.5, hjust = 0.5) + theme_void()

  
  g
  
}

  
  

