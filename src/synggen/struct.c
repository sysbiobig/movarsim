#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>

#include "struct.h"

const char RED[]       = "\033[1;31m";
const char GREEN[]     = "\033[1;32m";
const char YELLOW[]    = "\033[1;33m";
const char BLUE[]      = "\033[1;34m";
const char CYAN[]      = "\033[1;36m";
const char MAGENTA[]   = "\033[1;35m";
const char RESET_COL[] = "\033[0m";


void printHelp()
{
    fprintf(stderr,"Usage: \n %sGenerate Reference Models \n %s./synggen mode=0 bamlist=string bed=string fasta=string commonsnps=string [rlen=int] [seqprot=int] [mrq=int] [mbq=int] [minvaf=double] [out=string] [threads=int]%s\n", CYAN, YELLOW, RESET_COL);
    fprintf(stderr,"\n %sGenerate Synthetic Reads\n %s./synggen mode=1 bed=string fasta=string rdm=string qm=string [pbe=string] [snps=string] [cnv=string] [pm=string] [seqprot=int] [insize=int] [insizestd=float] [adm=double] [nreads=int] [out=string] [threads=int] [seed=int]%s\n\n", CYAN, YELLOW, RESET_COL);
    fprintf(stderr,"%sbamlist=string%s \n List of BAM files paths\n", MAGENTA, RESET_COL);
    fprintf(stderr,"%sbed=string%s \n List of genomic captured regions in BED format\n", MAGENTA, RESET_COL);
    fprintf(stderr,"%sfasta=string%s \n Reference genome FASTA\n", MAGENTA, RESET_COL);
    fprintf(stderr,"%scommonsnps=string%s \n Common SNPs catalogue (VCF format)\n", MAGENTA, RESET_COL);
    fprintf(stderr,"%sseqprot=int%s \n Sequencing protocol (single-end = %s0%s, paired-end = %s1%s)\n (default %s0%s)\n", MAGENTA, RESET_COL,CYAN, RESET_COL,CYAN, RESET_COL, CYAN, RESET_COL );
    fprintf(stderr,"%smrq=int%s \n Min read quality for PBE calculation\n (default %s10%s)\n", MAGENTA, RESET_COL,  CYAN, RESET_COL);
    fprintf(stderr,"%smbq=int%s \n Min base quality for PBE calculation\n (default %s10%s)\n", MAGENTA, RESET_COL,  CYAN, RESET_COL);
    fprintf(stderr,"%sminvaf=double%s \n Minimum VAF to include a genomic position in the PBE model \n (default %s0.01%s)\n", MAGENTA, RESET_COL,  CYAN, RESET_COL);
    fprintf(stderr,"%sout=string%s \n Output folder name\n", MAGENTA, RESET_COL, CYAN, RESET_COL,CYAN, RESET_COL);
    fprintf(stderr,"%sthreads=int%s \n number of threads used (if available) for the computation\n (default %s1%s)\n", MAGENTA, RESET_COL, CYAN, RESET_COL);
    fprintf(stderr,"%srdm=string%s \n Read depth model (RDM) file path\n", MAGENTA, RESET_COL);
    fprintf(stderr,"%spbe=string%s \n Pair Base Error model (PBE) file path\n", MAGENTA, RESET_COL);
    fprintf(stderr,"%sqm=string%s \n Quality model (QM) file path\n", MAGENTA, RESET_COL);
    fprintf(stderr,"%ssnps=string%s \n Germline SNPs (synggen format) file path\n", MAGENTA, RESET_COL);
    fprintf(stderr,"%scna=string%s \n Somatic copy number aberrations (synngen format) file path\n", MAGENTA, RESET_COL);
    fprintf(stderr,"%spm=string%s \n Somatic point mutations (synngen format) file path\n", MAGENTA, RESET_COL);
    fprintf(stderr,"%sinsize=int%s \n Insert size\n (default %s100%s)\n", MAGENTA, RESET_COL,  CYAN, RESET_COL);
    fprintf(stderr,"%sinsizestd=double%s \n Insert size standard deviation\n (default %s5 percent%s of the insert size)\n", MAGENTA, RESET_COL,  CYAN, RESET_COL);
    fprintf(stderr,"%stc=double%s \n Tumor content value for tumor sample generation\n (default %s0.0%s)\n", MAGENTA, RESET_COL,  CYAN, RESET_COL);
    fprintf(stderr,"%snreads=int%s \n Number of reads to generate\n (default %s1000%s)\n", MAGENTA, RESET_COL, CYAN, RESET_COL);
    fprintf(stderr,"%srlen=int%s \n Length of the read\n (default %s100%s)\n", MAGENTA, RESET_COL, CYAN, RESET_COL);
    fprintf(stderr,"%sseed=int%s \n Reads generation seed, should be >0\n (default %srandom%s)\n", MAGENTA, RESET_COL, CYAN, RESET_COL);
}

void deleteFile(const char path[])
{
    size_t path_len;
    char *full_path;
    DIR *dir;
    struct stat stat_path, stat_entry;
    struct dirent *entry;

    stat(path, &stat_path);


    if ((dir = opendir(path)) == NULL) {
        fprintf(stderr, "%s: %s\n", "Can`t open directory", path);
        exit(-1);
    }

    path_len = strlen(path);

    // iteration through entries in the directory
    while ((entry = readdir(dir)) != NULL) {

        if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, ".."))
            continue;

        full_path = calloc(path_len + strlen(entry->d_name) + 1, sizeof(char));
        strcpy(full_path, path);
        strcat(full_path, "/");
        strcat(full_path, entry->d_name);

        stat(full_path, &stat_entry);

        if (S_ISDIR(stat_entry.st_mode) != 0) {
            deleteFile(full_path);
            continue;
        }


        if (unlink(full_path) == 0){
            printf("%s \xe2\x9e\xa4  Removed a file: %s .....\xe2\x9c\x94 %s\n",GREEN, full_path, RESET_COL);
          }
        else
          {
            printf("%s\xe2\x9c\x98 ERROR%s: Can`t remove the file %s%s%s \n", RED, RESET_COL, CYAN,full_path,RESET_COL);
            return 1;
          }

        free(full_path);
    }

    if (rmdir(path) == 0){
        printf("%s \xe2\x9e\xa4  Removed directory: %s .....\xe2\x9c\x94 %s\n",GREEN, path, RESET_COL);
    }
    else{
        printf("%s\xe2\x9c\x98 ERROR%s: Can`t remove the directory %s%s%s \n", RED, RESET_COL, CYAN,path,RESET_COL);
        return 1;
      }

    closedir(dir);
}

int getChrPos(char *chr)
{
    if (strncmp(chr,"chr",3)==0)
    {
        char *tmp = chr;
        chr = (char *)malloc(strlen(chr)-2);
        strcpy(chr,tmp+3);
    }

    int val = atoi(chr);
    if (val>0 && val<=22)
        return(val-1);
    if (strncmp(chr,"X",1)==0)
        return(22);
    if (strncmp(chr,"Y",1)==0)
        return(23);
    return(-1);
}

int getIndexFromBase(char base)
{
	if (base=='A')
		return 0;
	else if (base=='C')
		return 1;
	else if (base=='G')
		return 2;
	else if (base=='T')
		return 3;
	else
		return 4;
}
