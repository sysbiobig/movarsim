#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>

#include "copy_number.h"

int loadCNV(char *file_name, struct target_info *target_regions, struct chr_idx **chrMap, struct input_args *arguments)
{
    FILE *file = fopen(file_name,"r");
    int i,len,r;
    int start, end;
    struct cnv_info *cnv_tmp;
    struct chr_idx *idx_tmp;
    if(file != NULL)
    {
        fprintf(stderr, "%s \xe2\x9e\xa4  Copy Number file: %s .....loaded \xe2\x9c\x94 %s\n",GREEN, file_name, RESET_COL);
    }

    int index = 0, control = 0, columns = 0, cnv_loaded = 0;
    char sep[] = "\t", *pch;
    int allele, line_numb = 1;
    double randnumb;

	char line[MAX_READ_BUFF];

    while(fgets(line,sizeof(line),file) != NULL )
	{
        if (control == 0)
        {
            for(i=0; i<strlen(line); i++)
            {
                if(strncmp(&line[i], sep, 1) == 0)
                {
                    columns++;
                }
            }
            columns++;
            control = 1;
        }

        char *str_tokens[columns];

        pch = strtok(line,"\t");
        i=0;
        while (pch != NULL)
        {
            str_tokens[i++]=pch;
            pch = strtok(NULL,"\t");
        }

        if(i < 4)
        {
            fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the number of columns is not correct.\n",RED, RESET_COL, line_numb);
            exit(1);
        }

        int chr_idx = getChrPos(str_tokens[0]);
		if(chr_idx<0)
		{
		    fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the chromosome is not correct. Human chromosomes should be specificed.\n",RED,RESET_COL,line_numb);
		    exit(1);
		}
        idx_tmp = chrMap[chr_idx];
        start = atoi(str_tokens[1]);
        end = atoi(str_tokens[2]);

        if(start<0 || end<0 || end<=start)
		{
		    fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d start/end positions are not correct.\n",RED,RESET_COL,line_numb);
		    exit(1);
		}

        // A copy number can span many regions
        // But one region can be assigned to just one copy number.
        // We create the structure just once and then reference to all regions spanning the copy number
        cnv_tmp = NULL;
        int added = 0;

        while(idx_tmp!=NULL)
        {
            // each region can be in only one copy number
            r = idx_tmp->elem;
            if (strcmp(str_tokens[0],target_regions->info[r]->chr) == 0 && 
                ((start<=target_regions->info[r]->from && end>=target_regions->info[r]->to) ||
                 (start>=target_regions->info[r]->from && start<=target_regions->info[r]->to) ||
                 (end>=target_regions->info[r]->from && end<=target_regions->info[r]->to)))
            {
                if(cnv_tmp==NULL)
                {
                    cnv_tmp = (struct cnv_info *)malloc(sizeof(struct cnv_info));
                    cnv_tmp->chr = (char*)malloc(sizeof(char)*strlen(str_tokens[0])+1);
                    strcpy(cnv_tmp->chr,str_tokens[0]);
                    subSlash(cnv_tmp->chr);
                    cnv_tmp->start = start;
                    cnv_tmp->end = end;
                    cnv_tmp->allele_a1 = atof(str_tokens[3]);
                    cnv_tmp->allele_a2 = atof(str_tokens[4]);
                    cnv_tmp->clonality_cnv = atof(str_tokens[5]);
                    cnv_tmp->cn_type = 0;
                    // If allele-specific copy number is a float number and itself encodes the clonality information, clonality variable is set to 1
                    if (fmod(cnv_tmp->allele_a1,1)!=0.0 || fmod(cnv_tmp->allele_a2,1)!=0.0)
                    {
                        cnv_tmp->clonality_cnv = 1.0;
                        cnv_tmp->cn_type = 1;
                    }
                }
                if(target_regions->info[r]->rdata->cnv!=NULL)
                {
                    fprintf(stderr,"\n%s\xe2\x9c\x98 WARNING%s: Multiple copy numbers are associated to region %s:%d-%d. Only the first specifid copy number will be kept for that region. Description of how nested/overlapping copy numbers can be defined is available in the synggen documentation.\n",
                            RED, RESET_COL, target_regions->info[r]->chr,target_regions->info[r]->from,target_regions->info[r]->to);
                } else
                {
                    target_regions->info[r]->rdata->cnv = cnv_tmp;
                    added++;
                }
            }
            idx_tmp = idx_tmp->next;
		}
		if(added>0)
        {
            cnv_loaded++;
        }
    }
    return(cnv_loaded);
}

void computeCNV(struct target_info *target_regions, int indexInit, int indexEnd, char *file_cnv, int length_cnv, struct input_args *arguments)
{
	int i=0,r;
	double c, frac, prob;
	struct cnv_info *cn;

	for(r = indexInit; r < indexEnd; r++)
	{
		cn = target_regions->info[r]->rdata->cnv;
        if(cn != NULL)
        {
            if(cn->cn_type==0)
            {
                c = (target_regions->info[r]->rdata->aN*(1.0-((1.0-arguments->adm)*cn->clonality_cnv)))+
                    (target_regions->info[r]->rdata->aN*(cn->allele_a1+cn->allele_a2)/2.0*((1.0-arguments->adm)*cn->clonality_cnv));
                target_regions->info[r]->rdata->aN = (int)ceil(c);
                i=0;
                while(i<target_regions->info[r]->to-target_regions->info[r]->from)
                {
                    c = (target_regions->info[r]->rdata->positions[i].number*(1.0-((1.0-arguments->adm)*cn->clonality_cnv)))+
                        (target_regions->info[r]->rdata->positions[i].number*(cn->allele_a1+cn->allele_a2)/2.0*((1.0-arguments->adm)*cn->clonality_cnv));
                    target_regions->info[r]->rdata->positions[i].number = (int)ceil(c);
                    i++;
                }
            } else
            {
                c = target_regions->info[r]->rdata->aN*(cn->allele_a1+cn->allele_a2)/2.0;
                target_regions->info[r]->rdata->aN = (int)ceil(c);
                i=0;
                while(i<target_regions->info[r]->to-target_regions->info[r]->from)
                {
                    c = target_regions->info[r]->rdata->positions[i].number*(cn->allele_a1+cn->allele_a2)/2.0;
                    target_regions->info[r]->rdata->positions[i].number = (int)ceil(c);
                    i++;
                }
            }
        }
	}
}

void create_allelicFraction(struct target_info *target_regions, struct input_args *arguments)
{
    int i=0;
    double ratio;
    struct cnv_info *cn;

    for(i; i<target_regions->length; i++)
    {
        // for each region retrieve the copy number
        cn = target_regions->info[i]->rdata->cnv;

        // calculate the right AF given the copy number
        target_regions->info[i]->rdata->af_region = 0.5;
        if(cn != NULL)
        {
            if((cn->allele_a1>0 || cn->allele_a2>0))
            {
                if(cn->cn_type==0)
                {
                    target_regions->info[i]->rdata->af_region =
                        ((cn->allele_a1-1)*(1.0-arguments->adm)*cn->clonality_cnv+1.0)/
                        ((cn->allele_a1+cn->allele_a2-2.0)*(1.0-arguments->adm)*cn->clonality_cnv+2.0);
                } else
                {
                    target_regions->info[i]->rdata->af_region = (cn->allele_a1)/(cn->allele_a1+cn->allele_a2);
                }
            }
        }
    }
}