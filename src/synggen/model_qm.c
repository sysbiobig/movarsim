#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#include "model_qm.h"

int getBGE(int **qualities,int read_position,dsfmt_t *dsfmt)
{
	int qual = (int)(dsfmt_genrand_close_open(dsfmt)*((double)(qualities[read_position][40]-1))+((double)1));
	int i = 0;
	
	while(qual>qualities[read_position][i])
	{
		i++;
		if(i>40)
			break;
	}
	return(i);
}

int getPBE(int **qualities,int read_position, int min_quality, dsfmt_t *dsfmt)
{
	int qual = (int)(dsfmt_genrand_close_open(dsfmt)*((double)(qualities[read_position][40]-qualities[read_position][min_quality]))+((double)qualities[read_position][min_quality]));
	int i = min_quality;
	while(qual>qualities[read_position][i])
	{
		i++;
		if(i>40)
			break;
	}
	return(i);
}

int smoothQM(int **qualities,int rlen)
{
	for(int i=0; i<rlen; i++)
	{
		for(int j=1; j<40; j++)
		{
			if(qualities[i][j]>3*qualities[i][j-1]&qualities[i][j]>3*qualities[i][j+1])
			{
				if(qualities[i][j-1]>0|qualities[i][j+1]>0)
					qualities[i][j] = qualities[i][j-1]+qualities[i][j+1];
			}
		}
	}
}

void fixQM(int **qualities,int rlen)
{
	int control;
	int tmp=-1;
	int i,j,k;
	for(i=0; i<rlen; i++)
	{
		control = 0;
		for(j=0; j<41; j++)
		{
			if(qualities[i][j]>0)
			{
				control = 1;
				tmp = i;
			}
		}
		if(control==0)
		{
			if(tmp>=0)
			{
				for(k=0; k<41; k++)
				{
					qualities[i][k] = qualities[tmp][k];
				}
			}
		}
	}
}

int getQMSize(char *file_name)
{
	 gzFile *file = gzopen(file_name,"r");
	char *seq,*tmp_seq,s[200],stmp[200];
	int i,j,len;

	if(file==NULL)
	{
	   fprintf(stderr, "\n%s\xe2\x9c\x98 ERROR%s: Quality model not present\n", RED, RESET_COL);
	   exit(1);
	}

	// get number of lines
	int number_of_lines = 0;

	char line[MAX_READ_BUFF];
	while(gzgets(file,line,sizeof(line)) != Z_NULL )
	{
	   i=0;
	   while (isspace(line[i])) {i++;}
	   if (line[i]!='#')
	      number_of_lines++;
	}

	return(number_of_lines);
}

void loadQM(char *file_name, int rlen, int** qualities)
{
    gzFile *file = gzopen(file_name,"r");
	char *seq,*tmp_seq,s[200],stmp[200];
	int i,j,len;
	char line[MAX_READ_BUFF];

    int columns;    
    int index,r,c;
	int control = 0;
	char sep[] = " ";
	char *pch;

    r = 0;
	while(gzgets(file,line,sizeof(line))!=Z_NULL)
	{
		columns=0;
		for(i=0;i<strlen(line);i++)
		{
			if (strncmp(&line[i],sep,1)==0)
				columns++;
		}
		columns++;

		if (columns!=41)
		{
			printMessage("Quality model and read length not compatible");
			exit(1);
		}

		index = 0;
		pch = strtok(line," ");
        c = 0;
		while (pch != NULL)
		{
	    	qualities[r][c]=atoi(pch);
			//fprintf(stdout,"READ %d %d %d\n",r,c,qualities[r][c]);
	    	pch = strtok(NULL," ");
            c++;
		}
	   r++;
	}

	gzclose(file);
}

void computeQM(int **qualities, int rows_number)
{
    int i,j;
    for(i=0;i<rows_number;i++)
    {
        for(j=1;j<41;j++)
        {
            qualities[i][j]+=qualities[i][j-1];
        }
    }
}

void printQM(gzFile *outfile, int rlen, int **qualities)
{
	int i,j;

	for(i=0;i<rlen;i++)
	{
		gzprintf(outfile,"%d",qualities[i][0]);
		for(j=1;j<41;j++)
			gzprintf(outfile," %d",qualities[i][j]);
		gzprintf(outfile,"\n");
	}
}