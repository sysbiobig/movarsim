#ifndef LOAD_FILES_H"
#include "struct.h"
#define LOAD_FILES_H

#if defined(__cplusplus)
extern "C" {
#endif

void printMessage(char *message);

void checkTargetBed(char *file_name, struct input_args *arguments, char *out_file);

struct target_info* loadTargetBed(char *file_name, struct input_args *arguments);

int loadCommonSNPs(int *paired, char *file_name,struct target_info *target_regions, struct chr_idx **chrMap);

void InitializeChrMap(struct target_info *target_regions, struct chr_idx **chrMap);

#if defined(__cplusplus)
}
#endif

#endif
