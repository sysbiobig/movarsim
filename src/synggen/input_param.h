#ifndef INPUT_H
#include "struct.h"
#define INPUT_H

#if defined(__cplusplus)
extern "C" {
#endif

struct input_args *getInputArgs(char *argv[],int argc);

int checkInputArgs(struct input_args *arguments);

void subSlash(char *s);

#if defined(__cplusplus)
}
#endif

#endif
