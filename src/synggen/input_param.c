#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>

#include "input_param.h"

struct input_args *getInputArgs(char *argv[],int argc)
{
    int i;
    struct input_args *arguments = malloc(sizeof(struct input_args));
    arguments->bamlist = NULL;
    arguments->bed = NULL;
    arguments->csnps = NULL;
    arguments->snps = NULL;
    arguments->cnv = NULL;
    arguments->paired = 0;
    arguments->insert_size = 0;
    arguments->std = 0;
    arguments->mrq = 10;
    arguments->mbq = 10;
    arguments->cores = 1;
    arguments->rlen = 100;
    arguments->adm = 0.0;
    arguments->probs = NULL;
    arguments->pm = NULL;
    arguments->pbe = NULL;
    arguments->qm = NULL;
    arguments->fasta = NULL;
    arguments->outdir = (char *)malloc(3);
    sprintf(arguments->outdir,"./");
    arguments->bgprob = 0.01;
    arguments->gen_reads = 0;
    arguments->nreads = 1000;
    arguments->seed = -1;
    arguments->sequencing_errors = 1;

    char *tmp=NULL;
    
    for(i=1;i<argc;i++)
    {
        if(strncmp(argv[i],"mode=",5) == 0 )
        {
          tmp = (char*)malloc(strlen(argv[i])-4);
          strcpy(tmp,argv[i]+5);
          arguments->gen_reads = atoi(tmp);
          free(tmp);
        }
        else if(strncmp(argv[i],"seqprot=",8) == 0 )
        {
          tmp = (char*)malloc(strlen(argv[i])-7);
          strcpy(tmp,argv[i]+8);
          arguments->paired = atoi(tmp);
          free(tmp);
        }
        else if(strncmp(argv[i],"bed=",4) == 0 )
        {
          arguments->bed = (char*)malloc(sizeof(char)*strlen(argv[i])+1);
          strcpy(arguments->bed,argv[i]+4);
  			  subSlash(arguments->bed);
        }
        else if(strncmp(argv[i],"bamlist=",8) == 0 )
        {
          arguments->bamlist = (char*)malloc(sizeof(char)*strlen(argv[i])+1);
          strcpy(arguments->bamlist,argv[i]+8);
  			  subSlash(arguments->bamlist);
        }
   	    else if(strncmp(argv[i],"rdm=",4) == 0 )
        {
          arguments->probs = (char*)malloc(sizeof(char)*strlen(argv[i])+1);
          strcpy(arguments->probs,argv[i]+4);
  			  subSlash(arguments->probs);
        }
        else if(strncmp(argv[i],"cna=",4) == 0 )
        {
          arguments->cnv = (char*)malloc(sizeof(char)*strlen(argv[i])+1);
          strcpy(arguments->cnv,argv[i]+4);
          subSlash(arguments->cnv);
        }
        else if(strncmp(argv[i],"threads=",8) == 0 )
        {
          tmp = (char*)malloc(strlen(argv[i])-7);
          strcpy(tmp,argv[i]+8);
          arguments->cores = atoi(tmp);
          free(tmp);
        }
        else if(strncmp(argv[i],"rlen=",5) == 0 )
        {
          tmp = (char*)malloc(strlen(argv[i])-4);
          strcpy(tmp,argv[i]+5);
          arguments->rlen = atoi(tmp);
          free(tmp);
        }
        else if(strncmp(argv[i],"out=",4) == 0 )
        {
          arguments->outdir = (char*)malloc(strlen(argv[i])-3);
          strcpy(arguments->outdir,argv[i]+4);
  	      subSlash(arguments->outdir);
        }
        else if(strncmp(argv[i],"fasta=",6) == 0 )
        {
          arguments->fasta = (char*)malloc(sizeof(char)*strlen(argv[i])+1);
          strcpy(arguments->fasta,argv[i]+6);
  	      subSlash(arguments->fasta);
        }
        else if(strncmp(argv[i],"commonsnps=",11) == 0 )
        {
        	arguments->csnps = (char*)malloc(sizeof(char)*strlen(argv[i])+1);
        	strcpy(arguments->csnps,argv[i]+11);
        	subSlash(arguments->csnps);
        }
        else if(strncmp(argv[i],"snps=", 5) == 0)
        {
      		arguments->snps = (char*)malloc(sizeof(char)*strlen(argv[i])+1);
      		strcpy(arguments->snps,argv[i]+5);
      		subSlash(arguments->snps);
        }
        else if(strncmp(argv[i],"pbe=",4) == 0 )
        {
          arguments->pbe = (char*)malloc(sizeof(char)*strlen(argv[i])+1);
          strcpy(arguments->pbe,argv[i]+4);
          subSlash(arguments->pbe);
        }
        else if(strncmp(argv[i],"qm=",3) == 0 )
        {
          arguments->qm = (char*)malloc(sizeof(char)*strlen(argv[i])+1);
          strcpy(arguments->qm,argv[i]+3);
          subSlash(arguments->qm);
        }
        else if(strncmp(argv[i],"tc=",3) == 0 )
        {
          tmp = (char*)malloc(strlen(argv[i])-3);
          strcpy(tmp,argv[i]+4);
          arguments->adm = 1.0-(double)atof(tmp);
          free(tmp);
        }
        else if(strncmp(argv[i],"pm=",3) == 0 )
        {
          arguments->pm = (char*)malloc(sizeof(char)*strlen(argv[i])+1);
          strcpy(arguments->pm,argv[i]+3);
          subSlash(arguments->pm);
        }
        else if(strncmp(argv[i],"minvaf=",7) == 0 )
        {
          tmp = (char*)malloc(strlen(argv[i])-6);
          strcpy(tmp,argv[i]+7);
          arguments->bgprob = (double)atof(tmp);
          free(tmp);
        }
        else if(strncmp(argv[i],"mbq=",4) == 0 )
        {
          tmp = (char*)malloc(strlen(argv[i])-3);
          strcpy(tmp,argv[i]+4);
          arguments->mbq = atoi(tmp);
          free(tmp);
        }
        else if(strncmp(argv[i],"insize=",7) == 0 )
        {
          tmp = (char*)malloc(strlen(argv[i])-6);
          strcpy(tmp,argv[i]+7);
          arguments->insert_size = atoi(tmp);
          free(tmp);
        }
        else if(strncmp(argv[i],"mrq=",4) == 0 )
        {
          tmp = (char*)malloc(strlen(argv[i])-3);
          strcpy(tmp,argv[i]+4);
          arguments->mrq = atoi(tmp);
          free(tmp);
        }
        else if(strncmp(argv[i],"nreads=",7) == 0 )
        {
            tmp = (char*)malloc(strlen(argv[i])-6);
            strcpy(tmp,argv[i]+7);
            arguments->nreads = atoi(tmp);
            free(tmp);
        }
        else if(strncmp(argv[i],"insizestd=",10) == 0)
        {
          tmp = (char*)malloc(strlen(argv[i])-9);
          strcpy(tmp,argv[i]+10);
          arguments->std = (float)atof(tmp);
          free(tmp);
        }
        else if(strncmp(argv[i],"seed=",5) == 0 )
        {
          tmp = (char*)malloc(strlen(argv[i])-4);
          strcpy(tmp,argv[i]+5);
          arguments->seed = atoi(tmp);
          if(arguments->seed<0)
          {
            fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: the seed should be greater than 0.\n", RED, RESET_COL);
            exit(1);
          }
          free(tmp);
        }
        else if(strncmp(argv[i],"sequencing_errors=",18) == 0 )
        {
            tmp = (char*)malloc(strlen(argv[i])-17);
            strcpy(tmp,argv[i]+18);

            arguments->sequencing_errors = atof(tmp);
            fprintf(stderr, "Value read : %f", arguments->sequencing_errors);

            if (arguments->sequencing_errors != 0.0 && arguments->sequencing_errors != 1.0)
            {
               fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: the sequencing_error should be equal to 0 (FALSE) or 1 (TRUE).\n", RED, RESET_COL);
               exit(1);
            }
            free(tmp);
        }
        else
        {
              fprintf(stderr," %s\xe2\x9e\xa4 ERROR%s: input parameters <%s> not valid  \n", RED, RESET_COL,argv[i]);
              printHelp();
              exit(1);
        }
    }
    return arguments;
}

int checkInputArgs(struct input_args *arguments)
{
	int control = 0;

  if (arguments->cores <= 0)
  {
		fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: the number of threads is not valid.\n", RED, RESET_COL);
		control=1;
  }
  if (arguments->sequencing_errors < 0 || arguments->sequencing_errors > 1)
  {
  		fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: the value of sequencing_error is not valid.\n", RED, RESET_COL);
		control=1;
  }
  if (arguments->bed == NULL)
  {
    fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: you should specify the captured regions BED file.\n", RED, RESET_COL);
    control=1;
  }
  if(arguments->fasta == NULL)
  {
    fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: you should specify the FASTA file.\n", RED, RESET_COL);
    control=1;
  }

  if (arguments->csnps == NULL & arguments->gen_reads==0)
	{
    fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: you should specify a VCF file with common SNPs.\n", RED, RESET_COL);
    control=1;
  }

  if (arguments->bamlist == NULL & arguments->gen_reads==0)
	{
    fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: you should specify a list of BAM files.\n", RED, RESET_COL);
    control=1;
  }

  if (arguments->paired < 0 || arguments->paired > 1)
	{
		fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: Sequencing protocol should be 0 (single-end) or 1 (paired-end).\n", RED, RESET_COL);
		control=1;
	}

  if (arguments->mrq < 0 || arguments->mbq < 0)
	{
		fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: Minimum read and base qualities should be >=0.\n", RED, RESET_COL);
		control=1;
	}

  if (arguments->bgprob <= 0)
	{
		fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: Minimum VAF should be >0.\n", RED, RESET_COL);
		control=1;
	}

  if (arguments->cores <= 0)
	{
		fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: Number of threads should be >0.\n", RED, RESET_COL);
		control=1;
	}

  if (arguments->probs == NULL & arguments->gen_reads==1)
	{
    fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: you should specify an RDM file.\n", RED, RESET_COL);
    control=1;
  }

  if (arguments->qm == NULL & arguments->gen_reads==1)
	{
    fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: you should specify an QM file.\n", RED, RESET_COL);
    control=1;
  }

  if (arguments->adm <0 || arguments->adm>1)
	{
		fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: Admixture value should be in [0,1].\n", RED, RESET_COL);
		control=1;
	}

  if (arguments->nreads <= 0)
	{
		fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: Number of reads should be >0.\n", RED, RESET_COL);
		control=1;
	}

  if (arguments->rlen <= 0)
	{
		fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: Read length should be >0.\n", RED, RESET_COL);
		control=1;
	}

  if (arguments->std < 0)
	{
		fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: Insert size standard deviation should be >0.\n", RED, RESET_COL);
		control=1;
	}

	if (arguments->bamlist != NULL & arguments->gen_reads==0)
	{
    fprintf(stderr, "%s \xe2\x9e\xa4  Bamlist file: %s .....loaded \xe2\x9c\x94 %s\n",GREEN, arguments->bamlist, RESET_COL);
		FILE *file = fopen(arguments->bamlist,"r");
  	bam_index_t *bamindex;

		if  (file==NULL)
		{
		    fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: BAM files list not present. \n", RED, RESET_COL);
        //printHelp();
		    return 1;
		}

		arguments->bamfiles_number = 0;

		char line[1000];
		while(fgets(line,sizeof(line),file) != NULL )
		{
		    arguments->bamfiles_number++;
		}

		rewind(file);
		arguments->bamfiles = malloc(sizeof(char*)*arguments->bamfiles_number);
		int counter=0;
		int size=0;

		while(fgets(line,sizeof(line),file) != NULL )
		{
		    if (line[strlen(line)-1]=='\n')
		        size = strlen(line);
		    else
		        size = strlen(line)+1;

		    arguments->bamfiles[counter] = (char*)malloc(size);
		    strncpy(arguments->bamfiles[counter],line,size-1);
		    (arguments->bamfiles[counter])[size-1] = '\0';
				subSlash(arguments->bamfiles[counter]);

		    if (access(arguments->bamfiles[counter], F_OK)!=0)
		    {
		        fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: BAM file %s not present. \n",RED, RESET_COL, arguments->bamfiles[counter]);
		        return 1;
		    }

		    // load BAM indexing
		    bamindex = bam_index_load(arguments->bamfiles[counter]);
		    if (bamindex == 0)
		    {
			     fprintf(stderr,"%sWARNING\xe2\x9d\x97%s: BAM indexing of file %s not present. Will be created now. \n",MAGENTA, RESET_COL, arguments->bamfiles[counter]);
           bamindex = bam_index_build(arguments->bamfiles[counter]);
		       bamindex = bam_index_load(arguments->bamfiles[counter]);
		    	 if (bamindex != 0)
		    	 {
		    	  	counter++;
		    	 }
		    	 else
		    	 {
		    	   fprintf(stderr,"%sWARNING\xe2\x9d\x97%s: unable to create BAM index for file %s.\n",MAGENTA, RESET_COL, arguments->bamfiles[counter]);
		    	 }
		    }
		    else
		    {
		    	counter++;
		    }
        free(bamindex);
		}

		if (counter==0)
		{
			 fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: index creation failed for all BAM files.\n",RED, RESET_COL);
			 return 1;
		}
    fclose(file);
	}

	if(control==1)
		return 1;
	return 0;
}


void subSlash(char *s)
{
	int i;
  int pos=-1;
	#ifdef _WIN32
	for(i=0;i<strlen(s);i++)
      if(s[i] == '/')
        s[i]='\\';
	#else
    for(i=0;i<strlen(s);i++)
      if(s[i] == '\\')
        s[i]="/";
	#endif
}
