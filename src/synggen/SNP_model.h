#ifndef SNP_MODEL_H
#include "struct.h"
#define SNP_MODEL_H

#ifdef __cplusplus
extern "C" {
#endif

int loadSNPModel(char *file_name, struct target_info *target_regions, struct chr_idx **chrMap, struct input_args *arguments);

void introduceSNPs(char s[], int init, int end, struct region_data *elem, double af);

#ifdef __cplusplus
}
#endif

#endif
