#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <math.h>

#include "model_pbe.h"

void computePBE(struct target_info *target_regions,int indexInit, int indexEnd)
{
	int i,r;
	for(r=indexInit;r<indexEnd;r++)
	{
		i=0;
		while(i<target_regions->info[r]->to-target_regions->info[r]->from)
		{
			target_regions->info[r]->rdata->positions[i].Tot =
				target_regions->info[r]->rdata->positions[i].A+
				target_regions->info[r]->rdata->positions[i].C+
				target_regions->info[r]->rdata->positions[i].G+
				target_regions->info[r]->rdata->positions[i].T;

			if(target_regions->info[r]->rdata->sequence[i]=='A')
				target_regions->info[r]->rdata->positions[i].A=0;

			if(target_regions->info[r]->rdata->sequence[i]=='C')
				target_regions->info[r]->rdata->positions[i].C=0;

			if(target_regions->info[r]->rdata->sequence[i]=='G')
				target_regions->info[r]->rdata->positions[i].G=0;

			if(target_regions->info[r]->rdata->sequence[i]=='T')
				target_regions->info[r]->rdata->positions[i].T=0;

			target_regions->info[r]->rdata->positions[i].C += target_regions->info[r]->rdata->positions[i].A;
			target_regions->info[r]->rdata->positions[i].G += target_regions->info[r]->rdata->positions[i].C;
			target_regions->info[r]->rdata->positions[i].T += target_regions->info[r]->rdata->positions[i].G;

			if(target_regions->info[r]->rdata->positions[i].T==0)
				target_regions->info[r]->rdata->positions[i].Tot = 0;

			i++;
		}
	}
}

void loadPBE(char *file_name,struct target_info *targets)
{
	char *seq,*tmp_seq,s[200],stmp[200];
	int i,len;
	gzFile *file = gzopen(file_name,"r");

	if  (file==NULL)
	{
		fprintf(stderr, "\n%s\xe2\x9c\x98 ERROR%s: PBE model not present\n", RED, RESET_COL);
		exit(1);
	}

	// get number of lines
	int number_of_lines = 0;

	char line[MAX_READ_BUFF];
	while(gzgets(file,line,sizeof(line))!=Z_NULL)
	{
		i=0;
		while (isspace(line[i])) {i++;}
		if (line[i]!='#')
		number_of_lines++;
	}

	gzrewind(file);

	if(number_of_lines!=targets->length)
	{
		fprintf(stderr, "\n%s\xe2\x9c\x98 ERROR%s: BED target regions specification and PBE model are not compatible.\n", RED, RESET_COL);
		gzclose(file);
		exit(1);
	}

 	int index,r;
	int control = 0;
	int columns;
	int line_numb = 1;
	char sep[] = ";";
	char *pch;
	char *pch2;
	char *str_tokens[6];
	char *end_str;
	char *end_token;

	r=0;
  	while(gzgets(file,line,sizeof(line))!=Z_NULL)
  	{
		columns=0;
		if(strncmp(line,"NA",2)==0)
		{
			line_numb++;
			r++;
			continue;
		}

		for(i=0;i<strlen(line);i++)
		{
				if (strncmp(&line[i],sep,1)==0)
					columns++;
		}
	   	columns++;

		if ((columns-1)>(targets->info[r]->to-targets->info[r]->from))
		{
			fprintf(stderr, "\n%s\xe2\x9c\x98 ERROR%s: BED target regions specification and PBE model are not compatible.\n", RED, RESET_COL);
			exit(1);
		}


		pch = strtok_r(line,";",&end_str);
		index = 0;
		while (pch != NULL)
		{
     		pch2 = strtok_r(pch,"|",&end_token);
	   		i=0;
			while (pch2!=NULL)
			{
				str_tokens[i++]=pch2;
				pch2 = strtok_r(NULL,"|",&end_token);
			}

			if(atoi(str_tokens[1])>0&&targets->info[r]->rdata->positions[atoi(str_tokens[0])].commonSNP==0)
			{
					targets->info[r]->rdata->positions[atoi(str_tokens[0])].A = atoi(str_tokens[2]);
					targets->info[r]->rdata->positions[atoi(str_tokens[0])].C = atoi(str_tokens[3]);
					targets->info[r]->rdata->positions[atoi(str_tokens[0])].G = atoi(str_tokens[4]);
					targets->info[r]->rdata->positions[atoi(str_tokens[0])].T = atoi(str_tokens[5]);
					targets->info[r]->rdata->positions[atoi(str_tokens[0])].Tot = atoi(str_tokens[1]);
			}
	   		pch = strtok_r(NULL,";",&end_str);
	   		index++;
	 	}
	line_numb++;
	r++;
  }
  gzclose(file);
}

void printPBE(gzFile *outfile, struct target_info *target_regions,int indexInit, int indexEnd, double bgprob)
{
	int i,r,init,control;
	int A,C,G,T,vaf,sum;

	struct snp_info *tmp;
	for (r=indexInit;r<indexEnd;r++)
	{
		i=0;
		init=0;
		control=0;
		while(i<target_regions->info[r]->to-target_regions->info[r]->from)
		{
			sum = target_regions->info[r]->rdata->positions[i].Tot;
			vaf = target_regions->info[r]->rdata->positions[i].T;

			if(vaf>0)
			{
				if(((double)vaf/(double)sum)<0.2&&((double)vaf/(double)sum)>=bgprob&&target_regions->info[r]->rdata->positions[i].commonSNP==0)
				{
					if(init==1)
						gzprintf(outfile, ";");
					if(init==0)
						init=1;

					gzprintf(outfile,"%d|%d|%d|%d|%d|%d",i,sum,target_regions->info[r]->rdata->positions[i].A,
																	target_regions->info[r]->rdata->positions[i].C,
																	target_regions->info[r]->rdata->positions[i].G,
																	target_regions->info[r]->rdata->positions[i].T);

					control=1;
				}
			}
			i++;
		}
		if(control==0)
		{
			gzprintf(outfile, "NA\n");
		}else
		{
			gzprintf(outfile, "\n");
		}
	}
}