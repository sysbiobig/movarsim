#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>

#include "pointMutation.h"

int loadPM(char *file_name,struct target_info *target_regions, struct chr_idx **chrMap)
{
	FILE *file = fopen(file_name,"r");
	char *seq,*tmp_seq,s[200],stmp[200];
	int i,len,r,pos;
	struct pm_info *pm_tmp;
	struct chr_idx *idx_tmp;

	if  (file==NULL)
	{
		printMessage("Point mutation file not present");
		exit(1);
	}

	int index = 0;
  	int control = 0;
  	int columns = 0;
  	char sep[] = "\t";
  	int line_numb = 1;
  	char *pch;
  	int pm_loaded = 0;

	char line [MAX_READ_BUFF];
	while(fgets(line,sizeof(line),file) != NULL )
	{
		if (control == 0)
		{
			for(i=0;i<strlen(line);i++)
			{
				if(strncmp(&line[i],sep,1)==0)
					columns++;
			}
			columns++;
			control = 1;
		}

		char *str_tokens[columns];

		pch = strtok(line,"\t");
		i=0;
		while (pch != NULL)
		{
			str_tokens[i++]=pch;
			pch = strtok(NULL,"\t");
		}

		if (i<6)
		{
		  fprintf(stderr,"ERROR: at line %d the number of columns is not correct.\n",line_numb);
		  exit(1);
		}

		pos = atoi(str_tokens[1]);
        if(pos<0)
		{
		    fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the position is not correct.\n",RED,RESET_COL,line_numb);
		    exit(1);
		}

        int chr_idx = getChrPos(str_tokens[0]);
		if(chr_idx<0)
		{
		    fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the chromosome is not correct. Human chromosomes should be specificed.\n",RED,RESET_COL,line_numb);
		    exit(1);
		}
		idx_tmp = chrMap[chr_idx];

		if(getIndexFromBase(*str_tokens[2])==4)
        {
            fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the specified base is not correct (valied entries are A, C, G or T).\n",RED,RESET_COL,line_numb);
		    exit(1);
        }

		if(atof(str_tokens[3])<0.0 || atof(str_tokens[3])>1.0)
		{
			fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the specified clonality should be in the range [0,1].\n",RED,RESET_COL,line_numb);
		    exit(1);
		}

		if(atoi(str_tokens[4])<0 || atoi(str_tokens[4])>2)
        {
            fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the specified allele is not correct (valid entries are 1 or 2).\n",RED,RESET_COL,line_numb);
		    exit(1);
        }

		if(atoi(str_tokens[5])<0)
        {
            fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the specified number of copies should be >0.\n",RED,RESET_COL,line_numb);
		    exit(1);
        }
		
		while(idx_tmp!=NULL)
		{
			r = idx_tmp->elem;
			if (strcmp(str_tokens[0],target_regions->info[r]->chr) == 0 && pos>=target_regions->info[r]->from && pos<=target_regions->info[r]->to)
			{
				pm_tmp = (struct pm_info *)malloc(sizeof(struct pm_info));

				pm_tmp->pos = (pos-target_regions->info[r]->from)-1;

				pm_tmp->chr = (char*)malloc(sizeof(char)*strlen(str_tokens[0])+1);
				strcpy(pm_tmp->chr,str_tokens[0]);
				subSlash(pm_tmp->chr);

				pm_tmp->base = (char*)malloc(sizeof(char)*strlen(str_tokens[2])+1);
        		strcpy(pm_tmp->base,str_tokens[2]);
	    		subSlash(pm_tmp->base);

				pm_tmp->clonality = atof(str_tokens[3]);
				pm_tmp->allele = atoi(str_tokens[4]);
			
				if(pm_tmp->allele==1)
				{
					pm_tmp->next = target_regions->info[r]->rdata->pm_a1;
					target_regions->info[r]->rdata->pm_a1 = pm_tmp;
				} else
				{
					pm_tmp->next = target_regions->info[r]->rdata->pm_a2;
					target_regions->info[r]->rdata->pm_a2 = pm_tmp;
				}

				pm_tmp->n_alleles = atof(str_tokens[5]);
	    		pm_loaded++;
	    		break;
			}
			idx_tmp = idx_tmp->next;
		}
	}
	return(pm_loaded);
}

void introducePMs(char s[], int init, int end, struct region_data *elem, dsfmt_t *dsfmt, double af)
{
	struct pm_info *pm_tmp;
	double randnumb;
	double allele;
	double clonality;
	double n_alleles;

	if(af<=elem->af_region)
	{
		pm_tmp = elem->pm_a1;
		while(pm_tmp!=NULL)
		{
			if(init<=pm_tmp->pos&&end>=pm_tmp->pos)
			{
				if(pm_tmp->allele==1)
				{
					randnumb = dsfmt_genrand_close_open(dsfmt);
					allele = 1.0;
					if(elem->cnv!=NULL)
						allele = elem->cnv->allele_a1;
					n_alleles = pm_tmp->n_alleles;
					if(n_alleles>allele)
						n_alleles = allele;
					clonality = pm_tmp->clonality;
					if(elem->cnv!=NULL)
						if(elem->cnv->cn_type==1)
							clonality=1;
					if(randnumb<=(n_alleles/allele)*clonality)
						*(s+(pm_tmp->pos-init)) = pm_tmp->base[0];
				}
			}
			pm_tmp = pm_tmp->next;
		}
	} else
	{
		pm_tmp = elem->pm_a2;
		while(pm_tmp!=NULL)
		{
			if(init<=pm_tmp->pos&&end>=pm_tmp->pos)
			{
				if(pm_tmp->allele==2)
				{
					
					randnumb = dsfmt_genrand_close_open(dsfmt);
					allele = 1.0;
					if(elem->cnv!=NULL)
						allele = elem->cnv->allele_a2;
					n_alleles = pm_tmp->n_alleles;
					if(n_alleles>allele)
						n_alleles = allele;
					clonality = pm_tmp->clonality;
					if(elem->cnv!=NULL)
						if(elem->cnv->cn_type==1)
							clonality=1;
					if(randnumb<=(n_alleles/allele)*clonality)
						*(s+(pm_tmp->pos-init)) = pm_tmp->base[0];
				}
			}
			pm_tmp = pm_tmp->next;
		}
	}
}