#ifndef MODEL_RDM_H
#include "struct.h"
#define MODEL_RDM_H

#ifdef __cplusplus
extern "C" {
#endif

void computeRDMCumulative(struct target_info *target_regions,int indexInit, int indexEnd, float adm);

void computeRDM(struct target_info *target_regions,int indexInit, int indexEnd, int offset);

void computeRDMCleanEnd(struct target_info *target_regions,int indexInit, int indexEnd, int offset);

void printRDM(gzFile *outfile, struct target_info *target_regions,int indexInit, int indexEnd);

int loadInsertSizeFromRDM(char *file_name,struct input_args *arguments);

void loadRDM(char *file_name,struct target_info *targets, struct input_args *arguments);

#ifdef __cplusplus
}
#endif

#endif
