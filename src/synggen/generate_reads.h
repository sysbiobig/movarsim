#ifndef GENERATE_H
#include "struct.h"
#define GENERATE_H

#if defined(__cplusplus)
extern "C" {
#endif

double drand();

double insert_size();

int getBSRandomTargetN(struct target_info *target_regions, dsfmt_t *dsfmt);

int getBSRandomTargetPosition(struct target_info *target_regions,int index, dsfmt_t *dsfmt);

int *introducePBE(char s[], int init, int end, struct target_t *elem, dsfmt_t *dsfmt, int rlen, int **qualities, int mbq, int bgprob, int seq_errors);

void *GenerateReads(void *args);

void reverseSequence(char *s);

void reverseComplement(char s[]);

#if defined(__cplusplus)
}
#endif

#endif
