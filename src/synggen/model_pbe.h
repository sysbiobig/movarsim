#ifndef MODEL_RDM_H
#include "struct.h"
#define MODEL_RDM_H

#ifdef __cplusplus
extern "C" {
#endif

void computePBE(struct target_info *target_regions,int indexInit, int indexEnd);

void loadPBE(char *file_name,struct target_info *targets);

void printPBE(gzFile *outfile, struct target_info *target_regions,int indexInit, int indexEnd, double bgprob);

#ifdef __cplusplus
}
#endif

#endif
