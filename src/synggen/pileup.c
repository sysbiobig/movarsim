#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

#include "pileup.h"
#include "model_rdm.h"
#include "model_pbe.h"
#include "model_qm.h"


// callback for bam_fetch()
static int fetch_func(const bam1_t *b, void *data)
{
    struct fetch_reads_t *buf_data = (struct fetch_reads_t *)data;
	unsigned char *qual;
	int i,low_qual;
	double delta;

	// calculate insert size stats
	if(buf_data->args->arguments->paired==1&&b->core.isize>0&&b->core.isize<500&&b->core.qual>20)
	{
		buf_data->args->ins_n++;
		delta = (b->core.isize-2*b->core.l_qseq)-buf_data->args->ins_mean;
		buf_data->args->ins_mean += delta/buf_data->args->ins_n;
		buf_data->args->ins_M2 += delta*((b->core.isize-2*b->core.l_qseq)-buf_data->args->ins_mean);
	}

	// create region specific quality matrix
	qual = bam1_qual(b);
	low_qual=0;
	for(i=0;i<b->core.l_qseq;i++)
	{
		if(i<buf_data->args->arguments->rlen)
		{
			buf_data->args->qualities[i][qual[i]]++;
		}
	}

    bam_plbuf_push(b,buf_data->buf);
    return 0;
}

// callback for bam_plbuf_init()
static int pileup_func(uint32_t tid, uint32_t pos, int n, const bam_pileup1_t *pl, void *data)
{
    int i,op,ol,strand;
    struct region_data *tmp = (struct region_data*)data;
    unsigned char *qual;
    int Ab,Cb,Gb,Tb,val,alt,sum,qual_index;

    if ((int)pos >= tmp->beg && (int)pos < tmp->end)
    {
    	Ab = Cb = Gb = Tb = 0;
        for(i=0; i<n; i++)
        {
    		qual = bam1_qual(pl[i].b);
		    if (!(pl[i].b->core.qual < tmp->arguments->mrq || pl[i].is_del != 0 || pl[i].indel != 0 || pl[i].is_refskip != 0 || (pl[i].b->core.flag & BAM_DEF_MASK)))
    		{

			    if((tmp->arguments->paired==0 && pl[i].b->core.pos==pos) ||
			   	   (tmp->arguments->paired==1 && pl[i].b->core.pos==pos && pl[i].b->core.pos<=pl[i].b->core.mpos && pl[i].b->core.tid==pl[i].b->core.mtid))
			    {
					tmp->positions[pos-tmp->beg].number++;
				}
			
	    		if(qual[pl[i].qpos] >= tmp->arguments->mbq)
	    		{
	  				val = (int)bam1_seqi(bam1_seq(pl[i].b),pl[i].qpos);
	  				if (val==1)
	  				   Ab++;
	  				else if (val==2)
	  				   Cb++;
	  				else if (val==4)
	  				   Gb++;
	  				else if (val==8)
	  				   Tb++;
    			}
      	 	}
        }

        alt = 0;
        
        if (tmp->sequence[pos-tmp->beg]=='A')
        	alt = Cb+Gb+Tb;
        if (tmp->sequence[pos-tmp->beg]=='C')
        	alt = Ab+Gb+Tb;
        if (tmp->sequence[pos-tmp->beg]=='G')
        	alt = Ab+Cb+Tb;
        if (tmp->sequence[pos-tmp->beg]=='T')
        	alt = Ab+Cb+Gb;

        sum = Ab+Cb+Gb+Tb;

	   if (sum>0&&(double)alt/(double)sum<=0.2)
	   {
	   	tmp->positions[pos-tmp->beg].A += Ab;
	   	tmp->positions[pos-tmp->beg].C += Cb;
	   	tmp->positions[pos-tmp->beg].G += Gb;
	   	tmp->positions[pos-tmp->beg].T += Tb;
	   }
	}
    return 0;
}

void *PileUp(void *args)
{
	struct args_thread *foo = (struct args_thread *)args;
	int i,j,r,ref,len;
	char s[500];
	char stmp[500];
	bam_plbuf_t *buf;

	samfile_t *in = samopen(foo->bam, "rb", 0);
	bam_index_t *idx = bam_index_load(foo->bam);

	for(r=foo->start;r<=foo->end;r++)
	{
		foo->target_regions->info[r]->rdata->beg = 0;
		foo->target_regions->info[r]->rdata->end = 0x7fffffff;
		foo->target_regions->info[r]->rdata->in = in;

		s[0] = '\0';

		sprintf(stmp,"%s",foo->target_regions->info[r]->chr);strcat(s,stmp);strcat(s,":");
		sprintf(stmp,"%d",foo->target_regions->info[r]->from+1);strcat(s,stmp);strcat(s,"-");
		sprintf(stmp,"%d",foo->target_regions->info[r]->to);strcat(s,stmp);

		bam_parse_region(foo->target_regions->info[r]->rdata->in->header,s,&ref,&(foo->target_regions->info[r]->rdata->beg),&(foo->target_regions->info[r]->rdata->end));
		foo->target_regions->info[r]->rdata->arguments = foo->arguments;

		if (ref < 0)
		{
			fprintf(stderr, "\n%sWARNING\xe2\x9d\x97%s: Invalid region %s\n",MAGENTA,RESET_COL,s);
			return 1;
		}

		buf = bam_plbuf_init(pileup_func,foo->target_regions->info[r]->rdata);
		struct fetch_reads_t *buf_data = (struct fetch_reads_t *)malloc(sizeof(struct fetch_reads_t));
		buf_data->buf = buf;
		buf_data->args = foo;
		bam_fetch(foo->target_regions->info[r]->rdata->in->x.bam,idx,ref,foo->target_regions->info[r]->rdata->beg,foo->target_regions->info[r]->rdata->end,buf_data,fetch_func);
		bam_plbuf_push(0,buf);
		bam_plbuf_destroy(buf);
		free(buf_data);
	}
	bam_index_destroy(idx);
	samclose(in);
}
