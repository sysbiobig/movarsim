import matplotlib.pyplot as plt
import pandas as pd
from matplotlib_venn import venn2
import csv
import seaborn as sns
import argparse



def read_vcf(file_path):
    variants = []
    with open(file_path, 'r') as file:
        for line in file:
            if line.startswith('#'):
                continue  # Skip header lines
            fields = line.strip().split('\t')
            chrom = fields[0]
            pos = fields[1]
            variants.append((chrom, pos))
    return variants

def read_vcf_err(file_path):
    variants = set()
    with open(file_path, 'r') as file:
        for line in file:
            if line.startswith('#'):
                continue  # Skip header lines
            fields = line.strip().split('\t')
            chrom = fields[0]
            pos = str(int(fields[1]) + 1)
            variants.add((chrom, pos))
    return variants


def main(args):

    simulated_vcf_files_paths = args.simulated_vcf_files
    merged = []
    merged_set= set()
    for vcf in simulated_vcf_files_paths:
        vcf_list =  read_vcf(vcf)
        merged_set = merged_set.union(set(vcf_list)) # remove duplicates

    merged.append(list(merged_set))
    merged= merged[0]
    with open("data/pipeline/complex/simulated.vcf", 'wt') as f:
        for l in merged:

            r = '\t'.join([l[0],l[1]])
            f.write(r + '\n')

    vcf1_path = args.vcf1
    vcf2_path = args.vcf2
    vcf3_path = args.vcf3


    # prefix = 'low_no_pbe'
    # vcf5_path = prefix + '.haplotypecaller_adj.vcf'
    # vcf6_path = prefix + '.unifiedgenotyper.vcf'
    # vcf7_path = prefix + '.freebayes.vcf'

    # prefix = 'low_no_rdm'
    # vcf8_path = prefix + '.haplotypecaller_adj.vcf'
    # vcf9_path = prefix + '.unifiedgenotyper.vcf'
    # vcf10_path = prefix + '.freebayes.vcf'

    vcf1_variants = read_vcf(vcf1_path)
    vcf2_variants = read_vcf(vcf2_path)
    vcf3_variants = read_vcf(vcf3_path)
    #vcf4_variants = read_vcf(vcf4_path)
    # vcf5_variants = read_vcf(vcf5_path)
    # vcf6_variants = read_vcf(vcf6_path)
    # vcf7_variants = read_vcf(vcf7_path)
    # vcf8_variants = read_vcf(vcf8_path)
    # vcf9_variants = read_vcf(vcf9_path)
    # vcf10_variants = read_vcf(vcf10_path)

    # fig, axes = plt.subplots(nrows=1, ncols=2,figsize=(20,20),dpi=300)
    # #fig.tight_layout()
    # #cols = ["HaplotypeCaller","Mutect2"]
    # # rows = ["Normal","No PBE","No RDM"]
    # #
    # v = venn2((merged_set, vcf1_variants),set_labels=["Simulated","Called"],set_colors=("g", "r"),alpha=0.5,ax=axes[0])
    # v.get_patch_by_id('11').set_color('gold')
    # for text in v.set_labels:
    #      text.set_fontsize(20)
    # for text in v.subset_labels:
    #      text.set_fontsize(20)
    # axes[0].legend(labels = ["FN","FP","TP"], fontsize=20,loc="upper left")
    # axes[0].set_title("HaplotypeCaller",fontsize=20)
    # #
    # v = venn2((merged_set, vcf2_variants),set_labels = ["Simulated", "Called"], set_colors = ("g", "r"), alpha = 0.5, ax = axes[1])
    # v.get_patch_by_id('11').set_color('gold')
    # for text in v.set_labels:
    #     text.set_fontsize(20)
    # for text in v.subset_labels:
    #      text.set_fontsize(20)
    # axes[1].legend(labels = ["FN","FP","TP"], fontsize=20,loc="upper left")
    # axes[1].set_title("Mutect2",fontsize=20)
    #
    # v = venn2((vcf1_variants, vcf4_variants),set_labels=["Simulated","Called"],set_colors=("g", "r"),alpha=0.5,ax=axes[0, 2])
    # v.get_patch_by_id('11').set_color('gold')
    # for text in v.set_labels:
    #     text.set_fontsize(20)
    # for text in v.subset_labels:
    #     text.set_fontsize(20)
    # axes[0,2].legend(labels = ["FN","FP","TP"], fontsize=20,loc="upper left")
    # #axes[0,2].set_title("Freebayes",fontsize=20)
    #
    # v = venn2((vcf1_variants, vcf5_variants),set_labels=["Simulated","Called"],set_colors=("g", "r"),alpha=0.5,ax=axes[1, 0])
    # v.get_patch_by_id('11').set_color('gold')
    # for text in v.set_labels:
    #     text.set_fontsize(20)
    # for text in v.subset_labels:
    #     text.set_fontsize(20)
    # axes[1,0].legend(labels = ["FN","FP","TP"], fontsize=20,loc="upper left")
    # #axes[1,0].set_title("HaplotypeCaller",fontsize=20)
    #
    # v = venn2((vcf1_variants, vcf6_variants),set_labels=["Simulated","Called"],set_colors=("g", "r"),alpha=0.5,ax=axes[1, 1])
    # v.get_patch_by_id('11').set_color('gold')
    # for text in v.set_labels:
    #     text.set_fontsize(20)
    # for text in v.subset_labels:
    #     text.set_fontsize(20)
    # axes[1,1].legend(labels = ["FN","FP","TP"], fontsize=20,loc="upper left")
    # #axes[1,0].set_title("UnifiedGenotyper",fontsize=20)
    #
    # v = venn2((vcf1_variants, vcf7_variants),set_labels=["Simulated","Called"],set_colors=("g", "r"),alpha=0.5,ax=axes[1, 2])
    # v.get_patch_by_id('11').set_color('gold')
    # for text in v.set_labels:
    #     text.set_fontsize(20)
    # for text in v.subset_labels:
    #     text.set_fontsize(20)
    # axes[1,2].legend(labels = ["FN","FP","TP"], fontsize=20,loc="upper left")
    # #axes[1,0].set_title("Freebayes",fontsize=20)
    #
    # v = venn2((vcf1_variants, vcf8_variants),set_labels=["Simulated","Called"],set_colors=("g", "r"),alpha=0.5,ax=axes[2, 0])
    # v.get_patch_by_id('11').set_color('gold')
    # for text in v.set_labels:
    #     text.set_fontsize(20)
    # for text in v.subset_labels:
    #     text.set_fontsize(20)
    # axes[2,0].legend(labels = ["FN","FP","TP"], fontsize=20,loc="upper left")
    # #axes[1,0].set_title("HaplotypeCaller",fontsize=20)
    #
    # v = venn2((vcf1_variants, vcf9_variants),set_labels=["Simulated","Called"],set_colors=("g", "r"),alpha=0.5,ax=axes[2, 1])
    # v.get_patch_by_id('11').set_color('gold')
    # for text in v.set_labels:
    #     text.set_fontsize(20)
    # for text in v.subset_labels:
    #     text.set_fontsize(20)
    # axes[2,1].legend(labels = ["FN","FP","TP"], fontsize=20,loc="upper left")
    # #axes[1,0].set_title("UnifiedGenotyper",fontsize=20)
    #
    # v = venn2((vcf1_variants, vcf10_variants),set_labels=["Simulated","Called"],set_colors=("g", "r"),alpha=0.5,ax=axes[2, 2])
    # v.get_patch_by_id('11').set_color('gold')
    # for text in v.set_labels:
    #     text.set_fontsize(20)
    # for text in v.subset_labels:
    #     text.set_fontsize(20)
    # axes[2,2].legend(labels = ["FN","FP","TP"], fontsize=20,loc="upper left")
    # #axes[1,0].set_title("Freebayes",fontsize=20)
    #
    # for ax in axes.flatten():
    #     ax.set_axis_on()
    #     ax.spines['top'].set_visible(False)
    #     ax.spines['right'].set_visible(False)
    #     ax.spines['left'].set_visible(False)
    #     ax.spines['bottom'].set_visible(False)
    #
    #
    # for ax, col in zip(axes[0,:], cols):
    #     if col == "HaplotypeCaller":
    #         ax.annotate(col, xy=(0.5, 1), xytext=(0, 4),
    #                     xycoords='axes fraction', textcoords='offset points',
    #                     size=30, ha='center',va='baseline')
    #     elif col == "UnifiedGenotyper":
    #         ax.annotate(col, xy=(0.5, 1), xytext=(0, 4),
    #                     xycoords='axes fraction', textcoords='offset points',
    #                     size=30, ha='center', va='baseline')
    #     else:
    #         ax.annotate(col, xy=(0.5, 1), xytext=(0, 50),
    #                     xycoords='axes fraction', textcoords='offset points',
    #                     size=30, ha='center', va='baseline')
    # fig.subplots_adjust(left=0.05, top=0.95)
    #
    #
    # for ax, row in zip(axes[:,0], rows):
    #     ax.annotate(row, xy=(0, 0.5), xytext=(-ax.yaxis.labelpad - 5, 0),
    #                 xycoords=ax.yaxis.label, textcoords='offset points',
    #                 size=30, ha='right', va='center',rotation=90)

    #plt.savefig('data/pipeline/complex/my_plot.png')

    # exit()
    cov_path = args.cov
    cov1_path = args.cov1
    cov2_path = args.cov2
    cov3_path = args.cov3


    data = dict.fromkeys(merged, '0')
    with open(cov_path, 'r') as input:
        reader = csv.reader(input, delimiter='\t')
        next(reader)

        for i, row in enumerate(reader):
            key = (row[0],row[1])
            value = row[3]
            data[key] = value

    with open(cov1_path, 'r') as input:
        reader = csv.reader(input, delimiter='\t')
        next(reader)

        for i, row in enumerate(reader):
            key = list(vcf1_variants)[i]
            value = row[3]
            data[key] = value

    with open(cov2_path, 'r') as input:
        reader = csv.reader(input, delimiter='\t')
        next(reader)

        for i, row in enumerate(reader):
            key = list(vcf2_variants)[i]
            value = row[3]
            data[key] = value

    with open(cov3_path, 'r') as input:
        reader = csv.reader(input, delimiter='\t')
        next(reader)

        for i, row in enumerate(reader):
            key = list(vcf3_variants)[i]
            value = row[3]
            data[key] = value

    common_variants_hc = list(set(merged).intersection(set(vcf1_variants)))
    common_variants_mt = list(set(merged).intersection(set(vcf2_variants)))
    common_variants_st = list(set(merged).intersection(set(vcf3_variants)))

    TP_hc = len(common_variants_hc)  # Called & Simulated
    FP_hc = len(vcf1_variants) - len(common_variants_hc)  # Called - Simulated
    FN_hc = len(merged) - len(common_variants_hc)  # Simulated - Called

    TP_mt = len(common_variants_mt)  # Called & Simulated
    FP_mt = len(vcf2_variants) - len(common_variants_mt)  # Called - Simulated
    FN_mt = len(merged) - len(common_variants_mt)  # Simulated - Called

    TP_st = len(common_variants_st)  # Called & Simulated
    FP_st = len(vcf3_variants) - len(common_variants_st)  # Called - Simulated
    FN_st = len(merged) - len(common_variants_st)  # Simulated - Called

    coverage_common_hc = []
    coverage_FN_hc = []
    coverage_FP_hc = []

    coverage_common_mt = []
    coverage_FN_mt = []
    coverage_FP_mt = []

    coverage_common_st = []
    coverage_FN_st = []
    coverage_FP_st = []

    for variant in merged:
        if variant in common_variants_hc:
            coverage_common_hc.append(float(data[variant]))
        else:
            coverage_FN_hc.append(float(data[variant]))

        if variant in common_variants_mt:
            coverage_common_mt.append(float(data[variant]))
        else:
            coverage_FN_mt.append(float(data[variant]))

        if variant in common_variants_st:
            coverage_common_st.append(float(data[variant]))
        else:
            coverage_FN_st.append(float(data[variant]))

    for variant in vcf1_variants:
        if not variant in common_variants_hc:
            #print("FP in HC : {}".format(variant))
            coverage_FP_hc.append(float(data[variant]))

    for variant in vcf2_variants:
        if not variant in common_variants_mt:
            # print("FP in UG : {}".format(variant))
            coverage_FP_mt.append(float(data[variant]))
    for variant in vcf3_variants:
        if not variant in common_variants_st:
            # print("FP in FB : {}".format(variant))
            coverage_FP_st.append(float(data[variant]))

    coverages_hc = coverage_FN_hc + coverage_common_hc + coverage_FP_hc
    categories_hc = ["FN"] * len(coverage_FN_hc) + ["TP"] * len(coverage_common_hc) + ["FP"] * len(coverage_FP_hc)
    d = {"Category": categories_hc, "Coverages": coverages_hc}
    df_hc = pd.DataFrame(d)

    coverages_mt = coverage_FN_mt + coverage_common_mt + coverage_FP_mt
    categories_mt = ["FN"] * len(coverage_FN_mt) + ["TP"] * len(coverage_common_mt) + ["FP"] * len(coverage_FP_mt)
    d = {"Category": categories_mt, "Coverages": coverages_mt}
    df_mt = pd.DataFrame(d)

    coverages_st = coverage_FN_st + coverage_common_st + coverage_FP_st
    categories_st = ["FN"] * len(coverage_FN_st) + ["TP"] * len(coverage_common_st) + ["FP"] * len(coverage_FP_st)
    d = {"Category": categories_st, "Coverages": coverages_st}
    df_st = pd.DataFrame(d)

    fig, axes = plt.subplots(nrows=2, ncols=3, figsize=(15, 10), dpi=300)

    v = venn2((set(merged), set(vcf1_variants)), set_labels=["Simulated", "Called"], set_colors=("g", "r"), alpha=0.5,
               ax=axes[0, 0])
    v.get_patch_by_id('11').set_color('gold')
    for text in v.set_labels:
        text.set_fontsize(15)
    for text in v.subset_labels:
        text.set_fontsize(15)
    axes[0, 0].legend(labels=["FN", "FP", "TP"], fontsize="large", loc="upper left")
    axes[0, 0].set_title("HaplotypeCaller", fontsize=15)
    #
    axes[1, 0].set_title("HaplotypeCaller", fontsize=15)
    # #axes[1, 0].set_ylim(-1, 25)
    sns.stripplot(df_hc, x=df_hc["Category"], y=df_hc["Coverages"], ax=axes[1, 0], jitter=0.4, size=3,
                  hue=df_hc["Category"], palette=["g", "gold", "r"])
    # sns.boxplot(showmeans=True,
    # meanline=True,
    # meanprops={'color': 'k', 'ls': '-', 'lw': 2},
    # medianprops={'visible': False},
    # whiskerprops={'visible': False},
    # zorder=10,
    # data=df_hc,
    # x=df_hc["Category"],
    # y=df_hc["Coverages"],
    # showfliers=False,
    # showbox=False,
    # showcaps=False,
    # ax=p)
    # sns.violinplot(df_hc,x=df_hc["Category"],y=df_hc["Coverages"],ax=axes[1,0],cut=0,bw_adjust=0.5,density_norm="width")
    # sns.violinplot(x=df_hc["Category"], y=df_hc["Coverages"], data=pd.melt((df_hc - df_hc.mean()) / df_hc.std()),ax=axes[1,0],cut=0,density_norm="width")
    # sns.swarmplot(x =df_hc["Category"], y =df_hc["Coverages"], data = df_hc,color= "white",ax=axes[1,0])
    axes[1, 0].set_ylabel("Coverage", fontsize=15)
    axes[1, 0].set_xlabel("")
    axes[1, 0].tick_params(axis='both', which='major', labelsize="large")
    # # axes[1, 0].set_yscale('log')
    # # semilogy([x], y, [fmt], data=None, **kwargs)
    #
    v = venn2((set(merged), set(vcf2_variants)), set_labels=["Simulated", "Called"], set_colors=("g", "r"), alpha=0.5,
              ax=axes[0, 1])
    v.get_patch_by_id('11').set_color('gold')
    for text in v.set_labels:
        text.set_fontsize(15)
    for text in v.subset_labels:
        text.set_fontsize(15)
    axes[0, 1].legend(labels=["FN", "FP", "TP"], fontsize="large", loc="upper left")
    axes[0, 1].set_title("Mutect2", fontsize=15)
    #
    axes[1, 1].set_title("Mutect2", fontsize=15)
    # #axes[1, 1].set_ylim(-1, 25)
    sns.stripplot(df_mt, x=df_mt["Category"], y=df_mt["Coverages"], ax=axes[1, 1], jitter=0.4, size=3,
                  hue=df_mt["Category"], palette=["g", "gold", "r"])
    axes[1, 1].set_ylabel("Coverage", fontsize=15)
    axes[1, 1].set_xlabel("")
    axes[1, 1].tick_params(axis='both', which='major', labelsize="large")
    #
    v = venn2((set(merged), set(vcf3_variants)), set_labels=["Simulated", "Called"], set_colors=("g", "r"), alpha=0.5,
              ax=axes[0, 2])
    v.get_patch_by_id('11').set_color('gold')
    for text in v.set_labels:
        text.set_fontsize(15)
    for text in v.subset_labels:
        text.set_fontsize(15)
    axes[0, 2].legend(labels=["FN", "FP", "TP"], fontsize="large", loc="upper left")
    axes[0, 2].set_title("Strelka", fontsize=15)
    #
    axes[1, 2].set_title("Strelka", fontsize=15)
    # #axes[1, 2].set_ylim(-1, 25)
    sns.stripplot(df_st, x=df_st["Category"], y=df_st["Coverages"], ax=axes[1, 2], jitter=0.4, size=3,
                  hue=df_st["Category"], palette=["g", "gold", "r"])
    axes[1, 2].set_ylabel("Coverage", fontsize=15)
    axes[1, 2].set_xlabel("")
    axes[1, 2].tick_params(axis='both', which='major', labelsize="large")
    #
    # plt.tight_layout()
    # plt.savefig(fname = "data/comparison.png",dpi=600,format="png")
    plt.savefig('data/pipeline/complex/my_plot.png',dpi=600,format="png")
    # # num_count = 0
    # #
    # # with open("root_het_1000_breast_cosmic_0.rdm", 'r') as file:  # RDM mod
    # #     for line in file:
    # #         numbers = line.strip().split(';')
    # #         num_count = num_count + len(numbers) - 1  # perché il primo numero dell'RDM non rappresenta una posizione
    #
    # # TN_hc = num_count - TP_hc - FP_hc - FN_hc
    # # TN_ug = num_count - TP_ug - FP_ug - FN_ug
    # # TN_fb = num_count - TP_fb - FP_fb - FN_fb
    #
    TPR_hc = TP_hc / (TP_hc + FN_hc)
    FDR_hc = FP_hc / (FP_hc + TP_hc)
    #
    TPR_mt = TP_mt / (TP_mt + FN_mt)
    FDR_mt = FP_mt / (FP_mt + TP_mt)
    #
    TPR_st = TP_st / (TP_st + FN_st)
    FDR_st = FP_st / (FP_st + TP_st)
    #
    # print("TP of HC: {}".format(TP_hc))
    # print("FP of HC: {}".format(FP_hc))
    # #print("TN of HC: {}".format(TN_hc))
    # print("FN of HC: {}".format(FN_hc))
    print("TPR of HC: {}".format(TPR_hc))
    print("FDR of HC: {}".format(FDR_hc))
    #
    # print("TP of UG: {}".format(TP_ug))
    # print("FP of UG: {}".format(FP_ug))
    # #print("TN of UG: {}".format(TN_ug))
    # print("FN of UG: {}".format(FN_ug))
    print("TPR of MT: {}".format(TPR_mt))
    print("FDR of MT: {}".format(FDR_mt))
    #
    # print("TP of FB: {}".format(TP_fb))
    # print("FP of FB: {}".format(FP_fb))
    # #print("TN of FB: {}".format(TN_fb))
    # print("FN of FB: {}".format(FN_fb))
    print("TPR of ST: {}".format(TPR_st))
    print("FDR of ST: {}".format(FDR_st))



def run():
    parser = argparse.ArgumentParser(description="Compare real VCF vs simulated VCF")

    parser.add_argument("simulated_vcf_files", nargs='+', help="List of simulated VCF files of different clones")

    parser.add_argument('-v1', '--vcf1', dest='vcf1',
                        help="VCF by HaplotypeCaller")
    parser.add_argument('-v2', '--vcf2', dest='vcf2',
                         help="VCF by Mutect2")
    parser.add_argument('-v3', '--vcf3', dest='vcf3',
                        help="VCF by Strelka")

    parser.add_argument('-c', '--cov', dest='cov',
                        help="Coverages at simulated sites")
    parser.add_argument('-c1', '--cov1', dest='cov1',
                        help="Coverages at sites called by HaplotypeCaller")
    parser.add_argument('-c2', '--cov2', dest='cov2',
                        help="Coverages at sites called by Mutect2")
    parser.add_argument('-c3', '--cov3', dest='cov3',
                        help="Coverages at sites called by Strelka")


    args = parser.parse_args()
    main(args)

if __name__ == '__main__':
    run()
