import csv
import argparse
import pybedtools
import pysam

def sort_bed_matrix(bed_matrix,custom_chromosome_order):

    # Order based on the order found in the input file
    chromosome_order_dict = {chrom: index for index, chrom in enumerate(custom_chromosome_order)}

    # Order first by chr and then by starting pos
    sorted_matrix = sorted(bed_matrix, key=lambda x: (chromosome_order_dict.get(x[0], float('inf')), int(x[1])))

    return pybedtools.BedTool(sorted_matrix)

def main(args):
    bed_path = args.bed
    to_be_extended= args.to_be_extended
    rlen=args.rlen
    seqprot=args.seqprot
    reference_genome=args.reference_genome

    if to_be_extended=="y":

        bed = []
        with open(bed_path, 'r') as f:
            for line in f:
                bed.append(line.strip().split())

        extended_bed = []
        for i, region in enumerate(bed):
            chr = region[0]
            start = int(region[1])
            end = int(region[2])
            if int(seqprot) == 1: # Vuol dire paired-end
                extended_bed.append([chr, start - 2*int(rlen), end + 2*int(rlen)])
            else:
                extended_bed.append([chr, start - int(rlen), end + int(rlen)])

        py_bed = pybedtools.BedTool('\n'.join(['\t'.join([str(c) for c in reg]) for reg in extended_bed]), from_string=True)
        fasseq = pysam.Fastafile(reference_genome)
        seqnames = [fasseq.references[i] for i in range(0, fasseq.nreferences)]
        genome_bed = [[fasseq.references[i], 0, fasseq.lengths[i]] for i in range(len(seqnames))]
        py_bed_genome = pybedtools.BedTool('\n'.join(['\t'.join([str(c) for c in reg]) for reg in genome_bed]), from_string=True)
        final_bed = py_bed.intersect(py_bed_genome)
        final_bed = sort_bed_matrix(final_bed, seqnames).merge()
        sizes= []
        for region in final_bed:
            end = region[2]
            start = region[1]
            length = int(end) - int(start)
            sizes.append(length)

        print(sum(sizes))


    else:
        sizes = []
        with open(bed_path, 'r') as input:
            reader = csv.reader(input, delimiter='\t')

            for row in reader:
                end = row[2]
                start = row[1]
                length = int(end) - int(start)
                sizes.append(length)

        print(sum(sizes))




def run():
    parser = argparse.ArgumentParser(description="Calculate genome size from a BED")

    parser.add_argument('-b', '--bed', dest='bed',
                        help="Input BED")
    parser.add_argument('-e', '--extension', dest='to_be_extended', choices=['y','n'],
                        help="Specify whether the BED in input should be extended or not (y or n)")
    parser.add_argument('-rlen', '--read_length', dest='rlen',
                        help="If to_be_extended is y, specify the read length (the number of bases for the extension)")
    parser.add_argument('-seqprot', '--sequencing_protocol', dest='seqprot', choices=['0','1'],
                        help="If to_be_extended is y, specify the sequencing protocol (if 0, single-end -> 1*rlen, if 1, paired-end -> 2*rlen)")
    parser.add_argument('-g', '--reference_genome', dest='reference_genome',
                        help="If to_be_extended is y, specify the reference genome")


    args = parser.parse_args()
    main(args)

if __name__ == '__main__':
    run()

