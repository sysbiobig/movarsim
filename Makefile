all: dynamic

LIBRARIES = -lbcf -lbam -lm -lz -lpthread
LIBRARIESDIR = lib/
INCLUDESDIR = include/
CC = gcc
CFLAGS = -g -w -Wall -O2
APPNAME = synggen

dSFMT.o: src/synggen/dSFMT.c src/synggen/dSFMT.h
	$(CC) -I$(INCLUDESDIR) -DDSFMT_MEXP=216091 -c src/synggen/dSFMT.c

dynamic: dSFMT.o src/synggen/main.c
	$(CC) $(CFLAGS) -I$(INCLUDESDIR) dSFMT.o src/synggen/main.c src/synggen/copy_number.c src/synggen/generate_reads.c src/synggen/input_param.c src/synggen/load_data.c src/synggen/model_pbe.c src/synggen/model_qm.c src/synggen/model_rdm.c src/synggen/pileup.c src/synggen/pointMutation.c src/synggen/SNP_model.c src/synggen/struct.c -o $(APPNAME) -L$(LIBRARIESDIR) $(LIBRARIES)

static: dSFMT.o src/synggen/main.c
	$(CC) $(CFLAGS) -I$(INCLUDESDIR) dSFMT.o src/synggen/main.c src/synggen/copy_number.c src/synggen/generate_reads.c src/synggen/input_param.c src/synggen/load_data.c src/synggen/model_pbe.c src/synggen/model_qm.c src/synggen/model_rdm.c src/synggen/pileup.c src/synggen/pointMutation.c src/synggen/SNP_model.c src/synggen/struct.c -o $(APPNAME) -L$(LIBRARIESDIR) $(LIBRARIES) -static

clean:
	rm -f *.o
