FROM ubuntu:18.04

ARG DEBIAN_FRONTEND=noninteractive

# Install dependencies for R compilation
RUN apt-get -qq update && \
    apt-get -y install python3.8 && \
    apt-get -y install python3.8-dev && \
    apt-get -y install python3-venv && \
    apt-get -y install python3-pip

RUN apt-get -y install wget  && \
    apt-get -y install curl  && \
    apt-get -y install gfortran && \
    apt-get -y install git  && \
    apt-get -y install tzdata  && \
    apt-get -y install openjdk-11-jdk && \
    apt-get -y install bedtools  && \
    apt-get -y install unzip  && \
    apt-get -y install autoconf  && \
    apt-get -y install libcurl4-openssl-dev  && \
    apt-get -y install libssl-dev  && \
    apt-get -y install libxml2-dev  && \
    apt-get -y install libreadline-dev  && \
    apt-get -y install libbz2-dev && \
    apt-get -y install liblzma-dev && \
    apt-get -y install libpcre2-dev && \
    apt-get -y install libpng-dev && \
    apt-get -y install libjpeg-dev && \
    apt-get -y install libncurses5-dev && \
    apt-get -y install libcairo2-dev && \
    apt-get -y install zlib1g-dev && \
    apt-get -y install cmake && \
    apt-get -y install bc

RUN update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.6 1
RUN update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.8 2

WORKDIR /home/movarsim

# Install dependencies:
COPY requirements.txt .
RUN python3 -m pip install --upgrade pip && \
    python3 -m pip install -r requirements.txt

### Install other dependencies
WORKDIR /tmp

# Download and compile R 4.3.1
RUN wget https://cran.r-project.org/src/base/R-4/R-4.3.1.tar.gz && \
    tar -xf R-4.3.1.tar.gz && \
    cd R-4.3.1 && \
    ./configure --with-x=no && \
    make && \
    make install && \
    rm -rf R-4.3.1 R-4.3.1.tar.gz

# Install R packages
RUN R -e "install.packages('rSHAPE',repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('stringr',repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('fitdistrplus',repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('magrittr',repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('dplyr',repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('patchwork',repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('jpeg',repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('png',repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('gridtext',repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('ggtext',repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('ggplot2',repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('tidyr',repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('Cairo',repos='http://cran.rstudio.com/')"

# Install Samtools 1.10
RUN wget https://github.com/samtools/samtools/releases/download/1.10/samtools-1.10.tar.bz2 && tar -xjf samtools-1.10.tar.bz2 && \
    cd samtools-1.10 && \
    ./configure && \
    make && \
    make install && \
    cd .. && \
    rm -rf samtools-1.10 samtools-1.10.tar.bz2

# Install Nextflow
RUN curl -s https://get.nextflow.io | bash && \
chmod +x nextflow && \
mv nextflow /bin/

# Install GATK v4.1.4.1
RUN apt-get update && apt-get install -y python
RUN wget https://github.com/broadinstitute/gatk/releases/download/4.1.4.1/gatk-4.1.4.1.zip && \
    unzip gatk-4.1.4.1.zip && \
    mv gatk-4.1.4.1 /opt/gatk && \
    ln -s /opt/gatk/gatk /usr/local/bin/gatk && \
    rm gatk-4.1.4.1.zip

# Copy directory
WORKDIR /home/movarsim
COPY . .

# Compile synggen
RUN make -f Makefile
RUN mv synggen /bin/
