# MOV&RSim: computational Modelling Of cancer-specific Variants And sequencing Reads characteristics for realistic tumoral sample Simulation 

To overcome the limitations of state-of-art somatic sample simulation
we developed MOV&RSim, a novel
simulator that leverages data-driven information to set both variants and reads
characteristics, producing realistic tumoral samples. 

MOV&RSim leverages on
cancer-specific presets derived from COSMIC and TCGA
to inform the simulator's parameters for 21 cancer types.
This new simulator, containerised with Docker and freely available
for academic use, empowers users to define each biological parameter 
of a tumoral genome and faithfully replicates the variability of technical
noise observed in real sequencing reads. 

The proposed simulator and presets 
represent the most adaptable and comprehensive framework currently available
for generating tumoral samples to benchmark and, ultimately, optimise somatic 
variant calling pipelines. 
## Structure

The complete project structure is shown and described below:

```graphql
.                                                      # Project folder
├── data/                                              # Data folder
│   ├── bams/                                          # BAM files for learning reads characteristics
│   │   ├── HG00138.bam                                
│   │   ├── HG00138.bai
│   │   ├── HG00139.bam
│   │   ├── HG00139.bai
│   │   ├── HG00140.bam
│   │   ├── HG00140.bai
│   │   └── ...
│   ├── out                                             # Synggen mode 0 outputs
│   ├── pipeline                                        # Variant calling results
│   ├── pipeline_testing                                # Variant calling working directory
│   │   ├── example.R1.fastq.gz                         
│   │   ├── example.R2.fastq.gz                         
│   │   └── samplesheet.csv                             # Manifest containing the path of R1 and R2
│   ├── reads                                           # Synggen mode 1 outputs
│   ├── cosmic_core_mutations_workspace_breast.RData    # Presets for setting variant characteristics
│   ├── cosmic_core_mutations_workspace_lung.RData   
│   ├── cosmic_core_mutations_workspace_colon.RData   
│   ├── ...
│   ├── tcga_mutations_workspace_breast.RData  
│   ├── tcga_mutations_workspace_breast.RData                                                                                                                     
│   ├── tcga_mutations_workspace_breast.RData                                                                                                                     
│   ├── ...                                                                                                                     
│   ├── hg38.fasta                                      # Genome
│   ├── hg38.fasta.fai
│   ├── hg38.dict
│   ├── template.fna                                    # Toy Genome                                                                                                                  
│   ├── template.fna.fai
│   ├── listOfBams.txt                                  # List of BAMs contained in bams directory
│   ├── template.fna.fai                                                                                                                     
│   ├── template.fna.fai                                                                                                                                                          
│   └── tree.txt                                        # Clonal tree
│
├── include/                                            # Include folder
│   └── ...
│
├── doc/                                                # Documentation folder
│   └── ...
│
├── lib/                                                # Library folder
│   └── ...
│
├── src/                                                # Source folder
│   ├── presets/                                        
│   │   ├── varfile_builder.R                           # Interactive mode for defining variants
│   │   ├── varfile_builder_bash.R                      # Automatic mode for defining variants
│   │   ├── varfile_analysis.R                          # Script for VAR files analysis
│   │   ├── cosmic_core_mutations.R                     # Script for mining COSMIC
│   │   ├── tcga_mutations.R                            # Script for mining TCGA
│   │   └── ...       
│   ├── synggen/                                        # Synggen folder
│   │   └── ...  
│   ├── compute_genome_size.py                          # Calculate the size of the genome of interest
│   ├── converter.py                                    # Convert Synggen BED and models information
│   ├── create_RDM.py                                   # Create fake RDM for even coverage
│   ├── genome_editing.py                               # Genome editing script
│   └── compare_vcf.py                                  # Evaluate variant calling results
│        
├── Makefile                                                                              
├── requirements.txt                                # Required packages
└── README.md                                       # README
```

### Description

MOV&RSim contains two main folders:

1. `src/`: contains the source code of MOV&RSim
2. `data/`: contains input/output files needed/produced by/with each MOV&RSim step

The presets are available in [Zenodo](https://doi.org/10.5281/zenodo.11453104). Also, raw data used to derive the presets are stored [here](https://doi.org/10.5281/zenodo.11458637).




MOV&RSim comprises 3 steps, with the third step further divided into 3 modules:
1. **Defining Variants**.


2. **Editing Genome**.


3. **Generating Reads**.
    - Synggen mode 0
    - Converter 
    - Synggen mode 1

<div align="center">
<img alt="MOV\&RSim simulation workflow, input/output files and formats." src="data/var_comparison.png" width="600">
</div>

## Getting started

### Run in Docker container (suggested)

In order to run MOV&RSim in provided Docker container, you can follow these steps:

#### Prerequisites

Make sure you installed **Docker** - [Download & Install Docker](https://docs.docker.com/get-docker/)

#### Project folder setup

```shell
mkdir <project-folder>                                        # Create a project folder
cd <project-folder>                                           # Move to the project folder

mkdir data                                                    # Create a local data/ folder
```

#### Get the Docker container

```
# Pull the docker container
docker pull registry.gitlab.com/sysbiobig/movarsim               

# Run MOV&RSim inside the docker container
docker run -v $(pwd)/data:/home/movarsim/data \               # Bind the data/ folder
           -ti registry.gitlab.com/sysbiobig/movarsim         # Set the container
```

### Run locally

##### Prerequisites

Make sure you have installed all the following prerequisites on your machine:

* **Git** - [Download & Install Git](https://git-scm.com/downloads).
* **Python 3.8** - [Download & Install Python](https://www.python.org/downloads/)
* The **Python packages** in [requirements.txt](requirements.txt)
* **R 4.3.1** - [Download & Install R](https://cran.r-project.org)
* The following **R packages**:
  * rSHAPE
  * stringr
  * fitdistrplus
  * magrittr
  * dplyr
  * patchwork
  * jpeg
  * png
  * gridtext
  * ggtext
  * ggplot2
  * tidyr
  * Cairo
* **GNU Make** - [Download & Install GNU Make](https://www.gnu.org/software/make/)
* **Nextflow** - [Download & Install Nextflow](https://www.nextflow.io/)
* **GATK v4.1.4.1** - [Download & Install GATK](https://github.com/broadinstitute/gatk/)
* **Samtools 1.10** - [Download & Install Samtools](https://github.com/samtools/)


#### Project folder setup

```shell
# Clone the repository
git clone git@gitlab.com:sysbiobig/movarsim.git <project-folder>  

# Move to the project folder
cd <project-folder>

# Compile Synggen 
make                                           
```

## Run the simulator

Now you can run the 3 MOV&RSim steps through the following commands.

> **N.B.** The `data/` folder should contain:
>* The presets from COSMIC and TCGA databases for each tumor type. You can find them on Zenodo at [https://doi.org/10.5281/zenodo.11453104](https://doi.org/10.5281/zenodo.11453104).
>* The template genome you want to use, e.g. *hg38.fasta* (real reference genome) or *template.fna* (toy genome).
>* The VAR file you want to use (if you want to use your own VAR file, instead of generating it with the guided procedure), e.g. *prova.var*.
>* The subdirectory `bams/`, containing BAM files of real samples mapped to the template genome you want to use (if you wanto to use *hg38.fasta*, then you can download CRAM files of samples from the 1000Genomes project [here](https://www.internationalgenome.org/data-portal/sample/) and convert them to BAM using samtools. See the exact command below.)
>* The file *listOfBams.txt*, containing the list of all the BAM file in `bams/` that you want to consider for the simulation. The format should be:
>   *  `./data/bams/1.bam `
>   * `./data/bams/2.bam`
>   * `./data/bams/3.bam`
>   * ...
>* The file containing the clonal tree architecture, e.g. *tree.txt*. The format should be:
>   * child1_name, child1_distance_from_parent, child1_parent_name
>   * child2_name, child2_distance_from_parent, child2_parent_name
>   * ...

>  All parent names must be listed as a separate child, i.e. if *child2_parent_name* is *child1_name*, then *child1_name* must also be listed as a child with its own *child1_parent_name*. The exception to this is when the parent clone is ‘root’, 
>and this must occur at least once. Optionally, another column might be added containing the percentage of heterozygous variants among the variants that differentiate each child from its parent.

### STEP 1: Defining Variants (i.e. the optional guided procedure to compile VAR files)

This step produces the main input file of MOV&RSim, i.e. the VAR file which is the list of variants 
that should be present in the genome of the final simulated sample. This step consists in a guided procedure that can run interactively or automatically.
With this procedure the user can choose to set variant characteristics (total number of variant in the sample, 
number of variants per type, variant zigosity, variant lengths, and variant positions) using cancer-specific
presets derived from COSMIC and TCGA data analysis. The presets contain statistical information about 21 cancer types (breast, lung, colon, adrenal gland, biliary tract, bone, cervix, eye, kidney, liver, oesophagus, ovary, pancreas, pleura, prostate, skin, soft tissue, stomach, testis, thymus, thyroid).
The presets should be in the folder `data/`. Also, this procedure optionally takes as input a clonal tree, which should be defined by the user in a .txt file and positioned in `data/`.

#### Interactive mode

```shell
# From the project folder
R                                                  # Start R
```
```R
source("$(pwd)/src/preset/varfile_builder.R")      # Run the guided procedure                                
```
#### Automatic mode
```shell
# From the project folder
Rscript  $(pwd)/src/preset/varfile_builder_bash.R 
         --tumor_type=breast                       # Select the tumor type you want to simulate
         --database=tcga                           # Select the database you want to rely on
         --ref_genome=hg38.fasta                   # Select the reference genome you want to use (should be in data/)
         --tree=tree.txt                           # (optional) Select the clonal tree architecture (should be in data/)
         --total_variants=10                       # (optional) Select the total number of variants you want to simulate for the root sample
         --het_percentage=1                        # (optional) Select the heterozigosity percentage for the root sample
```

### STEP 2: Editing Genome

This step takes as input one VAR file for each sample/clone the user might want to simulate.
It spikes the variants defined in the VAR file into a user-specified template genome used as scaffold. Variants are inserted in the order
specified in the VAR file. Overlapping variants are managed following precise rules for insertions and deletions events. 
In serial runs, this step requires ~50GB for each simulated sample/clone. Also, it takes ~1h to simulate 1000 variants in the VAR file and ~8h to simulate 10000.
This step generates 
   - 1 file with extension .vcf for each sample/clone, containing ground-truth variants. Variants that mimics the content of the template genome (invisible variants) can be flagged.
   - 1 file with extension .bed for each sample/clone, containing the coordinates of regions altered by variants, extended at both ends by a user-specified number of nucleotides.
   - 1 file with extension .pkl for each haploid (by default each sample/clone has 2 haploids) in each sample/clone, containing the map that associates the position of each nucleotide of the template genome to its position in the final altered genome.
   - 1 file with extension .fa for each haploid in each sample/clone, containing the altered genome.

```shell
# From the project folder
python3  $(pwd)/src/genome_editing.py 
         --varfiles $(pwd)/data/root.var           # The VAR file you want to use, generated manually or with the guided procedure
         --hapfiles $(pwd)/data/hg38.fasta         # The reference genome
         --tmpdir                                  # (optional) Temporary directory (default: "/home/")
         ---nprocs                                 # (optional) Number of processes (default: 1)
         --oprefix                                 # (optional) Prefix for output files (default: "root")
         --nploidy                                 # (optional) Genome ploidity (default: 2)
         --chooseSeed                              # (optional) Seed for random functions (default: "None")
         --left_padding                            # (optional) Left padding for the output BED file (default: 0)
         --right_padding                           # (optional) Rigth padding for the output BED file (default: 0)
```

### STEP 3: Geneting Reads

#### Synggen mode 0

This step is based on Synggen mode 0. It takes as input an arbitrary number of real samples BAM files ([Download CRAM files](https://www.internationalgenome.org/data-portal/sample) 
and convert them to BAM files) declared in `data/listOfBams.txt` and stored in `data/bams/`, 
a list of common SNPs (`data/regions.vcf` downloaded from dbSNP), the reference genome to which real samples are aligned (it must be the same used in the previous step, i.e. the template genome), 
and a .bed file containing regions of interest (you can use the BED file produced in the previous step to learn models and generate reads in the regions affected by variants). It extends the .bed file (-rlen and +rlen) and it learns the characteristics of real samples reads (coverage, base quality, and sequencing errors)
in three models: Read Depth Model (RDM), Quality Model (QM), and Position-Based Error Model (PBEM), respectively.
The extended .bed and models are stored in `data/out/`.

```shell
# From the project folder
$(pwd)/synggen \
            mode=0 \                                                # Synggen mode for model generation
            bamlist=$(pwd)/data/listOfBams.txt \                    # List the BAMs from which to learn reads characteristics
            bed=$(pwd)/data/root.bed \                              # BED files containing the regions of interest (produced in the Genome Editing step)
            fasta=$(pwd)/hg38.fasta \                               # The reference genome
            commonsnps=$(pwd)/data/regions.vcf \                    # Real variants from dbSNP
            out=$(pwd)/data/out \                                   # Output directory
            rlen=100 \                                              # Length of the reads to generate
            seqprot=0                                               # Sequencing protocol (0=single-end, 1=paired-end)
```

#### Converter

This step converts the coordinates specified in 3 output files from the previous step (extended .bed file, RDM, PBEM)
to match the coordinates of the altered genome, leveraging on the map produced during 
[Genome Editing](#Genome-Editing). The converted versions of the 3 files are saved in `data/out/`.

```shell
# From the project folder
python3 $(pwd)/src/converter.py 
        --bedfile $(pwd)/data/out/root.extended.bed                # The extended BED file produced during Model Generation
        --rdm $(pwd)/data/out/root.rdm.gz                          # The RDM produced during Model Generation
        --pbe $(pwd)/data/out/root.pbe.gz                          # The PBEM produced during Model Generation
        --index $(pwd)/data/root.idx_0.pkl                         # The map produced during Genome Editing
        --output $(pwd)/data/out/                                  # The output directory
        --read_length 100                                          # Length of the reads to generate                                             
```

#### (Optional) Generate even coverage RDM 

The converted version of the RDM can be used for generating a "fake" RDM, which is not learnt from empirical BAMS. 
Instead, it contains information for describing the scenario in which all the regions of interest have the same probability of
beeing chosen for reads generation, and, inside each region, all positions have the same probability of beeing chosen as
starting position for the generated read. 

```shell
# From the project folder
python3 $(pwd)/src/create_RDM.py 
        --rdm $(pwd)/data/out/root_0.rdm.gz                                # The RDM produced with Converter
        --output $(pwd)/data/out/                                          # Output directory                                             
```


#### Synggen mode1

This step is based on Synggen mode 1. It takes as input the converted .bed, RDM (normal or even coverage), PBEM, the original QM
   (QM does not need to be converted as it does not contain coordinates), and the altered genome and produces the reads that are saved in `data/reads/`.


```shell
# From the project folder
$(pwd)/synggen \
            mode=1 \                                                # Synggen mode for reads generation
            bed=$(pwd)/data/out/root_0.bed                          # The adjusted BED produced with Converter
            fasta=$(pwd)/data/root.hap_0.fa                         # The altered genome produced during Genome Editing
            rdm=$(pwd)/data/out/root_0.rdm.gz                       # The adjusted RDM produced with Converter
            pbe=$(pwd)/data/out/root_0.pbe.gz                       # The adjusted PBEM produced with Converter
            qm=$(pwd)/data/out/root.qm.gz                           # The QM produced during Model Generation
            out=$(pwd)/data/reads                                   # Output directory
            sequencing_errors=1                                     # Whether to apply the effect of PBEM (1) or not (0)
            rlen=100                                                # Length of the reads to generate  
            nreads=20000                                            # Number of reads to generate (see below for the formula)
            seqprot=0                                               # Sequencing protocol (0=single-end, 1=paired-end)                                                                          
```

### Optional steps outside the simulator

- #### Presets calculation
 
[Raw data](https://doi.org/10.5281/zenodo.11458637) derived from COSMIC and TGCA can be used to compute the presets using `cosmic_core_mutations.R` and `tgca_mutations.R`, respectively. The only required flag is the type of tumor for which the calculations should be performed. 

For COSMIC, the name of raw input files should be: `V99_38_MUTANT_<tumour_type>.csv`.
The produced output files will be: `cosmic_core_mutations_workspace_<tumour_type>.RData`.

```shell
# From the project folder
Rscript  $(pwd)/src/preset/cosmic_core_mutations.R 
         --c=breast                                # Select the tumor type you want to compute presets for 
```
For TGCA, the name of raw input files should be: `merged_<tumour_type>.maf`. The produced output files will be: `tgca_mutations_workspace_<tumour_type>.RData`.

```shell
# From the project folder
Rscript  $(pwd)/src/preset/tgca_mutations.R 
         --c=breast                                # Select the tumor type you want to compute presets for 
```

- #### Compare VAR files

The script `varfile_analysis.R` can be used to compare the variant characteristics in up to three VAR files (i.e. it produces figures such as `data/var_comparison.png`).
```shell
# From the project folder
Rscript  $(pwd)/src/preset/varfile_analysis.R
         --tumor_type1=breast                               # The tumour type represented by the first VAR file
         --varfile1=breast.var                              # First VAR file
         --tumor_type2=lung                                 # The tumour type represented by the second VAR file
         --varfile2=lung_.var                               # Second VAR file
         --tumor_type3=colon                                # The tumour type represented by the third VAR file                             
         --varfile3=colon.var                               # Third VAR file
```

- #### Convert CRAM to BAM with Samtools

[This](https://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/data/GBR/) website contains the CRAM files of many real samples (WGS or WES Illumina reads mapped to GRCh38). However, since Synggen works with BAM file, they should be converted. The view command of the samtools package (version > 1.9) can be used for this purpose.

```shell
# Run a docker container containing samtools and mount it on the directory containing your CRAMs
docker run -it -v $(pwd)/data/bams:/mnt pegi3s/samtools_bcftools

```
```shell
# From inside the docker container
cd mnt
samtools view -b -o output.bam input.cram
```

- #### Compute the size of the genome of interest

Given a BED file, MOV&RSim is able to compute the number of reads to generate to obtain a user-specified coverage in the regions of interest. 

```shell
python3 $(pwd)/src/compute_genome_size.py 
            -b $(pwd)/data/out/regions_of_interest.bed     # Input BED            
            -e y                                           # Specify whether the BED in input should be extended or not (y or n)
            -rlen=100                                      # If the BED in input should be extended, specify the read length (the number of bases for the extension)
            -seqprot=0                                     # If the BED in input should be extended, specify the sequencing protocol (if 0, single-end -> extension of 1*rlen at both ends of each region, if 1, paired-end -> extension of 2*rlen at both ends of each region)
            -g $(pwd)/data/hg38.fasta                      # If the BED in input should be extended, specify the reference genome to make sure that the extension happens within chromosome bounds
```

- #### Variant Calling with Nextflow SAREK pipeline

Once the FASTQ are produced using the simulator, it is possible to perform variant calling. To do that, one can use the pipeline manager Nextflow. In particular, the pre-built SAREK pipeline can carry out both germline and somatic variant calling. Once you downloaded [Nextflow](https://nf-co.re/docs/usage/getting_started/installation), run the following commands:

```shell

NEXTFLOW_PATH = /absolute/path/to/your/nextflow/executable
WD_PATH = /absolute/path/to/working/directory 

$NEXTFLOW_PATH run nf-core/sarek 
                        -profile docker                                         
                        --input ./samplesheet.csv                               # Should be withing the working directory                      
                        --outdir $WD_PATH 
                        --genome GATK.GRCh38 
                        --tools haplotypecaller 
                        --max_cpus 5 
                        --max_memory '10GB' 
                        --skip_tools baserecalibrator,baserecalibrator_report,bcftools,dnascope_filter,documentation,fastqc,haplotypecaller_filter,haplotyper_filter,markduplicates,markduplicates_report,mosdepth,multiqc,samtools,vcftools,versions
                        --save_output_as_bam
```
In this example we used SAREK to call variants using haplotypecaller. The samplesheet is a tab separated file containing the following information:

patient | status | sample | lane | fastq_1 | fastq_2 
--- | --- | --- | --- |--- |--- |
patient_1 | 0 | normal_sample | lane_1 | $WD_PATH/example.R1.fastq.gz | $WD_PATH/example.R2.fastq.gz |

MOV&RSim produces `.fastq` file. Make sure to gzip them before running Nextflow. Please refer to the SAREK [documentation](https://nf-co.re/sarek/3.5.0/docs/usage/) to properly set Nextflow parameters (changing aligner, variant caller, etc.).

- #### Compute coverages per site with Samtools

Given the BAM files produced after alignment with Nextflow, it might be interesting to check the coverage at the starting position of the simulated variants. That can be done using samtools and the VCF of ground truth variants produced by MOV&RSim:

```
# Derive the input file needed by samtools from the ground truth VCF
cat ground_truth.vcf | grep -v "#" | awk '{printf("%s:%s-%s\n",$1,$2,$2+1)}' > regions.txt

# Run a docker container containing samtools and mount it on the directory containing regions.txt and the BAMs produced with Nextflow
docker run -it -v $(pwd)/data/pipeline:/mnt pegi3s/samtools_bcftools

# Use samtools
while read p; do samtools coverage -r "$p" nextflow.bam; done < regions.txt > coverages.txt

```


- #### Compare called VCF vs simulated VCF

Nextflow can be used to call variants with different variant callers (e.g. for germline calling it offers haplotypecaller, freebayes, mpileup etc. For somatic calling it offers mutect, strelka, etc.). Samtools can be used to calculate the coverage at the ground truth variant sites and also at the sites called by the different variant callers which can coincide with the simulated variants (TP calls) or not (FP calls). In order to compute the numerosity of TP, FP and FN calls, between three variant callers and the ground truth we provide the script `compare_vcf.py`.

```
python3 $(pwd)/src/compare_vcf.py 
                    -v1 $(pwd)/data/breast.vcf 
                    -v2 $(pwd)/data/pipeline/haplotypecaller.vcf 
                    -v3 $(pwd)/data/pipeline/freebayes.vcf 
                    -v4 $(pwd)/data/pipeline/mpileup.vcf 
                    -c1 $(pwd)/data/pipeline/coverages_at_simulated_sites.txt 
                    -c2 $(pwd)/data/pipeline/coverages_at_hc_called_sites.txt 
                    -c3 $(pwd)/data/pipeline/coverages_at_fb_called_sites.txt 
                    -c4 $(pwd)/data/pipeline/coverages_at_mp_called_sites.txt
```


## First Experiment

We run MOV&RSim using Docker on the BLADE High Performance Computing (HPC) cluster from the Department of Information Engineering of the University of Padova.


We generated 3 VAR files using **Defining Variants** in *automatic mode*.
We selected COSMIC as preferred database and the following tumour types: breast, lung, colon.
We selected 1000 as total number of variants per sample and 1 as heterozigosity percentage in each of them.
```shell
# From the project folder

# First tumor: breast
Rscript  $(pwd)/src/preset/varfile_builder_bash.R 
         --tumor_type=breast                       
         --database=cosmic                           
         --ref_genome=hg38.fasta                    
         --total_variants=1000                       
         --het_percentage=1
# Second tumor: lung
Rscript  $(pwd)/src/preset/varfile_builder_bash.R 
         --tumor_type=lung                       
         --database=cosmic                           
         --ref_genome=hg38.fasta                   
         --total_variants=1000                       
         --het_percentage=1
# Third tumor: colon
Rscript  $(pwd)/src/preset/varfile_builder_bash.R 
         --tumor_type=colon                       
         --database=cosmic                           
         --ref_genome=hg38.fasta                   
         --total_variants=1000                       
         --het_percentage=1                        
```

We saved the 3 outputted VAR files as: 
* root_het_1000_breast_cosmic.var
* root_het_1000_lung_cosmic.var
* root_het_1000_colon_cosmic.var

We compared the 3 VAR files (in `data/`) using the following R script:

```shell
# From the project folder
Rscript src/preset/varfile_analysis.R 
         --tumor_type1=breast 
         --varfile1=root_het_1000_breast_cosmic.var 
         --tumor_type2=lung 
         --varfile2=root_het_1000_lung_cosmic.var 
         --tumor_type3=colon 
         --varfile3=root_het_1000_colon_cosmic.var                  
```

This script produces the following image (in `data/`);
<div align="center">
<img alt="Comparison of the 3 generated VAR files" src="data/var_comparison.png" width="600">
</div>

## Second Experiment

For the following steps, we used only the first VAR file (breast cancer).
We performed **Genome Editing** with the following command:
```shell
# From the project folder
python3 $(pwd)/src/genome_editing.py 
            --varfiles $(pwd)/data/root_het_1000_breast_cosmic.var
            --hapfiles $(pwd)/data/hg38.fasta
            --tmpdir $(pwd)/tmp
            --nprocs 1                    
```
We obtained the following outputs:
* The altered genome: root_het_1000_breast_cosmic.hap_0.fa
* The map: root_het_1000_breast_cosmic.idx_0.pkl
* The BED file: root_het_1000_breast_cosmic.bed
* The VCF file: root_het_1000_breast_cosmic.vcf

We then entered the **Generating Reads** step. We used the ouputted BED file for Synggen mode 0:
```shell
# From the project folder
$(pwd)/synggen 
      mode=0 
      bamlist=$(pwd)/data/listOfBams.txt
      bed=$(pwd)/data/root_het_1000_breast_cosmic.bed
      fasta=$(pwd)/data/hg38.fasta
      commonsnps=$(pwd)/data/regions.vcf 
      out=$(pwd)/data/out 
      rlen=100 
      seqprot=0
```
We obtained the following outputs:
* The extended BED: root_het_1000_breast_cosmic.extended.bed
* The RDM: root_het_1000_breast_cosmic.rdm.gz
* The PBEM: root_het_1000_breast_cosmic.pbe.gz
* The QM: root_het_1000_breast_cosmic.qm.gz

We applied the Converter to the outputted extended BED, RDM, and PBEM, leveraging the information contained in the map:
```shell
# From the project folder
python3 $(pwd)/src/converter.py 
        --bedfile $(pwd)/data/out/root_het_1000_breast_cosmic.extended.bed           
        --rdm $(pwd)/data/out/root_het_1000_breast_cosmic.rdm.gz                        
        --pbe $(pwd)/data/out/root_het_1000_breast_cosmic.pbe.gz                        
        --index $(pwd)/data/root_het_1000_breast_cosmic.idx_0.pkl                    
        --output $(pwd)/data/out/                                  
        --read_length 100                                                                                 
```
We obtained the following outputs:
* The adjusted BED: root_het_1000_breast_cosmic_0.bed
* The adjusted RDM: root_het_1000_breast_cosmic_0.rdm.gz
* The adjusted PBEM: root_het_1000_breast_cosmic_0.pbe.gz

We computed the even coverage RDM giving as input the adjusted RDM:

```shell
# From the project folder
python3 $(pwd)/src/create_RDM.py 
        --rdm $(pwd)/data/out/root_het_1000_breast_cosmic_0.rdm.gz                            
        --output $(pwd)/data/out/                                                                                 
```
We obtained the following output:
* The even coverage RDM: root_het_1000_breast_cosmic_0.uniform.rdm.gz

We then used Synngen mode 1 in 3 ways:
1) *Control reads*: using the adjusted RDM, the adjusted PBEM, the adjusted BED, and the altered genome (sequencing_errors=1).
2) *No error reads*: using the adjusted RDM, the adjusted PBEM, the adjusted BED, and the altered genome (sequencing_errors=0).
3) *Uniform coverage reads*: using the even coverage RDM, the adjusted PBEM, the adjusted BED, and the altered genome (sequencing_errors=1).

```shell
# From the project folder

# 1
$(pwd)/synggen \
            mode=1 \                                               
            bed=$(pwd)/data/out/root_het_1000_breast_cosmic_0.bed                     
            fasta=$(pwd)/data/root_het_1000_breast_cosmic.hap_0.fa                         
            rdm=$(pwd)/data/out/root_het_1000_breast_cosmic_0.rdm.gz                       
            pbe=$(pwd)/data/out/root_het_1000_breast_cosmic_0.pbe.gz                     
            qm=$(pwd)/data/out/root_het_1000_breast_cosmic.qm.gz                          
            out=$(pwd)/data/reads                                   
            sequencing_errors=1                                     
            rlen=100                                                
            nreads=20000                                            
            seqprot=0  
            
# 2
$(pwd)/synggen \
            mode=1 \                                               
            bed=$(pwd)/data/out/root_het_1000_breast_cosmic_0.bed                     
            fasta=$(pwd)/data/root_het_1000_breast_cosmic.hap_0.fa                         
            rdm=$(pwd)/data/out/root_het_1000_breast_cosmic_0.rdm.gz                       
            pbe=$(pwd)/data/out/root_het_1000_breast_cosmic_0.pbe.gz                     
            qm=$(pwd)/data/out/root_het_1000_breast_cosmic.qm.gz                          
            out=$(pwd)/data/reads                                   
            sequencing_errors=0                                    
            rlen=100                                                
            nreads=20000                                            
            seqprot=0   
            
# 3
$(pwd)/synggen \
            mode=1 \                                               
            bed=$(pwd)/data/out/root_het_1000_breast_cosmic_0.bed                     
            fasta=$(pwd)/data/root_het_1000_breast_cosmic.hap_0.fa                         
            rdm=$(pwd)/data/out/root_het_1000_breast_cosmic_0.uniform.rdm.gz                       
            pbe=$(pwd)/data/out/root_het_1000_breast_cosmic_0.pbe.gz                     
            qm=$(pwd)/data/out/root_het_1000_breast_cosmic.qm.gz                          
            out=$(pwd)/data/reads                                   
            sequencing_errors=1                                     
            rlen=100                                                
            nreads=20000                                            
            seqprot=0                                                                                                                  
```

We obtained the following outputs:
* Normal Reads: low.R1.fastq
* No Sequencing Errors Reads: low_no_pbe.R1.fastq
* No Coverage Reads: low_no_rdm.R1.fastq

## Third Experiment

Using **Defining Variants** in *interactive mode*, we generated 4 VAR files each representing the variants of four clones which will be included in the final simulated sample. 
We selected COSMIC as preferred database and breast as tumour type.

The "root" clone contains 1000 variants, with half of them in homozygosity and half in heterozygosity. The first clone ("clone1") has 1000 variants more than the "root", the second ("clone2") has 500 variants more than the "root", and the third ("clone3") has 500 variants more than the second. For each non-root clone, half of the additional variants are in homozygosity and half in heterozygosity. 

The phylogenetic tree `tree.txt` that should be given as input to the guided procedure is:

parent | child | distance | zigosity | 
--- | --- | --- | ---|
root | clone1 | 1000 | 0.5|
root | clone2 | 500 | 0.5|
clone1 | clone3 | 500 | 0.5|

We obtained 4 VAR files: `root.var`, `clone1.var`, `clone2.var`, `clone3.var`.


We performed **Genome Editing** with the following command:
```shell
python3 $(pwd)/src/genome_editing.py 
            --varfiles $(pwd)/data/root.var,$(pwd)/data/clone1.var,$(pwd)/data/clone2.var,$(pwd)/data/clone3.var
            --hapfiles $(pwd)/data/hg38.fasta
            --tmpdir $(pwd)/tmp
            --nprocs 4                    
```


We obtained one directory per clone (named `root`, `clone1`, `clone2`, `clone3`).
Each directory contains the outputs for the corresponding clone:
* The altered genome (first haploid): root.hap_0.fa
* The altered genome (second haploid): root.hap_1.fa
* The map (first haploid): root.idx_0.pkl
* The map (second haploid): root.idx_1.pkl
* The BED file: root.bed
* The VCF file: root.vcf

To generate the models and the reads with Synggen, we need a unique BED file, containing the regions of interest of all the four clones. Thus, we used the following script yo merge them:

```

python3 $(pwd)/src/merge_bed.py 
                $(pwd)/data/root/root.bed 
                $(pwd)/data/clone1/clone1.bed 
                $(pwd)/data/clone2/clone2.bed 
                $(pwd)/data/clone3/clone3.bed 
                -a $(pwd)/data/hg38.fasta 
                -o $(pwd)/data/final.bed

```


We used Synggen mode 0 with the merged BED file:

```shell
$(pwd)/synggen 
      mode=0 
      bamlist=$(pwd)/data/listOfBams.txt
      bed=$(pwd)/data/final.bed
      fasta=$(pwd)/data/hg38.fasta
      commonsnps=$(pwd)/data/regions.vcf 
      out=$(pwd)/data/out 
      rlen=100 
      seqprot=1
```
We obtained the following outputs:
* The extended BED: final.extended.bed
* The RDM: final.rdm.gz
* The PBEM: final.pbe.gz
* The QM: final.qm.gz

We applied the Converter to the outputted extended BED, RDM, and PBEM, leveraging the information contained in the map *for each haplotype and for each clone* (e.g. in this case we choose to simulate 4 clones, each comprising 2 haplotypes, thus the Converter will be used 4*2 times):

```shell
# Starting with the root clone
mkdir -p "$(pwd)/data/out/root"

# Convert the root first haplotype
python3 $(pwd)/src/converter.py 
                -b $(pwd)/data/out/final.extended.bed
                -r $(pwd)/data/out/final.rdm.gz 
                -p $(pwd)/data/out/final.pbe.gz 
                -i $(pwd)/data/root/root.idx_0.pkl 
                -o $(pwd)/data/out/root/ 
                -rlen 100
                
# Convert the root second haplotype
python3 $(pwd)/src/converter.py 
                -b $(pwd)/data/out/final.extended.bed 
                -r $(pwd)/data/out/final.rdm.gz 
                -p $(pwd)/data/out/final.pbe.gz 
                -i $(pwd)/data/root/root.idx_1.pkl 
                -o $(pwd)/data/out/root/ 
                -rlen 100
                
# Now convert the other clones
CLONES=($(seq 1 1 3)) 

for i in ${CLONES[@]}; do
    
    mkdir -p "$(pwd)data/out/clone$i"

    # Convert the i-th clone first haplotype
    python3 $(pwd)/src/converter.py 
                 -b $(pwd)/data/out/final.extended.bed
                 -r $(pwd)/data/out/final.rdm.gz
                 -p $(pwd)/data/out/final.pbe.gz 
                 -i "$(pwd)/data/clone$i/clone$i.idx_0.pkl" 
                 -o "$(pwd)/data/out/clone$i/" 
                 -rlen 100

    # Convert the i-th clone first haplotype
    python3 $(pwd)/src/converter.py 
                 -b $(pwd)/data/out/final.extended.bed 
                 -r $(pwd)/data/out/final.rdm.gz 
                 -p $(pwd)/data/out/final.pbe.gz 
                 -i "$(pwd)/data/clone$i/clone$i.idx_1.pkl" 
                 -o "$(pwd)/data/out/clone$i/" 
                 -rlen 100
done 

```
We obtained the adjusted BED, RDM, and PBEM for each haplotype of each clone stored in their respective directories inside the `data/out/` directory.

* In `data/out/root`: 
    * The adjusted BED for the first haplotype: final_0.bed
    * The adjusted RDM for the first haplotype: final_0.rdm.gz
    * The adjusted PBEM for the first haplotype: final_0.pbe.gz
    * The adjusted BED for the second haplotype: final_1.bed
    * The adjusted RDM for the second haplotype: final_1.rdm.gz
    * The adjusted PBEM for the second haplotype: final_1.pbe.gz
* In `data/out/clone1`:
    * ...
* In `data/out/clone2`:
    * ...

We computed the even coverage RDM giving as input the adjusted RDM *for each haplotype of each clone*:

```shell

python3 $(pwd)/src/create_RDM.py 
        --rdm $(pwd)/data/out/root_het_1000_breast_cosmic_0.rdm.gz                            
        --output $(pwd)/data/out/    
        

# Root clone

python3 $(pwd)/src/create_RDM.py 
            -r $(pwd)/data/out/root/final_0.rdm.gz 
            -o $(pwd)/data/out/root/
            
python3 $(pwd)/src/create_RDM.py 
            -r $(pwd)/data/out/root/final_1.rdm.gz 
            -o $(pwd)/data/out/root/

# Other clones 

CLONES=($(seq 1 1 3)) 

for i in ${CLONES[@]}; do
    
    # Create even rdm for the first haplotype
    python3 $(pwd)/src/create_RDM.py 
            -r "$(pwd)/data/out/clone$i/final_0.rdm.gz" 
            -o "$(pwd)/data/out/clone$i/"
            
    # Create even rdm for the second haplotype
    python3 $(pwd)/src/create_RDM.py 
            -r "$(pwd)/data/out/clone$i/final_1.rdm.gz" 
            -o "$(pwd)/data/out/clone$i/"
    
done 
```

We obtained the even RDM *for each haplotype of each clone* stored in the respective directory inside the `data/out/` directory.

* In `data/out/root`: 
    * The even coverage RDM for the fist haplotype: final_0.uniform.rdm.gz
    * The even coverage RDM for the second haplotype: final_1.uniform.rdm.gz
* In `data/out/clone1`:
    * ...
* In `data/out/clone2`:
    * ...

We used Synggen mode 1 in 3 ways:
1) *Control reads*: using the adjusted RDM, the adjusted PBEM, the adjusted BED, and the altered genome (sequencing_errors=1).
2) *No error reads*: using the adjusted RDM, the adjusted PBEM, the adjusted BED, and the altered genome (sequencing_errors=0).
3) *Uniform coverage reads*: using the even coverage RDM, the adjusted PBEM, the adjusted BED, and the altered genome (sequencing_errors=1).

<div style="border: 2px solid red; padding: 10px; background-color: #ffe6e6;">
For brevity, we provide below the code to reproduce only the first of these three cases. However, as seen in the second experiment, it is sufficient to tune the sequencing_errors parameter of synggen from 1 to 0 to remove sequencing errors and replace the RDM learned from real data (e.g. final_0.rdm.gz) with the even coverage RDM (e.g. final_0.uniform.rdm.gz) to produce a uniform coverage reads.
</div>

```shell

# 1

COVERAGE=10                                         # Desired coverage for the final simulated sample
PROPORTIONS=(0.4 0.3 0.2 0.1)                       # Proportion of different clone in the final simulated sample
STRAND_BIAS_1=0.5                                   # Proportion of reads that should be taken from the first haplotype
RLEN=100                                            # Reads length
SEQPROT=1                                           # Sequencing protocol

STRAND_BIAS_2=$(echo "1 - $STRAND_BIAS_1" | bc)     # The simulator derives the proportion of reads that should be taken from the second haplotype

# Root clone

mkdir -p "data/reads/root"

c=$(echo "$COVERAGE * ${PROPORTIONS[0]}" | bc)      # Compute the coverage for the root clone based on the user-specified proportions
c1=$(echo "$c * $STRAND_BIAS_1" | bc)               # Compute the coverage for the first haplotype of the root clone based on the strand bias
c2=$(echo "$c * $STRAND_BIAS_2" | bc)               # Compute the coverage for the second haplotype of the root clone based on the strand bias

# Compute genome size for the first haplotype of the root clone
G1=$(python3 $(pwd)/src/compute_genome_size.py 
                -b $(pwd)/data/out/root/final_0.bed 
                -e y 
                -rlen=$RLEN 
                -seqprot=$SEQPROT 
                -g $(pwd)/data/hg38.fasta)

# Compute genome size for the first haplotype of the root clone
G2=$(python3 $(pwd)/src/compute_genome_size.py 
                -b $(pwd)/data/out/root/final_1.bed 
                -e y 
                -rlen=$RLEN 
                -seqprot=$SEQPROT 
                -g $(pwd)/data/hg38.fasta) 

# Compute the number of reads to generate from the first haplotype of the root clone
tmp=$(echo "$c1 * $G1" | bc)
r1=$(echo "$tmp / $RLEN" | bc) 

# Compute the number of reads to generate from the second haplotype of the root clone
tmp=$(echo "$c2 * $G2" | bc)
r2=$(echo "$tmp / $RLEN" | bc) 

# Generate the reads from the first haplotype of the root clone
$(pwd)/synggen 
            mode=1 
            bed=$(pwd)/data/out/root/final_0.bed
            fasta=$(pwd)/data/root/root.hap_0.fa
            rdm=$(pwd)/data/out/root/final_0.rdm.gz 
            pbe=$(pwd)/data/out/root/final_0.pbe.gz 
            qm=$(pwd)/data/out/final.qm.gz 
            out=$(pwd)/data/reads/root 
            nreads=$r1 
            seqprot=$SEQPROT 
            rlen=$RLEN 
            sequencing_errors=1 

# Generate the reads from the second haplotype of the root clone
$(pwd)/synggen 
            mode=1 
            bed=$(pwd)/data/out/root/final_1.bed 
            fasta=$(pwd)/data/root/root.hap_1.fa 
            rdm=$(pwd)/data/out/root/final_1.rdm.gz 
            pbe=$(pwd)/data/out/root/final_1.pbe.gz 
            qm=$(pwd)/data/out/final.qm.gz 
            out=$(pwd)/data/reads/root 
            nreads=$r2 
            seqprot=$SEQPROT 
            rlen=$RLEN 
            sequencing_errors=1 

# Other clones

CLONES=($(seq 1 1 3)) 

for i in ${CLONES[@]}; do
    
    mkdir -p "data/reads/clone$i"

    c=$(echo "$COVERAGE * ${PROPORTIONS[$i]}" | bc)             # Compute the coverage for the i-th clone based on the user-specified proportions
    c1=$(echo "$c * $STRAND_BIAS_1" | bc)                       # Compute the coverage for the first haplotype of the root clone based on the strand bias
    c2=$(echo "$c * $STRAND_BIAS_2" | bc)                       # Compute the coverage for the second haplotype of the root clone based on the strand bias
    
    # Compute genome size for the first haplotype of the i-th clone
    G1=$(python3 $(pwd)/src/compute_genome_size.py 
                    -b "$(pwd)/data/out/clone$i/final_0.bed" 
                    -e y 
                    -rlen=$RLEN 
                    -seqprot=$SEQPROT 
                    -g $(pwd)/data/hg38.fasta) 
    
    # Compute genome size for the second haplotype of the i-th clone                
    G2=$(python3 $(pwd)/src/compute_genome_size.py 
                    -b "$(pwd)/data/out/clone$i/final_1.bed" 
                    -e y 
                    -rlen=$RLEN 
                    -seqprot=$SEQPROT 
                    -g $(pwd)/data/hg38.fasta) 
                    
    # Compute the number of reads to generate from the first haplotype of the i-th clone
    tmp=$(echo "$c1 * $G1" | bc)
    r1=$(echo "$tmp / $RLEN" | bc) 
    
    # Compute the number of reads to generate from the second haplotype of the i-th clone
    tmp=$(echo "$c2 * $G2" | bc)
    r2=$(echo "$tmp / $RLEN" | bc) 

    # Generate the reads from the first haplotype of the i-th clone
    $(pwd)/synggen 
            mode=1 
            bed="$(pwd)/data/out/clone$i/final_0.bed" 
            fasta="$(pwd)/data/clone$i/clone$i.hap_0.fa" 
            rdm="$(pwd)/data/out/clone$i/final_0.rdm.gz" 
            pbe="$(pwd)/data/out/clone$i/final_0.pbe.gz" 
            qm=$(pwd)/data/out/final.qm.gz 
            out="$(pwd)/data/reads/clone$i" 
            nreads=$r1 
            seqprot=$SEQPROT 
            rlen=$RLEN 
            sequencing_errors=1

    # Generate the reads from the second haplotype of the i-th clone
    $(pwd)/synggen 
            mode=1 
            bed="$(pwd)/data/out/clone$i/final_1.bed" 
            fasta="$(pwd)/data/clone$i/clone$i.hap_1.fa" 
            rdm="$(pwd)/data/out/clone$i/final_1.rdm.gz" 
            pbe="$(pwd)/data/out/clone$i/final_1.pbe.gz" 
            qm=$(pwd)/data/out/final.qm.gz 
            out="$(pwd)/data/reads/clone$i" 
            nreads=$r2 
            seqprot=$SEQPROT 
            rlen=$RLEN 
            sequencing_errors=1

done 
                                                                                                                 
```

We obtained the following outputs:
* Normal Reads: low.R1.fastq
* No Sequencing Errors Reads: low_no_pbe.R1.fastq
* No Coverage Reads: low_no_rdm.R1.fastq

We obtained the reads *for each haplotype of each clone* stored in the respective directory inside the `data/reads/` directory.

* In `data/reads/root`: 
    * R1 first haplotype: final_0.R1.fastq
    * R2 first haplotype: final_0.R2.fastq
    * R1 second haplotype: final_1.R1.fastq
    * R2 second haplotype: final_1.R2.fastq
* In `data/out/clone1`:
    * ...
* In `data/out/clone2`:
    * ...

Finally, to merge the reads representing the final simulated sample:
```
# Input R1 FASTQ files
root_fastq_0="data/reads/root/final_0.R1.fastq"
root_fastq_1="data/reads/root/final_1.R1.fastq"

clone1_fastq_0="data/reads/clone1/final_0.R1.fastq"
clone1_fastq_1="data/reads/clone1/final_1.R1.fastq"

clone2_fastq_0="data/reads/clone2/final_0.R1.fastq"
clone2_fastq_1="data/reads/clone2/final_1.R1.fastq"

clone3_fastq_0="data/reads/clone3/final_0.R1.fastq"
clone3_fastq_1="data/reads/clone3/final_1.R1.fastq"

output_fastq_R1="data/reads/merged.R1.fastq"

# Combine the reads from different clones, ensuring the header line of each read retains information about its origin
cat $root_fastq_0 | sed '1~4 s/$/_root_0/' > $output_fastq_R1
cat $root_fastq_1 | sed '1~4 s/$/_root_1/' >> $output_fastq_R1

cat $clone1_fastq_0 | sed '1~4 s/$/_clone1_0/' >> $output_fastq_R1
cat $clone1_fastq_1 | sed '1~4 s/$/_clone1_1/' >> $output_fastq_R1

cat $clone2_fastq_0 | sed '1~4 s/$/_clone2_0/' >> $output_fastq_R1
cat $clone2_fastq_1 | sed '1~4 s/$/_clone2_1/' >> $output_fastq_R1

cat $clone3_fastq_0 | sed '1~4 s/$/_clone3_0/' >> $output_fastq_R1
cat $clone3_fastq_1 | sed '1~4 s/$/_clone3_1/' >> $output_fastq_R1


# Input R2 FASTQ files
root_fastq_0="data/reads/root/final_0.R2.fastq"
root_fastq_1="data/reads/root/final_1.R2.fastq"

clone1_fastq_0="data/reads/clone1/final_0.R2.fastq"
clone1_fastq_1="data/reads/clone1/final_1.R2.fastq"

clone2_fastq_0="data/reads/clone2/final_0.R2.fastq"
clone2_fastq_1="data/reads/clone2/final_1.R2.fastq"

clone3_fastq_0="data/reads/clone3/final_0.R2.fastq"
clone3_fastq_1="data/reads/clone3/final_1.R2.fastq"

output_fastq_R2="data/reads/merged.R2.fastq"

# Combine the reads from different clones, ensuring the header line of each read retains information about its origin
cat $root_fastq_0 | sed '1~4 s/$/_root_0/' > $output_fastq_R2
cat $root_fastq_1 | sed '1~4 s/$/_root_1/' >> $output_fastq_R2

cat $clone1_fastq_0 | sed '1~4 s/$/_clone1_0/' >> $output_fastq_R2
cat $clone1_fastq_1 | sed '1~4 s/$/_clone1_1/' >> $output_fastq_R2

cat $clone2_fastq_0 | sed '1~4 s/$/_clone2_0/' >> $output_fastq_R2
cat $clone2_fastq_1 | sed '1~4 s/$/_clone2_1/' >> $output_fastq_R2

cat $clone3_fastq_0 | sed '1~4 s/$/_clone3_0/' >> $output_fastq_R2
cat $clone3_fastq_1 | sed '1~4 s/$/_clone3_1/' >> $output_fastq_R2
```

We obtained the reads for the final simulated sample in `data/reads/` directory.

## Create a bug report

If you run into any issue, please feel free to [create a bug report](https://gitlab.com/sysbiobig/movarsim/-/issues/new).

## License

This project is licensed under the GNU General Public License v3.0. See the [LICENSE](LICENSE) file for details

## Contact

* **Francesca Longhin**<sup>1</sup> - [francesca.longhin.2@studenti.unipd.it](mailto:francesca.longhin.2@studenti.unipd.it)
* **Giacomo Baruzzo**<sup>1</sup> - [giacomo.baruzzo@unipd.it](mailto:giacomo.baruzzo@unipd.it)
* **Barbara Di Camillo**<sup>1,2</sup> - [barbara.dicamillo@unipd.it](mailto:barbara.dicamillo@unipd.it)

<sup>1</sup> Department of Information Engineering, University of Padova, Padova, Italy
<br/>
<sup>2</sup> Department of Comparative Biomedicine and Food Science, University of Padova, Padova, Italy
